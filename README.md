# README #

### What is this repository for? ###

* Android code for feton app. It is a part of project FETON 1.0.

### How do I get set up? ###

* Checkout project from git
* Make and build using android studio IDE
* Depends on MPChartLib, beats-V3  and google-services
* Firebase database service

### Contribution guidelines ###

* Code review is not mandatory but recommended
* A standard company documentation should be followed while pushing new modifications in the code base

### Who do I talk to? ###

* CareNX Innovations Pvt. Ltd.
* www.carenx.com
