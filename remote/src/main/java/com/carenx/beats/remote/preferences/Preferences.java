package com.carenx.beats.remote.preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.carenx.beats.remote.R;

import java.util.Collections;
import java.util.Set;

/**
 * Created by sylvek on 18/05/2015.
 */
public class Preferences {

    public enum Source {
        single_click, double_click, out_of_range, connected
    }

    public static final String ACTION_SINGLE_BUTTON_CLICK = "action_single_click";
    public static final String ACTION_DOUBLE_BUTTON_CLICK = "action_double_click";
    public static final String ACTION_OUT_OF_RANGE_LIST = "action_out_of_range_list";
    public static final String ACTION_CONNECTED = "action_connected";
    public static final String ACTION_ON_POWER_OFF = "action_on_power_off";
    public static final String FOREGROUND = "action_foreground";
    private static final String DOUBLE_BUTTON_DELAY = "double_button_delay";
    public static final String CUSTOM_ACTION = "custom_action";
    public static final String CUSTOM_ACTION_ON_CONNECTED = "com.carenx.feto.marker.state_connected";
    public static final String CUSTOM_ACTION_ON_SINGLE_CLICK = "com.carenx.feto.marker.single_click";
    public static final String CUSTOM_ACTION_ON_DOUBLE_CLICK = "com.carenx.feto.marker.double_click";
    public static final String CUSTOM_ACTION_ON_DISCONNECTED = "com.carenx.feto.marker.state_disconnected";

    public static long getDoubleButtonDelay(Context context)
    {
        final String defaultDoubleButtonDelay = context.getString(R.string.default_double_button_delay);
        return Long.valueOf(PreferenceManager.getDefaultSharedPreferences(context).getString(DOUBLE_BUTTON_DELAY, defaultDoubleButtonDelay));
    }

    private static SharedPreferences getSharedPreferences(Context context, String address)
    {
        return context.getSharedPreferences(address, Context.MODE_PRIVATE);
    }


    public static boolean isActionOnPowerOff(Context context, String address)
    {
        return getSharedPreferences(context, address).getBoolean(ACTION_ON_POWER_OFF, false);
    }


    public static boolean clearAll(Context context, String address)
    {
        return getSharedPreferences(context, address).edit().clear().commit();
    }


}
