package com.carenx.beats.remote;

import android.app.ActivityManager;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.carenx.beats.remote.database.Devices;
import com.carenx.beats.remote.database.Events;
import com.carenx.beats.remote.deviceDashboard.DashboardFragment;
import com.carenx.beats.remote.preferences.Preferences;
import com.carenx.beats.remote.services.BluetoothLEService;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class MarkerConfigActivity extends SwipeRefreshActivity implements DevicesFragment.OnDevicesListener,
        DeviceAlertDialogFragment.OnConfirmAlertDialogListener, ConfirmAlertDialogFragment.OnConfirmAlertDialogListener,
        DashboardFragment.OnDashboardListener{


    public static final String TAG = MarkerConfigActivity.class.toString();

    private BluetoothLEService service;

    private final static int REQUEST_ENABLE_BT = 1;

    private static final int CONFIRM_REMOVE_KEYRING = 1;
    private static final long SCAN_PERIOD = 10000; // 10 seconds


    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            if (iBinder instanceof BluetoothLEService.BackgroundBluetoothLEBinder) {
                service = ((BluetoothLEService.BackgroundBluetoothLEBinder) iBinder).service();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            Log.d(BluetoothLEService.TAG, "onServiceDisconnected()");
        }
    };

    private BluetoothLEService serviceDash;
    private ServiceConnection serviceConnectionDashboard = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            if (iBinder instanceof BluetoothLEService.BackgroundBluetoothLEBinder) {
                serviceDash = ((BluetoothLEService.BackgroundBluetoothLEBinder) iBinder).service();
                serviceDash.connect(MarkerConfigActivity.this.address);
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            Log.d(BluetoothLEService.TAG, "onServiceDisconnected()");
        }
    };

    private final Random random = new Random();

    private final DevicesFragment devicesFragment = DevicesFragment.instance();

    private Handler mHandler;
    private BluetoothAdapter mBluetoothAdapter;
    private Runnable stopScan;

    private final Map<String, String> devices = new HashMap<>();

    private BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback() {

        @Override
        public void onLeScan(BluetoothDevice device, int rssi, byte[] scanRecord) {
            final String address = device.getAddress();
            final String name = device.getName();

            Log.d(TAG, "device " + name + " with address " + address + " found");
            if (!Devices.containsDevice(MarkerConfigActivity.this, address)) {
                devices.put((name == null) ? address : name, address);
            }
        }
    };
    private boolean activated = false;
    private FloatingActionButton mFabBell;
    private String address;
    private ProgressBar progressBar;

    private void selectDevice(String name, String address) {
        Devices.insert(this, name, address);
        Devices.setEnabled(this, address, true);
        service.connect(address);
        devicesFragment.refresh();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_marker_config);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setIcon(R.drawable.ic_logo);
        // detect Bluetooth LE support
        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(this, R.string.ble_not_supported, Toast.LENGTH_SHORT).show();
            finish();
        }

        TextView txtWarning = (TextView) findViewById(R.id.txtWarning);
        LocationManager lm = (LocationManager)
                getSystemService(Context.LOCATION_SERVICE);
        boolean locationEnabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if(!locationEnabled){
            txtWarning.setText("Please Enable the Location Service");
            txtWarning.setVisibility(View.VISIBLE);
        }

        final BluetoothManager bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();

        mHandler = new Handler();

        showDevices();

        if (!isMyServiceRunning(BluetoothLEService.class)) {
            startService(new Intent(this, BluetoothLEService.class));
        }
        mFabBell = (FloatingActionButton) findViewById(R.id.fabBell);
        mFabBell.hide();
        mFabBell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mFabBell.setImageResource((activated) ? android.R.drawable.ic_lock_silent_mode_off : android.R.drawable.ic_lock_silent_mode);
                activated = !activated;
                onImmediateAlert(address, activated);
            }
        });
        progressBar = findViewById(R.id.progressBar);
    }

    private void showDevices() {
        getFragmentManager().beginTransaction().replace(R.id.listContainer, devicesFragment).commit();
    }

    @Override
    protected void onStart() {
        super.onStart();

        // detect Bluetooth enabled
        if (mBluetoothAdapter == null || !mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_ENABLE_BT) {
            final BluetoothManager bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
            mBluetoothAdapter = bluetoothManager.getAdapter();
            if (mBluetoothAdapter == null || !mBluetoothAdapter.isEnabled()) {
                finish();
            }
        }
    }

    @Override
    public void onScanStart() {
        devices.clear();
        stopScan = new Runnable() {
            @Override
            public void run() {
                mHandler.removeCallbacks(stopScan);
                mBluetoothAdapter.stopLeScan(mLeScanCallback);
                setRefreshing(false);

                if (!devices.isEmpty()) {
                    displayListScannedDevices();
                    progressBar.setVisibility(View.GONE);
                } else {
                    devicesFragment.snack(getString(R.string.beacon_not_found));
                    progressBar.setVisibility(View.GONE);
                }
            }
        };
        mHandler.postDelayed(stopScan, SCAN_PERIOD);
        mBluetoothAdapter.startLeScan(mLeScanCallback);
        setRefreshing(true);
        progressBar.setVisibility(View.VISIBLE);

    }

    @Override
    public void onDevicesStarted() {
        // bind service
        bindService(new Intent(getApplicationContext(), BluetoothLEService.class), serviceConnection, BIND_AUTO_CREATE);

    }

    @Override
    public void onDevicesStopped() {
        mBluetoothAdapter.stopLeScan(mLeScanCallback);
        mHandler.removeCallbacks(stopScan);

        unbindService(serviceConnection);
    }

    @Override
    public void onDevice(String name, String _address) {
        if(_address!=null) {
            address = _address;
            service.connect(MarkerConfigActivity.this.address);

            showSignal();
            //showPrefrence();
        }

        /*final Intent intent = new Intent(this, DeviceListActivity.class);
        intent.putExtra(Devices.ADDRESS, address);
        intent.putExtra(Devices.NAME, name);
        startActivity(intent);*/
    }
    private void showSignal() {
        getFragmentManager().beginTransaction().replace(R.id.signalContainer, DashboardFragment.instance(address)).commit();
    }
    /*private void showPrefrence() {
        getFragmentManager().beginTransaction().replace(R.id.prefrencefragContainer, DevicePreferencesFragment.instance(address)).commit();
    }*/
    @Override
    public void onChangeDeviceName(String name, String address, boolean checked) {
        DeviceAlertDialogFragment.instance(name, address, checked).show(getFragmentManager(), "dialog");
    }



    @Override
    public void onDeviceStateChanged(String address, boolean enabled) {
        if (service != null) {
            Devices.setEnabled(this, address, enabled);

            if (enabled) {
                service.connect(address);
            } else {
                //AlertDialogFragment.instance(R.string.app_name, R.string.link_loss_disabled).show(getFragmentManager(), null);
                service.disconnect(address);
            }
        }
    }

    private void displayListScannedDevices() {
        final AlertDialog.Builder builderSingle = new AlertDialog.Builder(this);
        builderSingle.setIcon(R.drawable.ic_launcher);
        builderSingle.setTitle(R.string.select_device);
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.select_dialog_singlechoice);
        arrayAdapter.addAll(devices.keySet());
        builderSingle.setSingleChoiceItems(arrayAdapter, 0, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int witch) {
                final String device = arrayAdapter.getItem(witch);
                selectDevice(device, devices.get(device));
                dialog.dismiss();
            }
        });
        builderSingle.setNegativeButton(android.R.string.cancel,
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        builderSingle.show();
    }

    @Override
    public void doPositiveClick(String address, String name) {
        Devices.updateDevice(this, address, name);
        devicesFragment.refresh();
    }

    @Override
    public void doNegativeClick() {
    }

    @Override
    public void doPositiveClick(int returnCode) {
        switch (returnCode) {
            case CONFIRM_REMOVE_KEYRING:
                if (Preferences.clearAll(this, address)) {
                    setRefreshing(false);
                    serviceDash.remove(address);
                    Devices.removeDevice(this, address);
                    Events.removeEvents(this, address);
                    NavUtils.navigateUpFromSameTask(this);
                }
                break;
        }
    }

    @Override
    public void doNegativeClick(int returnCode) {
    }

    @Override
    public void doAlertClick(String address, int alertType) {
        this.service.immediateAlert(address, alertType);
    }

    @Override
    public void doDeleteClick(String address) {
        service.remove(address);
        Devices.removeDevice(this, address);
        devicesFragment.refresh();
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }else
            return super.onOptionsItemSelected(item);
    }

    /*@Override
    public void onOutOfRangerBip(Boolean enabled) {
        serviceDash.setLinkLossNotificationLevel(address, (enabled) ? BluetoothLEService.HIGH_ALERT : BluetoothLEService.NO_ALERT);
    }*/

    @Override
    public void onDashboardStarted() {
        bindService(new Intent(this, BluetoothLEService.class), serviceConnectionDashboard, BIND_AUTO_CREATE);
    }

    @Override
    public void onDashboardStopped() {
        if (serviceDash != null) {
            serviceDash.disconnect(this.address);
        }

        setRefreshing(false);

        unbindService(serviceConnectionDashboard);
    }

    @Override
    public void onImmediateAlertAvailable() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mFabBell.show();
            }
        });
    }
    private void onImmediateAlert(final String address, final boolean activate) {
        service.immediateAlert(address, (activate) ? BluetoothLEService.HIGH_ALERT : BluetoothLEService.NO_ALERT);
    }

    @Override
    protected void onResume() {
        super.onResume();
        address = getIntent().getStringExtra(Devices.ADDRESS);
        //name = getIntent().getStringExtra(Devices.NAME);
        //setTitle(name);
    }
}
