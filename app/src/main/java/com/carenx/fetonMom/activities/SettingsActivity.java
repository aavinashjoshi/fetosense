package com.carenx.fetonMom.activities;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSeekBar;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.carenx.beatsv3.BeatsProcessor;
import com.carenx.beatsv3.NstView;
import com.carenx.fetonMom.R;

public class SettingsActivity extends AppCompatActivity implements View.OnClickListener {

    private SharedPreferences preferences;
    private EditText preDefault;
    private EditText postDefault;
    private SwitchCompat filterSwitch;
    private long exitTime = 0;
    private SwitchCompat UseMicSwitch;
    private EditText txtFilter;
    private EditText txtUseMic;
    private EditText txtUseWavelet;
    private SwitchCompat useWaveletSwitch;
    private SwitchCompat UseBluetoothSwitch;
    private EditText txtUseBluetooth;
    private EditText nstTimeDefault;
    private EditText txtConfigureMarker;
    private EditText txtDemo;
    private SwitchCompat demoSwitch;
    private EditText txtCmPerGrid;
    private SwitchCompat cmSwitch;
    private float preAmpDef;
    private AppCompatSpinner spinner;
    /*private EditText preBell;
    private EditText postBell;*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setIcon(R.drawable.ic_logo);
        preferences = getSharedPreferences("preferences", MODE_PRIVATE);
        int nstTime = preferences.getInt("nstTime", 10);
        //int nstMinPerGrid = preferences.getInt("nstMinPerGrid", 20);

        preAmpDef = preferences.getFloat("preAmpDef", 10f);
        //float postAmpDef = preferences.getFloat("postAmpDef", 1f);
        final boolean isFilterOn = preferences.getBoolean("isFilterOn", true);
        boolean isUseMicOn = preferences.getBoolean("isUseMicOn", true);
        boolean isUseWaveletOn = preferences.getBoolean("isUseWaveletOn", false);
        boolean isUseBluetoothOn = preferences.getBoolean("isUseBluetoothOn", true);
        boolean isDemoOn = preferences.getBoolean("isDemoOn", false);
        boolean is1CmOn = preferences.getBoolean("is1CmOn", false);

        nstTimeDefault = (EditText) findViewById(R.id.defaultNstTime);
        preDefault = (EditText) findViewById(R.id.preAmpDia);
        postDefault = (EditText) findViewById(R.id.postAmpDia);
        filterSwitch = (SwitchCompat) findViewById(R.id.filterSwitch);
        UseMicSwitch = (SwitchCompat) findViewById(R.id.useMicSwitch);
        useWaveletSwitch = (SwitchCompat) findViewById(R.id.useWaveletSwitch);
        UseBluetoothSwitch = (SwitchCompat) findViewById(R.id.useBluetoothSwitch);
        demoSwitch = (SwitchCompat) findViewById(R.id.demoSwitch);
        cmSwitch = (SwitchCompat) findViewById(R.id.cmSwitch);
        txtFilter = (EditText) findViewById(R.id.txtFilter);
        txtCmPerGrid = (EditText) findViewById(R.id.txtCmPerGrid);
        txtUseMic = (EditText) findViewById(R.id.txtUseMic);
        txtUseWavelet = (EditText) findViewById(R.id.txtUseWavelet);
        txtUseBluetooth = (EditText) findViewById(R.id.txtUseBluetooth);
        txtDemo = (EditText) findViewById(R.id.txtdemo);
        txtConfigureMarker = (EditText) findViewById(R.id.txtConfigureMarker);
        spinner = (AppCompatSpinner) findViewById(R.id.spinner);



        nstTimeDefault.setText("" + nstTime);
        preDefault.setText("" + preAmpDef);
        //postDefault.setText("" + postAmpDef);

        filterSwitch.setChecked(isFilterOn);
        UseMicSwitch.setChecked(isUseMicOn);
        useWaveletSwitch.setChecked(isUseWaveletOn);
        UseBluetoothSwitch.setChecked(isUseBluetoothOn);
        demoSwitch.setChecked(isDemoOn);

        cmSwitch.setChecked(is1CmOn);

        txtFilter.setText("");
        txtUseMic.setText("");
        txtUseWavelet.setText("");
        txtUseBluetooth.setText("");
        txtDemo.setText("");
        txtCmPerGrid.setText("");


        txtFilter.setText(isFilterOn ? "on" : "off");
        txtUseMic.setText(isUseMicOn ? "on" : "off");
        txtUseWavelet.setText(isUseWaveletOn ? "on" : "off");
        txtUseBluetooth.setText(isUseBluetoothOn ? "on" : "off");
        txtDemo.setText(isDemoOn ? "on" : "off");
        txtCmPerGrid.setText(String.format("%scm/min",is1CmOn ? "1" : "3"));

        

        filterSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                BeatsProcessor.setFilterOn(b);
                txtFilter.setText(b ? "on" : "off");
                preferences.edit().putBoolean("isFilterOn", b).apply();

            }

        });

        useWaveletSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                BeatsProcessor.setUseWavelet(b);
                txtUseWavelet.setText(b ? "on" : "off");
                preferences.edit().putBoolean("isUseWaveletOn", b).apply();

            }

        });
        UseMicSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                BeatsProcessor.setUseMic(b);
                txtUseMic.setText(b ? "on" : "off");
                preferences.edit().putBoolean("isUseMicOn", b).apply();

            }

        });
        UseBluetoothSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                BeatsProcessor.setUseBluetooth(b);
                txtUseBluetooth.setText(b ? "on" : "off");
                preferences.edit().putBoolean("isUseBluetoothOn", b).apply();

            }

        });

        demoSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                BeatsProcessor.setUseBluetooth(b);
                txtDemo.setText(b ? "on" : "off");
                preferences.edit().putBoolean("isDemoOn", b).apply();
                UseBluetoothSwitch.setChecked(!b);
                filterSwitch.setChecked(!b);
                UseMicSwitch.setChecked(!b);
                toggleDemoGainValues(b);

            }

        });
        nstTimeDefault.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                spinner.performClick();
            }
        });
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                int nstTime = Integer.parseInt(spinner.getSelectedItem().toString());
                nstTimeDefault.setText(spinner.getSelectedItem().toString());
                preferences.edit().putInt("nstTime", nstTime).apply();

                NstView.setNstTime(nstTime);

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinner.setSelection(nstTime==10?0:nstTime==20?1:2);
        /*txtCmPerGrid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cmSwitch.performClick();
            }
        });*/
        cmSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                NstView.setMinPerGridValue(b ? 1 : 3);
                txtCmPerGrid.setText(String.format("%scm/min",b ? "1" : "3"));
                preferences.edit().putBoolean("is1CmOn", b).apply();

            }

        });

        preDefault.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showGainDialog();
            }
        });
        /*preBell.setText(""+preAmpBell);
        postBell.setText(""+postAmpBell);*/
        //((TextInputLayout)findViewById(R.id.tilFilter)).setHintAnimationEnabled(true)

        txtConfigureMarker.setOnClickListener(this);

    }

    private void toggleDemoGainValues(boolean state) {
        if (state) {
            //float previousGain = preferences.getFloat("preAmpDef", 1f);
            BeatsProcessor.setPreAmpDefault(1.5f);
            BeatsProcessor.setPostAmpDefault(1.5f);
        } else {
            float previousGain = preferences.getFloat("preAmpDef", 1f);
            BeatsProcessor.setPreAmpDefault(previousGain);
            BeatsProcessor.setPostAmpDefault(previousGain);
        }
    }

    private void showGainDialog(){
        final Dialog gainDialog = new Dialog(this);
        LayoutInflater inflater = (LayoutInflater)this.getSystemService(LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.dialog_seekbar_for_gain, (ViewGroup)findViewById(R.id.dialog_parent));
        gainDialog.setContentView(layout);
        final TextView tvValue = (TextView) layout.findViewById(R.id.tvValue);
        AppCompatSeekBar seekBar = (AppCompatSeekBar) layout.findViewById(R.id.seekbar);
        Button btnOkay = (Button) layout.findViewById(R.id.btnOkay);

        tvValue.setText(preAmpDef+"");
        seekBar.setProgress((int)preAmpDef*10);
        seekBar.setMax(100);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                tvValue.setText(i/10f+"");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        btnOkay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                float newValue =Float.parseFloat(tvValue.getText().toString());
                preDefault.setText(tvValue.getText());
                preferences.edit().putFloat("preAmpDef", newValue).apply();
                preAmpDef=newValue;


                BeatsProcessor.setPreAmpDefault(newValue);
                BeatsProcessor.setPostAmpDefault(newValue);

                toggleDemoGainValues(demoSwitch.isChecked());

                gainDialog.cancel();
            }
        });
        gainDialog.show();
    }



    @Override
    public void onClick(View view) {

        if (view.getId() == R.id.txtConfigureMarker) {
            //startActivity(new Intent(SettingsActivity.this, MarkerConfigActivity.class));
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        } else
            return super.onOptionsItemSelected(item);
    }

}

