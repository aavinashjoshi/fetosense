package com.carenx.fetonMom.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;

import com.carenx.fetonMom.R;

public class DisclaimerActivity extends AppCompatActivity {

    private Button btnAccept;
    private TextView txtWarning;
    private TextView txtDescription;
    private String warning = "";
    private String desc = "";
    private WebView wv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_disclaimer);

        int type = getIntent().getIntExtra("type", 0);

        if(type ==0 || type == 2)
            getSupportActionBar().setTitle("Terms of use");
        else
            getSupportActionBar().setTitle("Privacy Policy");

        setViews(type);
        setListeners();
    }


    private void setViews(int type) {

        btnAccept = findViewById(R.id.btnAccept);
        wv = findViewById(R.id.wbDescription);

        if(type!=0)
            btnAccept.setVisibility(View.GONE);

        wv.getSettings().setJavaScriptEnabled(true);
        switch (type) {
            case 0:
                wv.loadUrl("file:///android_asset/terms.html");
                break;
            case 1:
                wv.loadUrl("file:///android_asset/policy.html");
                break;
            case 2:
                wv.loadUrl("file:///android_asset/terms.html");
                break;

            default:
                wv.loadUrl("file:///android_asset/terms.html");
        }
        //wv.loadUrl("file:///android_asset/terms.html");

    }

    private void setListeners() {
        btnAccept.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                MyApp.getInstance().getSharedPreferences().edit().putBoolean("disclaimer", true).apply();
                Intent intent = new Intent(DisclaimerActivity.this, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });

    }

    @Override
    public void onBackPressed() {
        finish();

        super.onBackPressed();
    }
	/*
	private void parseSliderInformationFileForValues(String fileName) {
		try {
			String response = Common.getJsonStringFromAssetsFile(DisclaimerActivity.this,fileName);
			if (!TextUtils.isEmpty(response)) {
				

				JSONObject jsonObject = new JSONObject(response);
				JSONArray jsonArray = jsonObject.optJSONArray("Title");
				if (jsonArray != null) {
					
					for (int i = 0; i < jsonArray.length(); i++) {
						warning = jsonArray.optString(i).toString();
						
					}
				}
				jsonArray = jsonObject
						.optJSONArray("Content");
				if (jsonArray != null) {
					
					for (int i = 0; i < jsonArray.length(); i++) {
						desc = jsonArray.optString(i).toString();
						
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/


}
