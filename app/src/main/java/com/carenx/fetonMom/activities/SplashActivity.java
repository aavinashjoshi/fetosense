package com.carenx.fetonMom.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.carenx.fetonMom.R;
import com.carenx.fetonMom.model.User;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

public class SplashActivity extends AppCompatActivity {

    private static final int REQUEST_PERMISSIONS = 112;
    private FirebaseAuth mAuth;
    private FirebaseFirestore mDatabase;
    private ListenerRegistration mListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);

        mAuth = FirebaseAuth.getInstance();
        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setTimestampsInSnapshotsEnabled(true)
                .build();
        mDatabase = FirebaseFirestore.getInstance();
        mDatabase.setFirestoreSettings(settings);

        boolean hasPermission = (ContextCompat.checkSelfPermission(SplashActivity.this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(SplashActivity.this,
                Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(SplashActivity.this,
                Manifest.permission.VIBRATE) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(SplashActivity.this,
                Manifest.permission.BLUETOOTH) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(SplashActivity.this,
                Manifest.permission.BLUETOOTH_ADMIN) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(SplashActivity.this,
                Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(SplashActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED);

        if (!hasPermission) {
            ActivityCompat.requestPermissions(SplashActivity.this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.RECORD_AUDIO,
                            Manifest.permission.BLUETOOTH, Manifest.permission.BLUETOOTH_ADMIN,
                            Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_PERMISSIONS);
        } else {

            /*BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            if (mBluetoothAdapter.isEnabled()) {
                mBluetoothAdapter.disable();
            }*/
            if (!(MyApp.getInstance().getSharedPreferences().getBoolean("disclaimer", false))) {
                Intent intent = new Intent(SplashActivity.this, DisclaimerActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            } else {

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        /*startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                        finish();*/
                        if (mAuth.getCurrentUser() != null) {
                            onAuthSuccess(mAuth.getCurrentUser());
                        } else {
                            startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                            finish();
                        }
                    }
                }, 1000);
            }
        }

    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_PERMISSIONS: {
                for (int result : grantResults) {
                    if (result != PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(SplashActivity.this, "The app was not allowed to the permissions required. Hence, it cannot function properly. Please consider granting it this permission", Toast.LENGTH_LONG).show();
                        finish();
                    }
                }
                if (!(MyApp.getInstance().getSharedPreferences().getBoolean("disclaimer", false))) {
                    Intent intent = new Intent(SplashActivity.this, DisclaimerActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                } else
                    startActivity(new Intent(SplashActivity.this, LoginActivity.class));

                finish();

            }
        }

    }

    private String usernameFromEmail(String email) {
        if (email.contains("@")) {
            return email.split("@")[0];
        } else {
            return email;
        }
    }

    private void onAuthSuccess(FirebaseUser user) {
        String username = user.getDisplayName();
        if (username == null || username.isEmpty())
            username = usernameFromEmail(user.getEmail());

        // Write new user
        User mother = new User(username, user.getEmail(), user.getUid());
        getUser(mother);
    }

    private void getUser(final User user) {
        Query reference = mDatabase.collection("users").whereEqualTo("email", user.getEmail()).whereEqualTo("type", "mother");

        try {
            mListener = reference.addSnapshotListener(new EventListener<QuerySnapshot>() {
                @Override
                public void onEvent(@Nullable QuerySnapshot querySnapshot,
                                    @Nullable FirebaseFirestoreException e) {
                    //DocumentSnapshot document = documentSnapshot.toObject(Mother.class);
                    if (querySnapshot.getDocuments().size() > 0) {
                        DocumentSnapshot document = querySnapshot.getDocuments().get(0);
                        final User u = document.toObject(User.class);
                        Toast.makeText(SplashActivity.this, "Welcome Back", Toast.LENGTH_SHORT).show();
                        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
                            @Override
                            public void onSuccess(InstanceIdResult instanceIdResult) {
                                String token = instanceIdResult.getToken();
                                u.setNotificationToken(token);
                                mDatabase.collection("users").document(u.getDocumentId()).update("notificationToken", token);
                            }
                        });


                        // Go to HomeActivity
                        //Parcelable parcel = Parcels.wrap(user);
                        Intent intent = new Intent(SplashActivity.this, MotherMainActivity.class);
                        intent.putExtra("user", u.getDocumentId());
                        startActivity(intent);
                        mListener.remove();
                        finish();
                    } else {
                        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
                            @Override
                            public void onSuccess(InstanceIdResult instanceIdResult) {
                                String token = instanceIdResult.getToken();
                                user.setNotificationToken(token);
                                mListener.remove();
                                writeNewUser(user);
                            }
                        });
                    }
                }
            });

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    // [START basic_write]
    private void writeNewUser(User user) {

        DocumentReference reference = mDatabase.collection("users").document();
        user.setDocumentId(reference.getId());
        user.setType("mother");
        reference.set(user);

        // Go to HomeActivity
        //Parcelable parcel = Parcels.wrap(user);
        Intent intent = new Intent(SplashActivity.this, MotherMainActivity.class);
        intent.putExtra("user", user.getDocumentId());
        startActivity(intent);
        finish();
    }

    // [END basic_write]
    @Override
    protected void onStop() {
        super.onStop();
        if (mListener != null)
            mListener.remove();
    }
}
