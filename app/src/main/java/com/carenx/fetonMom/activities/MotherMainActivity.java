package com.carenx.fetonMom.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.carenx.fetonMom.R;
import com.carenx.fetonMom.adapters.CalenderAdapter;
import com.carenx.fetonMom.model.Mother;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import static com.carenx.fetonMom.activities.HomeActivity.AUDIO_RECORDER_FOLDER;


public class MotherMainActivity extends AppCompatActivity
        implements FirebaseAuth.AuthStateListener, CalenderAdapter.CalenderEventListener,
        NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    private GridView mGridView;
    private TextView txtMonth;
    private ImageButton btnNext;
    private ImageButton btnPrevious;
    private CalenderAdapter mAdapter;
    private String motherId;
    private Mother mUser;
    private FirebaseFirestore mDatabase;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mother_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseFirestore.getInstance();
        motherId = getIntent().getStringExtra("user");

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MotherMainActivity.this, HomeActivity.class);
                intent.putExtra("motherId", mUser.getDocumentId());
                intent.putExtra("motherName", mUser.getName());
                intent.putExtra("motherLMP", mUser.getLmp().getTime());
                intent.putExtra("motherWeight", mUser.getWeight());
                intent.putExtra("motherMobileNo", mUser.getMobileNo());
                intent.putExtra("doctorName", mUser.getDoctorName());
                intent.putExtra("doctorId", mUser.getDoctorId());
                intent.putExtra("hospitalName", mUser.getDoctorId());

                startActivity(intent);
            }
        });

        setListeners();
        computeDisplayDates();

        getUser();
        getTestDates();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        HomeActivity.externalStoragePath = Environment.getExternalStorageDirectory().getPath() + "/" + AUDIO_RECORDER_FOLDER;
    }

    public void setListeners() {
        mGridView = findViewById(R.id.gridView1);
        txtMonth = findViewById(R.id.txtMonth);
        btnNext = findViewById(R.id.btnNext);
        btnPrevious = findViewById(R.id.btnPrevious);

        txtMonth.setOnClickListener(this);
        btnNext.setOnClickListener(this);
        btnPrevious.setOnClickListener(this);
        /*mGridView.setOnTouchListener(new OnSwipeTouchListener(this) {
            @Override
            public void onSwipeRight() {

                mCalendar.add(Calendar.MONTH, -1);
                resetCalender();
            }

            @Override
            public void onSwipeLeft() {
                mCalendar.add(Calendar.MONTH, 1);
                resetCalender();
            }

            @Override
            public void onSwipeTop() {

            }

            @Override
            public void onSwipeBottom() {

            }
        });*/
    }

    public void computeDisplayDates() {


        mAdapter = new CalenderAdapter(this);//this,mCalendar.get(Calendar.MONTH),mCalendar.get(Calendar.YEAR));
        Calendar calendar = Calendar.getInstance();
        txtMonth.setText(String.format(Locale.ENGLISH, "%tB %tY", calendar, calendar));
        mGridView.setAdapter(mAdapter);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(this);

    }

    @Override
    public void onStop() {
        super.onStop();
        if (this != null) {
            mAuth.removeAuthStateListener(this);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.mother_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        } else if (id == R.id.action_sign_out) {
            mAuth.signOut();

            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            // Handle the camera action
        } else if (id == R.id.nav_profile) {
            Intent intent = new Intent(MotherMainActivity.this, RegistrationActivity.class);
            intent.putExtra("motherId", motherId);
            startActivity(intent);
        } else if (id == R.id.nav_history) {
            Intent intent = new Intent(MotherMainActivity.this, HistoryActivity.class);
            intent.putExtra("motherId", motherId);
            startActivity(intent);
        } else if (id == R.id.nav_doctor) {
            if (mUser.getDoctorId() == null || mUser.getDoctorId().isEmpty()) {
                Intent intent = new Intent(MotherMainActivity.this, HandlerActivity.class);
                intent.putExtra("motherId", motherId);
                startActivity(intent);
            } else {
                Intent intent = new Intent(MotherMainActivity.this, DoctorProfileActivity.class);
                intent.putExtra("doctorId", mUser.getDoctorId());
                startActivity(intent);
            }
        } else if (id == R.id.nav_settings) {
            startActivity(new Intent(MotherMainActivity.this, SettingsActivity.class));
        } else if (id == R.id.nav_about) {
            startActivity(new Intent(MotherMainActivity.this, AboutActivity.class));
        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtMonth:
                resetCalender(0);
                break;
            case R.id.btnNext:
                resetCalender(1);
                break;
            case R.id.btnPrevious:
                resetCalender(-1);
                break;
        }
    }

    private void resetCalender(int add) {
        String month = mAdapter.setMonth(add);
        txtMonth.setText(month);
    }

    private void getUser() {
        Task<DocumentSnapshot> reference = mDatabase.collection("users").document(motherId).get();

        try {
            reference.addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                @Override
                public void onSuccess(DocumentSnapshot documentSnapshot) {
                    if (documentSnapshot != null && documentSnapshot.exists()) {

                        mUser = documentSnapshot.toObject(Mother.class);
                        if (!mUser.getName().isEmpty()) {
                            getSupportActionBar().setTitle(mUser.getName());
                        } else
                            getSupportActionBar().setTitle(mUser.getEmail());
                        //if(!mUser.noOfMother)
                        //getSupportActionBar().setSubtitle("Mothers : " + mUser.noOfMother);
                        //Log.i("userid", mAuth.getCurrentUser().getUid());

                        if (mAdapter != null && mUser.getEdd() != null)
                            mAdapter.setLimits(mUser.getLmp(), mUser.getEdd());
                        else {
                            Intent intent = new Intent(MotherMainActivity.this, RegistrationActivity.class);
                            intent.putExtra("motherId", motherId);
                            startActivity(intent);
                            finish();
                        }
                    }
                }
            });

        } catch (Exception ex) {
            ex.printStackTrace();
        }


    }

    private void getTestDates(){
        final ArrayList<Date> dates = new ArrayList<>();
        mDatabase.collection("Tests").whereEqualTo("motherId",motherId).get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                for (DocumentSnapshot snapshot : queryDocumentSnapshots){
                    long timestamp = (long) snapshot.get("createdOn");
                    dates.add(new Date(timestamp));
                }
                mAdapter.setHighlightedDates(dates);
            }
        });
    }

    @Override
    public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
        FirebaseUser user = firebaseAuth.getCurrentUser();
        if (user == null) {
            // user auth state is changed - user is null
            // launch login activity
            startActivity(new Intent(MotherMainActivity.this, LoginActivity.class));
            finish();
        }
    }

    @Override
    public void onDateSelected(int date, int month, int year) {

        Toast.makeText(this, date + "", Toast.LENGTH_SHORT).show();
    }
}
