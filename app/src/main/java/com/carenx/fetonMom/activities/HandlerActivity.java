package com.carenx.fetonMom.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.carenx.fetonMom.R;
import com.carenx.fetonMom.model.Doctor;
import com.carenx.fetonMom.model.Mother;
import com.carenx.fetonMom.model.User;
import com.carenx.fetonMom.model.UserMom;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.util.HashMap;
import java.util.Map;

public class HandlerActivity extends AppCompatActivity {


    private FirebaseFirestore mDatabase;
    private String mEmail;
    private FirebaseAuth mAuth;
    private EditText edtEmail;
    private Button btnSave;
    private Button btnSkip;
    private ProgressBar progressBar;
    private LinearLayout wideLayout;
    private String TAG = HandlerActivity.class.getSimpleName();
    private UserMom mom;
    private User mDoctor;
    private ListenerRegistration mListener;
    private String motherId;
    private boolean handler;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_handler);
        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);

        mDatabase = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();

        motherId = getIntent().getStringExtra("motherId");

        findViews();

        mEmail = mAuth.getCurrentUser().getEmail();

        //checkIfReturningUser();
        showAddDoctorDetails();

    }

    private void findViews() {
        edtEmail = findViewById(R.id.email);
        btnSave = findViewById(R.id.save);
        btnSkip = findViewById(R.id.skip);
        wideLayout = findViewById(R.id.wideLayout);
        progressBar = findViewById(R.id.progressBar);

        wideLayout.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtEmail.getText().toString().isEmpty()) {
                    edtEmail.setError("wrong email");
                    return;
                }
                checkIfDoctorExists(edtEmail.getText().toString());
                showProgressBar();
            }
        });
    }




    private void showAddDoctorDetails() {
        wideLayout.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
    }
    private void showProgressBar() {
        wideLayout.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
    }


    private void checkIfDoctorExists(String email) {

        final com.google.firebase.firestore.Query reference = mDatabase.collection("users").whereEqualTo("email", email).whereEqualTo("type","doctor");

        try {
            mListener = reference.addSnapshotListener(new EventListener<QuerySnapshot>() {
                @Override
                public void onEvent(@Nullable QuerySnapshot querySnapshot,
                                    @Nullable FirebaseFirestoreException e) {
                    //DocumentSnapshot document = documentSnapshot.toObject(Mother.class);
                    if (querySnapshot.getDocuments().size() > 0) {
                        DocumentSnapshot document = querySnapshot.getDocuments().get(0);
                        final Doctor u = document.toObject(Doctor.class);
                        writeDoctorDetails(u);
                        // Go to DoctorProfileActivity
                        //Parcelable parcel = Parcels.wrap(user);
                        Intent intent = new Intent(HandlerActivity.this, DoctorProfileActivity.class);
                        intent.putExtra("doctorId", u.getDocumentId());
                        intent.putExtra("motherId", motherId);
                        intent.putExtra("handler", true);
                        startActivity(intent);
                        mListener.remove();
                        finish();
                    } else {
                        Toast.makeText(HandlerActivity.this, "No such doctor exists", Toast.LENGTH_SHORT).show();
                        edtEmail.setError("No such doctor exists");
                        showAddDoctorDetails();
                        mListener.remove();
                    }
                }
            });

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }


    private void writeDoctorDetails(Doctor doctor){
        Map<String,Object> map = new HashMap<>();
        map.put("doctorId",doctor.getDocumentId());
        map.put("doctorName",doctor.getName());
        map.put("hospitalName",doctor.getHospitalName());
        map.put("doctorEmail",doctor.getEmail());
        mDatabase.collection("users").document(motherId).update(map);
    }


}
