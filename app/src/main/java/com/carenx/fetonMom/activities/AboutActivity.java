//about app
package com.carenx.fetonMom.activities;

import android.animation.ObjectAnimator;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.carenx.fetonMom.R;

public class AboutActivity extends AppCompatActivity {

	TextView txtWebSite;
	TextView txtDescription;
	TextView txtTerms;
	private TextView txtPolicy;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_about);
		findViews();
		android.support.v7.app.ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setTitle("About");
		setListeners();
		final int menuHeight = 500;
		final ObjectAnimator slideDownTranslate =
				ObjectAnimator.ofFloat(txtTerms,View.TRANSLATION_Y, menuHeight);

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {

			case android.R.id.home:
				finish();

				return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private void findViews(){
		txtWebSite=findViewById(R.id.txtWebSite);
		txtDescription=findViewById(R.id.txtmail);
		txtTerms=findViewById(R.id.txtTerms);
		txtPolicy=findViewById(R.id.txtPolicy);
	}

	private void setListeners(){
		txtDescription.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(Intent.ACTION_SEND);
				i.setType("message/rfc822");
				i.putExtra(Intent.EXTRA_EMAIL, new String[]{"info@carenx.com"});
				i.putExtra(Intent.EXTRA_SUBJECT, "Sent from feton App");
				try {
					startActivity(Intent.createChooser(i, "Send mail..."));
				} catch (android.content.ActivityNotFoundException ex) {
					Toast.makeText(AboutActivity.this,"Email_Clients_Not_Installed", Toast.LENGTH_SHORT).show();
				}
			}
		});
		txtWebSite.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String url = "https://feton.carenx.com";
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri.parse(url));
				startActivity(i);
			}
		});
		txtTerms.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent=new Intent(AboutActivity.this,DisclaimerActivity.class);
				intent.putExtra("type",2);
				startActivity(intent);
			}
		});
		txtPolicy.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent=new Intent(AboutActivity.this,DisclaimerActivity.class);
				intent.putExtra("type",1);
				startActivity(intent);
			}
		});

	}
}
