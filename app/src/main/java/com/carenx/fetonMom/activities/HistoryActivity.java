package com.carenx.fetonMom.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import com.carenx.fetonMom.R;
import com.carenx.fetonMom.fragments.HistoryFragment;
import com.carenx.fetonMom.model.NSTTest;

public class HistoryActivity extends AppCompatActivity implements HistoryFragment.OnClickEventListener {

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    private boolean hideMenuOptions = true;
    private String motherId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        motherId = getIntent().getStringExtra("motherId");

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

    }

    @Override
    public void OnFhrItemClick(String path) {
        /*Intent intent = new Intent(HomeActivity.this, DetailsActivity.class);
        intent.putExtra("path", path);
        startActivity(intent);*/
        onBackPressed();
    }

    @Override
    public void OnFhrItemStatusClick(NSTTest file, int type) {
        if (type == 0) {
            Toast.makeText(this, "File is all ready synced", Toast.LENGTH_SHORT).show();
            //return;
        } //else if (type == 1)
            //uploadFromUri(file);
        //else if (type == 2)
            //beginDownload(file);
        //else
            //Toast.makeText(this, "File is not uploaded", Toast.LENGTH_SHORT).show();
        //Toast.makeText(this, file.getAudioLocalPath(), Toast.LENGTH_SHORT).show();
        //showProgressDialog();
    }

    @Override
    public void OnFrrItemsSelected(int size) {
        if (size > 0) {
            HomeActivity.showMenuItems = true;
        } else {
            HomeActivity.showMenuItems = false;
        }
        supportInvalidateOptionsMenu();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            switch (position) {
                case 0:
                    return HistoryFragment.newInstance(motherId);

            }
            return HistoryFragment.newInstance(motherId);
        }

        @Override
        public int getCount() {
            // Show 4 total pages.
            return 1;
        }


        @Override
        public CharSequence getPageTitle(int position) {

            return "History";
        }

    }
}
