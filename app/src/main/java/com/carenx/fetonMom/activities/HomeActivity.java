package com.carenx.fetonMom.activities;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.session.MediaSession;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.carenx.beatsv3.BeatsProcessor;
import com.carenx.beatsv3.BeatsRecorder;
import com.carenx.beatsv3.NstView;
import com.carenx.beatsv3.utils.BluetoothDeviceConnectionReciver;
import com.carenx.fetonMom.R;
import com.carenx.fetonMom.Utils.DividerItemDecoration;
import com.carenx.fetonMom.adapters.NavigationRecylerViewAdapter;
import com.carenx.fetonMom.fragments.HistoryFragment;
import com.carenx.fetonMom.fragments.NstFragment;
import com.carenx.fetonMom.model.NSTTest;
import com.carenx.fetonMom.service.DownloadService;
import com.carenx.fetonMom.service.RemoteControl;
import com.carenx.fetonMom.service.SettingsContentObserver;
import com.carenx.fetonMom.service.UploadService;
import com.carenx.fetonMom.service.marker.BluetoothLEService;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static com.google.android.gms.location.LocationServices.*;

//import com.carenx.fetonMom.adapters.FireBaseRecyclerAdapter;

public class HomeActivity extends AppCompatActivity
        implements NavigationRecylerViewAdapter.NavigationClickListener,
        View.OnClickListener,
        NstFragment.NstTestCompletionListener,
        HistoryFragment.OnClickEventListener {

    public static String AUDIO_RECORDER_FOLDER = "feton";
    private final static int REQUEST_ENABLE_BT = 1001;
    public static FirebaseFirestore mDatabase;
    public static FirebaseAuth mAuth;
    public static boolean showMenuItems = false;
    public static boolean showMenuItemsNST = false;

    // this listener will be called when there is change in firebase user session
    FirebaseAuth.AuthStateListener authListener = new FirebaseAuth.AuthStateListener() {
        @Override
        public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
            FirebaseUser user = firebaseAuth.getCurrentUser();
            if (user == null) {
                // user auth state is changed - user is null
                // launch login activity
                startActivity(new Intent(HomeActivity.this, LoginActivity.class));
                finish();
            }
        }
    };
    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;
    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    private long exitTime = 0;
    private NstFragment.NstTestCompletionListener nstListener;
    private BroadcastReceiver mBroadcastReceiver;
    private String TAG = HomeActivity.class.getSimpleName();
    private ProgressDialog mProgressDialog;
    private String[] tabTitles = {"NST", "History"};
    private int[] tabIcons = {android.R.drawable.ic_menu_always_landscape_portrait, android.R.drawable.ic_menu_recent_history};
    private MediaSession audioSession;
    private AudioManager mAudioManager;
    private SettingsContentObserver mSettingsContentObserver;
    public static RemoteControl.Client mRemoteControlClient;
    public static String motherId;
    public static String motherName;
    static String externalStoragePath;
    public static int gestAge;
    public static String mobileNo;
    private BluetoothAdapter mBluetoothAdapter;
    private static boolean hideMenuOptions = false;
    public static String doctorName;
    public static String hospitalName;
    public static int motherWeight;
    public static boolean recording = false;
    private SharedPreferences prefs;
    private static String doctorId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseFirestore.getInstance();

        setContentView(R.layout.activity_home);

        nstListener = this;
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        Toolbar toolbar2 = (Toolbar) findViewById(R.id.toolbar2);
        toolbar.setLogo(R.drawable.ic_profile_black);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        motherId = getIntent().getExtras().getString("motherId");
        mobileNo = getIntent().getExtras().getString("motherMobileNo");
        motherName = getIntent().getExtras().getString("motherName");
        motherWeight = getIntent().getExtras().getInt("motherWeight");
        long motherLMP = getIntent().getExtras().getLong("motherLMP", -1);

        doctorName = getIntent().getExtras().getString("doctorName");
        doctorId = getIntent().getExtras().getString("doctorId");
        hospitalName = getIntent().getExtras().getString("hospitalName");


        gestAge = (int) ((System.currentTimeMillis() - motherLMP) / (1000 * 60 * 60 * 24));
        gestAge = gestAge / 7;

        getSupportActionBar().setTitle(motherName);


        AUDIO_RECORDER_FOLDER = "fetoscope/" + motherName.replace(" ", "_");


        externalStoragePath = Environment.getExternalStorageDirectory().getPath() + "/" + AUDIO_RECORDER_FOLDER;

        showMenuItemsNST = false;
        showMenuItems = false;


        //setting up the prefrences
        prefs = getSharedPreferences("preferences", MODE_PRIVATE);
        BeatsProcessor.setFilterOn(prefs.getBoolean("isFilterOn", true));
        BeatsProcessor.setUseWavelet(prefs.getBoolean("isUseWaveletOn", false));
        BeatsProcessor.setUseMic(prefs.getBoolean("isUseMicOn", true));
        BeatsProcessor.setPreAmpDefault(prefs.getFloat("preAmpDef", 10f));
        BeatsProcessor.setPostAmpDefault(prefs.getFloat("postAmpDef", 10f));
        BeatsProcessor.setUseMic(prefs.getBoolean("isUseMicOn", true));
        BeatsProcessor.setUseBluetooth(prefs.getBoolean("isUseBluetoothOn", true));
        NstView.setNstTime((int) prefs.getInt("nstTime", 10));
        NstView.setMinPerGridValue((prefs.getBoolean("is1CmOn", true) ? 1 : 3));

        if (prefs.getBoolean("isDemoOn", false)) {
            BeatsProcessor.setPreAmpDefault(1.5f);
            BeatsProcessor.setPostAmpDefault(1.5f);
        }
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);


        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.list);
        NavigationRecylerViewAdapter adapter = new NavigationRecylerViewAdapter(this);
        // use a linear layout manager
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL_LIST));

        recyclerView.setAdapter(adapter);
        mViewPager.setCurrentItem(0);
        setTitle(tabTitles[0]);
        UploadListener();
        mRemoteControlClient = new RemoteControl().getClient(this);
        mRemoteControlClient.initializeRemote();
        //bluetootButtonPress();
        /*mSettingsContentObserver = new SettingsContentObserver(this,new Handler());
        getApplicationContext().getContentResolver().registerContentObserver(android.provider.Settings.System.CONTENT_URI, true, mSettingsContentObserver );*/
        /*Intent intent = new Intent( getApplicationContext(), MediaSessionService.class );
        intent.setAction( MediaSessionService.ACTION_PLAY );
        startService( intent );*/

        bluetoothSetup();
        displayLocationSettingsRequest(this);
        initMarkerSetUp();

        for (int i = 0; i < toolbar2.getChildCount(); i++) {
            View view = toolbar2.getChildAt(i);
            view.setOnClickListener(this);
            if (view.getId() == R.id.ivAxis) {
                if (NstView.getMinPerGridValue() == 1)
                    ((ImageView) view).setImageResource(R.drawable.ic_axis_60);
                else
                    ((ImageView) view).setImageResource(R.drawable.ic_axis_20);
            }

        }
        /*for (int i = 0; i < toolbar.getChildCount(); i++) {
            View view = toolbar.getChildAt(i);
            view.setOnClickListener(this);

        }*/
    }

    void bluetoothSetup() {
        final BluetoothManager bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.ivAxis) {
            boolean val = prefs.getBoolean("is1CmOn", true);
            if (val) {
                NstView.setMinPerGridValue(3);
                ((ImageView) v).setImageResource(R.drawable.ic_axis_20);
            } else {
                NstView.setMinPerGridValue(1);
                ((ImageView) v).setImageResource(R.drawable.ic_axis_60);
            }
            prefs.edit().putBoolean("is1CmOn", !val).apply();
            //Toast.makeText(HomeActivity.this,"yes",Toast.LENGTH_SHORT).show();
        } else if (recording) {
            Toast.makeText(HomeActivity.this, "Try again after the test ends.", Toast.LENGTH_SHORT).show();
        } else if(v.getId() == android.R.id.home) {
            doExitApp();
        }else {
            Intent intent = new Intent(HomeActivity.this, RegistrationActivity.class);
            intent.putExtra("motherId", motherId);
            startActivity(intent);
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        /*getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);*/
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //getApplicationContext().getContentResolver().unregisterContentObserver(mSettingsContentObserver);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if (hideMenuOptions)
            return true;
        getMenuInflater().inflate(R.menu.menu_home, menu);
        MenuItem tab = menu.findItem(R.id.action_records);
        if (mViewPager.getCurrentItem() == 0) {
            tab.setIcon(R.drawable.ic_history_24dp_black);
        } else {
            tab.setIcon(R.drawable.ic_nst_24dp_black);
        }
        return true;
    }

   /* @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //Toast.makeText(this, "point", Toast.LENGTH_SHORT).show();

        if (keyCode == KeyEvent.KEYCODE_MEDIA_NEXT) {
            Log.i("movement","marker press home");
            // Do your thing
            sendVolumeBroadcast(KeyEvent.KEYCODE_MEDIA_NEXT);
            //Toast.makeText(this, "point", Toast.LENGTH_SHORT).show();
            return true;
        } else {
            return super.onKeyDown(keyCode, event);
        }


    }*/
/*

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        switch (keyCode) {
            case KeyEvent.KEYCODE_HEADSETHOOK:
            case KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE:
            case KeyEvent.KEYCODE_MEDIA_NEXT:
            case KeyEvent.KEYCODE_MEDIA_PREVIOUS:
                return MediaButtonReceiver.processKey(this, event);
        }

        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event)
    {
        switch (keyCode) {
            case KeyEvent.KEYCODE_HEADSETHOOK:
            case KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE:
            case KeyEvent.KEYCODE_MEDIA_NEXT:
            case KeyEvent.KEYCODE_MEDIA_PREVIOUS:
                return MediaButtonReceiver.processKey(this, event);
        }

        return super.onKeyUp(keyCode, event);
    }
*/


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            doExitApp();
            return true;
        } else if (id == R.id.action_records) {
            if (mViewPager.getCurrentItem() == 0)
                mViewPager.setCurrentItem(1);
            else
                mViewPager.setCurrentItem(0);
            return true;
        /*} else if (id == R.id.action_settings) {
            startActivity(new Intent(HomeActivity.this, SettingsActivity.class));
            return true;*/
        } else if (id == R.id.action_sign_out) {
            mAuth.signOut();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void OnNavigationItemClick(int position) {
        mViewPager.setCurrentItem(position);
        setTitle(tabTitles[position]);
    }


    private void sendVolumeBroadcast(int key) {
        Intent intent = new Intent(NstView.NST_MOVEMENTS);
        intent.putExtra("key", key);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_ENABLE_BT) {
            final BluetoothManager bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
            mBluetoothAdapter = bluetoothManager.getAdapter();
            if (mBluetoothAdapter == null || !mBluetoothAdapter.isEnabled()) {
                finish();
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(authListener);

        // Register receiver for uploads and downloads
        LocalBroadcastManager manager = LocalBroadcastManager.getInstance(this);
        manager.registerReceiver(mBroadcastReceiver, DownloadService.getIntentFilter());
        manager.registerReceiver(mBroadcastReceiver, UploadService.getIntentFilter());

        // detect Bluetooth enabled
        if (mBluetoothAdapter == null || !mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }

        // bind service
        bindService(new Intent(getApplicationContext(), BluetoothLEService.class), serviceConnection, BIND_AUTO_CREATE);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (authListener != null) {
            mAuth.removeAuthStateListener(authListener);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            //audioSession.release();
        }

        // Unregister download receiver
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mBroadcastReceiver);
        //unbind servive marker
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mBluetoothAdapter.getBluetoothLeScanner().stopScan(scanCallback);
        }else {
            mBluetoothAdapter.stopLeScan(mLeScanCallback);
        }
        if (!BeatsRecorder.getIsBluetoothMarkerConnected()) {
            sendBroadcast(new Intent(BluetoothDeviceConnectionReciver.CUSTOM_ACTION_ON_DISCONNECTED));
        }
        mHandler.removeCallbacks(stopScan);
        mHandler.removeCallbacks(startScan);
        unbindService(serviceConnection);
    }

    @Override
    public void onNstTestStarted() {
        hideMenuOptions = true;
        invalidateOptionsMenu();
    }

    @Override
    public void onNstTestFinished(String audioPath, String imagePath, String id) {


        if (imagePath != null)
            writeNewFileMeta(audioPath, imagePath, id);

        hideMenuOptions = false;
        invalidateOptionsMenu();
    }

    @Override
    public void onNstTestFinished(String id, String audioPath, String imagePath, ArrayList<Integer> bpm, ArrayList<Integer> movements) {
        if (bpm != null)
            writeNewFileMeta(id, audioPath, imagePath, bpm, movements);

        hideMenuOptions = false;
        invalidateOptionsMenu();
    }

    public static String getExternalStoragePath() {
        File f = new File(externalStoragePath);
        if (!f.exists())
            f.mkdirs();
        return externalStoragePath;
    }


    private void writeNewFileMeta(String localAudioPath, String localImagePath, String id) {
        /*NSTTest test = new NSTTest(id, localImagePath, false, localAudioPath, false);
        String doctorId = mAuth.getCurrentUser().getUid();
        DocumentReference reference = mDatabase.collection("Tests").document();
        test.setTestByMother(false);
        test.setId(reference.getId());
        test.setMotherId(motherId);
        test.
        reference.set(test);*/
    }

    private void writeNewFileMeta(String id, String localAudioPath, String localImagePath, ArrayList<Integer> bpm, ArrayList<Integer> movements) {

        if(bpm.size()==0)
            return;
        DocumentReference reference = mDatabase.collection("Tests").document();
        NSTTest test = new NSTTest();
        test.setId(reference.getId());
        test.setMotherId(motherId);
        test.setDoctorId(doctorId);
        test.setBpmEntries(bpm);
        test.setMovementEntries(movements);
        test.setLengthOfTest(bpm.size());
        test.setTestByMother(true);
        test.setCreatedOn(System.currentTimeMillis());
        test.setCreatedBy(doctorId);

        test.setMotherName(motherName);
        test.setDoctorName(doctorName);
        test.setHospitalName(hospitalName);
        test.setgAge(gestAge);
        test.setWeight(motherWeight);

        test.setImageLocalPath(localImagePath);
        test.setAudioLocalPath(localAudioPath);

        reference.set(test);
    }

    @Override
    public void OnFhrItemClick(String path) {
        /*Intent intent = new Intent(HomeActivity.this, DetailsActivity.class);
        intent.putExtra("path", path);
        startActivity(intent);*/
        onBackPressed();
    }

    @Override
    public void OnFhrItemStatusClick(NSTTest file, int type) {
        if (type == 0) {
            Toast.makeText(this, "File is all ready synced", Toast.LENGTH_SHORT).show();
            //return;
        } else if (type == 1)
            uploadFromUri(file);
        else if (type == 2)
            beginDownload(file);
        else
            Toast.makeText(this, "File is not uploaded", Toast.LENGTH_SHORT).show();
        //Toast.makeText(this, file.getAudioLocalPath(), Toast.LENGTH_SHORT).show();
        //showProgressDialog();
    }

    @Override
    public void OnFrrItemsSelected(int size) {
        if (size > 0) {
            showMenuItems = true;
        } else {
            showMenuItems = false;
        }
        supportInvalidateOptionsMenu();
    }

    public void setTitle(String title) {
        getSupportActionBar().setSubtitle(title);
    }

    @Override
    public void onBackPressed() {
        doExitApp();
    }

    public void doExitApp() {
        if (recording)
            return;
        if (mViewPager.getCurrentItem() == 1) {
            mViewPager.setCurrentItem(0);
        } else if ((System.currentTimeMillis() - exitTime) > 2000) {
            Toast.makeText(this, R.string.press_again_exit_app, Toast.LENGTH_SHORT).show();
            exitTime = System.currentTimeMillis();
        } else {
            finish();
        }
    }

    private void beginDownload(NSTTest files) {

        Uri audio = Uri.parse(files.getAudioFirePath());
        Uri image = Uri.parse(files.getImageFirePath());
        //Uri localFile = Uri.parse(files.getAudioLocalPath());

        /*String path =  Environment.getExternalStorageDirectory().getPath() + "/"
                + AUDIO_RECORDER_FOLDER + "/" + motherName+"/"+p.getLastPathSegment();*/
        String audioPath = audio.getLastPathSegment();
        String imagePath = image.getLastPathSegment();
        //String folderPath = localFile.getPath();

        //File dir = new File(folderPath);
        //dir.mkdirs();

        // Kick off DownloadService to download the file
        /*Intent intent = new Intent(this, DownloadService.class)
                .putExtra(DownloadService.EXTRA_DOWNLOAD_PATH, audioPath)
                .putExtra(DownloadService.EXTRA_LOCAL_PATH, getExternalStoragePath())
                .putExtra(DownloadService.EXTRA_DOWNLOAD_ID, files.getId())
                .putExtra(DownloadService.EXTRA_FILE_TYPE, DownloadService.EXTRA_FILE_AUDIO)
                .setAction(DownloadService.ACTION_DOWNLOAD);
        startService(intent);*/

        Intent intent2 = new Intent(this, DownloadService.class)
                .putExtra(DownloadService.EXTRA_DOWNLOAD_PATH, imagePath)
                .putExtra(DownloadService.EXTRA_LOCAL_PATH, getExternalStoragePath())
                .putExtra(DownloadService.EXTRA_DOWNLOAD_ID, files.getId())
                .putExtra(DownloadService.EXTRA_FILE_TYPE, DownloadService.EXTRA_FILE_IMAGE)
                .setAction(DownloadService.ACTION_DOWNLOAD);
        startService(intent2);

        // Show loading spinner
        showProgressDialog("downloading");
    }


    private void uploadFromUri(NSTTest file) {
        Log.d(TAG, "uploadFromUri:src:" + file.getAudioLocalPath() + " " + file.getId());

        // Toast message in case the user does not see the notificatio
        Toast.makeText(this, "Uploading...", Toast.LENGTH_SHORT).show();

        // Start MyUploadService to upload the file, so that the file is uploaded
        // even if this Activity is killed or put in the background
        startService(new Intent(this, UploadService.class)
                .putExtra(UploadService.EXTRA_FILE_URI, file.getImageLocalPath())
                .putExtra(UploadService.EXTRA_FILE_ID, file.getId())
                .putExtra(UploadService.EXTRA_FILE_TYPE, UploadService.EXTRA_FILE_IMAGE)
                .putExtra(UploadService.EXTRA_FILE_MOTHER_ID, motherId)
                .setAction(UploadService.ACTION_UPLOAD));

        /*startService(new Intent(this, UploadService.class)
                .putExtra(UploadService.EXTRA_FILE_URI, file.getAudioLocalPath())
                .putExtra(UploadService.EXTRA_FILE_ID, file.getId())
                .putExtra(UploadService.EXTRA_FILE_TYPE, UploadService.EXTRA_FILE_AUDIO)
                .setAction(UploadService.ACTION_UPLOAD));
        */
        showProgressDialog("uploading");
    }

    private void UploadListener() {
        // Local broadcast receiver
        mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d(TAG, "onReceive:" + intent);
                hideProgressDialog();

                switch (intent.getAction()) {
                    case DownloadService.DOWNLOAD_COMPLETED:

                        Toast.makeText(HomeActivity.this, getString(R.string.success), Toast.LENGTH_SHORT).show();
                        // Alert success
                        //showMessageDialog(getString(R.string.success), String.format(Locale.getDefault(),
                        //       "downloaded to %s", intent.getStringExtra(DownloadService.EXTRA_DOWNLOAD_PATH)));
                        break;
                    case DownloadService.DOWNLOAD_ERROR:
                        // Alert failure
                        showMessageDialog("Error", String.format(Locale.getDefault(),
                                "Failed to download from %s",
                                intent.getStringExtra(DownloadService.EXTRA_DOWNLOAD_PATH)));
                        break;
                    case UploadService.UPLOAD_COMPLETED:
                    case UploadService.UPLOAD_ERROR:
                        onUploadResultIntent(intent);
                        break;
                }
            }
        };
    }

    private void onUploadResultIntent(Intent intent) {
        // Got a new intent from MyUploadService with a success or failure
        Parcelable mDownloadUrl = intent.getParcelableExtra(UploadService.EXTRA_DOWNLOAD_URL);
        Parcelable mFileUri = intent.getParcelableExtra(UploadService.EXTRA_FILE_URI);
        String type = intent.getStringExtra(UploadService.EXTRA_FILE_TYPE);
        String mFileId = intent.getStringExtra(UploadService.EXTRA_FILE_ID);
        String mId = intent.getStringExtra(UploadService.EXTRA_FILE_MOTHER_ID);


        updateDatabase(mDownloadUrl, mFileUri, mId, mFileId, type);


        //updateUI(mAuth.getCurrentUser());
    }

    private void updateDatabase(final Parcelable fireUri, Parcelable localUri, String mId, String id, final String type) {

        /*DatabaseReference reference = mDatabase.collection("Tests").document(mAuth.getCurrentUser()
                .getUid()).child(mId).child(id).getRef();
        try {
            reference.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot snapshot) {

                    DatabaseReference file = snapshot.getRef();
                    try {
                        if (type.equals(UploadService.EXTRA_FILE_AUDIO)) {
                            file.child("audioFirePath").setValue(fireUri.toString());
                            file.child("audioSynced").setValue(true);
                        } else {
                            file.child("imageFirePath").setValue(fireUri.toString());
                            file.child("imgSynced").setValue(true);
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }

                @Override
                public void onCancelled(DatabaseError error) {
                    Log.w(TAG, "Failed to read value.", error.toException());
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
*/

    }

    private void showProgressDialog(String status) {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage(status + "...");
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.show();
    }

    private void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    private void showMessageDialog(String title, String message) {
        AlertDialog ad = new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(message)
                .create();
        ad.show();
    }

    private void displayLocationSettingsRequest(Context context) {
        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(context)
                .addApi(API).build();
        googleApiClient.connect();

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(10000 / 2);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result = SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        Log.i(TAG, "All location settings are satisfied.");
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        Log.i(TAG, "Location settings are not satisfied. Show the user a dialog to upgrade location settings ");

                        try {
                            // Show the dialog by calling startResolutionForResult(), and check the result
                            // in onActivityResult().
                            status.startResolutionForResult(HomeActivity.this, 8878);
                        } catch (IntentSender.SendIntentException e) {
                            Log.i(TAG, "PendingIntent unable to execute request.");
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        Log.i(TAG, "Location settings are inadequate, and cannot be fixed here. Dialog not created.");
                        break;
                    case LocationSettingsStatusCodes.CANCELED:
                        finish();
                        break;
                }
            }
        });
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            switch (position) {
                case 0:
                    return NstFragment.newInstance(nstListener);
                case 1:
                    return HistoryFragment.newInstance(motherId);

            }
            return NstFragment.newInstance(nstListener);
        }

        @Override
        public int getCount() {
            // Show 4 total pages.
            return 2;
        }


        @Override
        public CharSequence getPageTitle(int position) {

            return tabTitles[position];
        }

    }


    private BluetoothLEService service;
    private Handler mHandler;

    private Runnable stopScan = new Runnable() {
        @Override
        public void run() {
            mHandler.removeCallbacks(stopScan);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                mBluetoothAdapter.getBluetoothLeScanner().stopScan(scanCallback);
            }else {
                mBluetoothAdapter.stopLeScan(mLeScanCallback);
            }
            if (!BeatsRecorder.getIsBluetoothMarkerConnected()) {
                sendBroadcast(new Intent(BluetoothDeviceConnectionReciver.CUSTOM_ACTION_ON_DISCONNECTED));
            }

        }
    };

    private Runnable startScan = new Runnable(){
        @Override
        public void run() {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                mBluetoothAdapter.getBluetoothLeScanner().startScan(scanCallback);
            } else {
                mBluetoothAdapter.startLeScan(mLeScanCallback);
            }
            mHandler.postDelayed(stopScan, SCAN_PERIOD);
            mHandler.removeCallbacks(startScan);
        }
    };

    private static final long SCAN_PERIOD = 10000; // 10 seconds

    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            if (iBinder instanceof BluetoothLEService.BackgroundBluetoothLEBinder) {
                service = ((BluetoothLEService.BackgroundBluetoothLEBinder) iBinder).service();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            Log.d(BluetoothLEService.TAG, "onServiceDisconnected()");
        }
    };

    private void initMarkerSetUp() {

        mHandler = new Handler();
    }

    private BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback() {

        @Override
        public void onLeScan(BluetoothDevice device, int rssi, byte[] scanRecord) {
            final String address = device.getAddress();
            final String name = device.getName();

            Log.d(TAG, "device " + name + " with address " + address + " found");
            /*if (!Devices.containsDevice(MarkerConfigActivity.this, address)) {
                devices.put((name == null) ? address : name, address);
            }*/


            if (name != null && name.trim().equalsIgnoreCase("iTag")) {
                selectDevice(name, address);
            }
        }
    };

    private void selectDevice(String name, String address) {
        //Devices.insert(this, name, address);
        //Devices.setEnabled(this, address, true);

        Log.d("Marker", "device " + name + " with address " + address + " found");
        service.connect(address);
    }


    public void onScanStart() {

        //mBluetoothAdapter.startLeScan(mLeScanCallback);

        mHandler.postDelayed(startScan,1000);


    }
    private ScanCallback scanCallback = new ScanCallback() {
        @TargetApi(Build.VERSION_CODES.LOLLIPOP)
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            super.onScanResult(callbackType, result);
            final String address = result.getDevice().getAddress();
            final String name = result.getDevice().getName();

            Log.d(TAG, "device " + name + " with address " + address + " found");
            /*if (!Devices.containsDevice(MarkerConfigActivity.this, address)) {
                devices.put((name == null) ? address : name, address);
            }*/


            if (name != null && name.trim().equalsIgnoreCase("iTag")) {
                selectDevice(name, address);
            }
        }

        @Override
        public void onBatchScanResults(List<ScanResult> results) {
            super.onBatchScanResults(results);
        }

        @Override
        public void onScanFailed(int errorCode) {
            super.onScanFailed(errorCode);
        }
    };


}
