package com.carenx.fetonMom.activities;

import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

//import com.carenx.beats.remote.services.BluetoothLEService;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;

/**
 * Created by avinash on 4/7/17.
 */

public class MyApp extends Application {

    private static MyApp myApplication;
    private SharedPreferences mPrefs;
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;

    @Override
    public void onCreate() {
        super.onCreate();
        Fresco.initialize(this);
        /*if (!isMyServiceRunning(BluetoothLEService.class)) {
            startService(new Intent(this, BluetoothLEService.class));
        }*/
        myApplication = this;
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {

                return true;

            }
        }
        return false;
    }

    public static synchronized MyApp getInstance() {
        if (myApplication == null) {
            myApplication = new MyApp();
        }
        return myApplication;
    }

    public SharedPreferences getSharedPreferences() {
        if (mPrefs == null)
            mPrefs = getSharedPreferences("feton", MODE_PRIVATE);
        return mPrefs;

    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }
}
