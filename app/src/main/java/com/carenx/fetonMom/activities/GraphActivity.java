package com.carenx.fetonMom.activities;

import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.carenx.beatsv3.BeatsRecorder;
import com.carenx.beatsv3.FHRGraphView;
import com.carenx.beatsv3.GraphExtraData;
import com.carenx.beatsv3.NstView;
import com.carenx.fetonMom.BuildConfig;
import com.carenx.fetonMom.R;
import com.carenx.fetonMom.Utils.GraphView;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import static com.carenx.fetonMom.activities.HomeActivity.AUDIO_RECORDER_FOLDER;

public class GraphActivity extends AppCompatActivity implements View.OnClickListener {

    private SharedPreferences prefs;
    private GraphView graphView;

    public static String getImageFilename() {
        String filepath = Environment.getExternalStorageDirectory().getPath();
        File file = new File(filepath, BeatsRecorder.AUDIO_RECORDER_FOLDER);
        if (!file.exists()) {
            file.mkdirs();
        }

        SimpleDateFormat format = new SimpleDateFormat("ddMMyyyy_HHmmss_SSS", Locale.ENGLISH);
        Date now = Calendar.getInstance().getTime();
        return file.getAbsolutePath() + "/test_" + format.format(now) + ".jpg";
    }

    public static String getPDFFilename() {
        String filepath = Environment.getExternalStorageDirectory().getPath();
        File file = new File(filepath, AUDIO_RECORDER_FOLDER);
        if (!file.exists()) {
            file.mkdirs();
        }

        SimpleDateFormat format = new SimpleDateFormat("ddMMyyyy_HHmmss_SSS", Locale.ENGLISH);
        Date now = Calendar.getInstance().getTime();
        return file.getAbsolutePath() + "/test_" + format.format(now) + ".pdf";
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_nst, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_share:
                shareFiles();
                return true;
            case R.id.action_print:
                printFiles();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graph_view);

        //setting up the prefrences
        prefs = getSharedPreferences("preferences", MODE_PRIVATE);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        toolbar.setTitle(GraphView.mTest.getMotherName());
        toolbar.setSubtitle(new Date(GraphView.mTest.getCreatedOn()).toString());


        graphView = findViewById(R.id.graphView);

        ImageView iv = findViewById(R.id.ivAxis);
        iv.setOnClickListener(this);
        if (graphView.getGridPreMin() == 1)
            iv.setImageResource(R.drawable.ic_axis_60);
        else
            iv.setImageResource(R.drawable.ic_axis_20);
        /*for (int i = 0; i < get.getChildCount(); i++) {
            View view = toolbar.getChildAt(i);

            view.setOnClickListener(this);
            if (view.getId() == R.id.ivAxis) {
                view.setOnClickListener(this);
                if (NstView.getMinPerGridValue() == 1)
                    ((ImageView) view).setImageResource(R.drawable.ic_axis_60);
                else
                    ((ImageView) view).setImageResource(R.drawable.ic_axis_20);
            }

        }*/

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.ivAxis) {
            boolean val = prefs.getBoolean("is1CmOn", true);
            if (val) {
                //GraphView.gridPreMin = 3;
                graphView.setGridPreMin(3);
                NstView.setMinPerGridValue(3);
                ((ImageView) v).setImageResource(R.drawable.ic_axis_20);
            } else {

                graphView.setGridPreMin(1);
                NstView.setMinPerGridValue(1);
                //GraphView.gridPreMin = 1;
                ((ImageView) v).setImageResource(R.drawable.ic_axis_60);
            }
            prefs.edit().putBoolean("is1CmOn", !val).apply();
            //Toast.makeText(HomeActivity.this,"yes",Toast.LENGTH_SHORT).show();
        }
    }

    private void shareFiles() {
        try {
            String path = BeatsRecorder.createNstPdf();
            File file = new File(path);
            Uri uri = FileProvider.getUriForFile(this,
                    BuildConfig.APPLICATION_ID + ".provider", file);

            // share
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_SEND);
            intent.setType("application/pdf");
            intent.putExtra(Intent.EXTRA_STREAM,uri);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            startActivity(intent);
            //adapter.notifyDataSetChanged();
        } catch (Exception ex) {
            ex.printStackTrace();
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            if(GraphView.mTest!=null)
                GraphView.mTest = null;
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    private void printFiles() {

        try {

            String path = createNstPdf();
            File file = new File(path);
            Uri uri = FileProvider.getUriForFile(this,
                    BuildConfig.APPLICATION_ID + ".provider", file);


            // View
            Intent intent = new Intent();
            //intent.setAction("org.androidprinting.intent.action.PRINT");//Intent.ACTION_VIEW);
            intent.setAction(Intent.ACTION_VIEW);
            intent.setDataAndType(uri, "application/pdf");
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            startActivity(intent);
            //adapter.notifyDataSetChanged();

        } catch (Exception ex) {
            ex.printStackTrace();
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG).show();
        }
    }

    public String[] getBitmapFromView() {

        //Define a bitmap with the same size as the view
        //Bitmap b = nstView.getParentView().getDrawingCache();
        com.carenx.beatsv3.FHRGraphView graphView = new FHRGraphView();

        GraphExtraData graphdata = new GraphExtraData(GraphView.mTest.getMotherName(),GraphView.mTest.getDoctorName(),
                GraphView.mTest.getHospitalName(),GraphView.mTest.getgAge()+"",GraphView.mTest.getWeight()+"");

        Bitmap b[] = graphView.getNSTGraph(GraphView.mTest.getBpmEntries(), GraphView.mTest.getBpmEntries(), graphdata );
        String imgPath[] = new String[b.length];
        for (int i = 0; i < b.length; i++) {


            imgPath[i] = getImageFilename();
            File file = new File(imgPath[i]);

            try {
                OutputStream os = new FileOutputStream(file);
                b[i].compress(Bitmap.CompressFormat.JPEG, 60, os);
                os.flush();
                os.close();
                ContentValues image = new ContentValues();
                image.put(MediaStore.Images.Media.TITLE, "NST");
                image.put(MediaStore.Images.Media.DISPLAY_NAME, imgPath[i].substring(imgPath[i].lastIndexOf('/') + 1));
                image.put(MediaStore.Images.Media.DESCRIPTION, "App Image");
                image.put(MediaStore.Images.Media.DATE_ADDED, System.currentTimeMillis());
                image.put(MediaStore.Images.Media.MIME_TYPE, "image/jpg");
                image.put(MediaStore.Images.Media.ORIENTATION, 0);
                File parent = file.getParentFile();
                image.put(MediaStore.Images.ImageColumns.BUCKET_ID, parent.toString()
                        .toLowerCase().hashCode());
                image.put(MediaStore.Images.ImageColumns.BUCKET_DISPLAY_NAME, parent.getName()
                        .toLowerCase());
                image.put(MediaStore.Images.Media.SIZE, file.length());
                image.put(MediaStore.Images.Media.DATA, file.getAbsolutePath());
                Uri result = getContentResolver().insert(
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI, image);
            } catch (FileNotFoundException fnfe) {
                fnfe.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        return imgPath;
    }

    public  String createNstPdf() {
        if (GraphView.mTest.getBpmEntries() != null && GraphView.mTest.getBpmEntries().size() == 0)
            return null;
        String dirpath = getPDFFilename();
        try {
            Document document = new Document(PageSize.A4.rotate());
            PdfWriter.getInstance(document, new FileOutputStream(dirpath)); //  Change pdf's name.
            document.open();


            String paths[] = getBitmapFromView();
            Image img[] = new Image[paths.length];  // Change image's name and extension.
            for (int i = 0; i < paths.length; i++) {
                img[i] = Image.getInstance(paths[i]);


                float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
                        - document.rightMargin() - 0) / img[i].getWidth()) * 100; // 0 means you have no indentation. If you have any, change it.
                img[i].scalePercent(scaler);


                img[i].setAlignment(Image.ALIGN_CENTER | Image.ALIGN_TOP);
                //img.setAlignment(Image.LEFT| Image.TEXTWRAP);

            /* float width = document.getPageSize().width() - document.leftMargin() - document.rightMargin();
            float height = document.getPageSize().height() - document.topMargin() - document.bottomMargin();
            img.scaleToFit(width, height)*/

                document.add(img[i]);
            }
            document.close();
        } catch (BadElementException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return dirpath;
    }
}
