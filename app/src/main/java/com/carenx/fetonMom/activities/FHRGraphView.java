package com.carenx.fetonMom.activities;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.RectF;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by avinash on 24/11/17.
 */

public class FHRGraphView extends View {

    private float pixelsPerOneMM;
    private float pixelsPerOneCM;
    public String strGrapTitle = "Test";

    float touched_down=0;
    float touched_up=0;
    private Paint graphOutlines;
    private Paint graphGridLines;
    private Paint graphBackGround;
    private Paint graphSafeZone;
    private Paint graphAxisText;
    private Paint graphGridSubLines;
    private float yDivLength;
    private float yOrigin;
    private float axisFontSize;
    private float paddingLeft;
    private float paddingTop;
    private float paddingBottom;
    private float paddingRight;
    private float yAxisLabelWidth;
    private float xOrigin;
    private Paint graphBpmLine;
    private float xDivLength;
    private float xDiv;
    private int screenHeight;
    private int screenWidth;
    private float xAxisLength;
    private float yDiv;
    private float yAxisLength;
    private ArrayList<Integer> bpmList;

    public FHRGraphView(Context context) {
        super(context);

        /*pixelsPerOneCM = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_MM, 10,
                getResources().getDisplayMetrics());

        pixelsPerOneCM/=2;
        pixelsPerOneMM = pixelsPerOneCM/10;
        strGrapTitle ="FHR Graph : "+pixelsPerOneCM;*/
        initialize();
    }

    public FHRGraphView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public FHRGraphView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    public void initialize(){

        pixelsPerOneCM = 105.88228f;
        pixelsPerOneMM = 10.588228f;

        graphOutlines = new Paint();
        graphOutlines.setStrokeWidth(3f);
        graphOutlines.setColor(Color.BLACK);

        graphGridLines = new Paint();
        graphGridLines.setStrokeWidth(2.5f);
        graphGridLines.setColor(Color.GRAY);

        graphGridSubLines = new Paint();
        graphGridSubLines.setStrokeWidth(1.5f);
        graphGridSubLines.setColor(Color.LTGRAY);

        graphBackGround = new Paint();
        graphBackGround.setColor(Color.WHITE);

        graphSafeZone = new Paint();
        graphSafeZone.setStrokeWidth(1f);
        graphSafeZone.setColor(Color.argb(40,100,200,0));

        graphAxisText = new Paint();
        graphAxisText.setStrokeWidth(1f);
        graphAxisText.setTextSize(40f);
        graphAxisText.setColor(Color.BLACK);

        graphBpmLine = new Paint();
        graphBpmLine.setColor(Color.BLUE);
        graphBpmLine.setStrokeWidth(5);
        graphBpmLine.setStyle(Style.STROKE);

        axisFontSize=pixelsPerOneMM*5;

        paddingLeft= pixelsPerOneCM/2;
        paddingTop=pixelsPerOneCM*3;
        paddingBottom=pixelsPerOneCM;
        paddingRight=pixelsPerOneCM;
        yAxisLabelWidth= pixelsPerOneCM;

    }

    @Override
    protected void onDraw(Canvas canvas) {
        // TODO Auto-generated method stub
        super.onDraw(canvas);

        canvas.drawPaint(graphBackGround);

        int i;
        screenHeight=getHeight();//1440
        screenWidth=getWidth();//2560
        Log.i("HeightWidth",screenHeight+" "+screenWidth+" "+pixelsPerOneMM);
        //set font size

        //int labelPaddingTop=pixelsPerOneMM;
        float yAxisPos=paddingLeft+yAxisLabelWidth;
        float xAxisPos=paddingBottom+axisFontSize;

        yAxisLength=screenHeight-xAxisPos-paddingTop;
        xAxisLength=screenWidth-yAxisPos-paddingRight;

        xOrigin=yAxisPos;
        yOrigin=screenHeight-xAxisPos;

        xDivLength= pixelsPerOneCM;//10.588228
        xDiv=(getWidth()-xOrigin-paddingRight)/pixelsPerOneCM;

        yDivLength=xDivLength/2;
        yDiv=(getHeight()-paddingTop-paddingBottom-axisFontSize)/pixelsPerOneCM*2;

        displayInformation(canvas);

        drawXAxis(canvas);

        drawYAxis(canvas);

        drawBpmLine(canvas);
        //invalidate();
    }

    private void drawXAxis(Canvas canvas){
        for(int j=1;j<2;j++){

            //canvas.drawLine(yAxisPos, paddingTop, yAxisPos, screenHeight-xAxisPos, paint);
            canvas.drawLine(xOrigin+((xDivLength/2)*j),
                    paddingTop,
                    xOrigin+((xDivLength/2)*j),
                    yOrigin, graphGridSubLines);


        }

        for(int i=1;i<=xDiv;i++){
            canvas.drawLine(xOrigin+(xDivLength*i), paddingTop,
                    xOrigin+(xDivLength*i), yOrigin, graphGridLines);

            for(int j=1;j<2;j++){
                canvas.drawLine(xOrigin+(xDivLength*i)+((xDivLength/2)*j),
                        paddingTop,
                        xOrigin+(xDivLength*i)+((xDivLength/2)*j),
                        yOrigin, graphGridSubLines);
            }

            //if(i!=1)
            canvas.drawText(String.format("%2d",i),
                    xOrigin+(xDivLength*i)-
                            (graphAxisText.measureText("00")/2) ,
                    yOrigin+axisFontSize*2, graphAxisText);


        }

    }

    private void drawYAxis(Canvas canvas){
        //y-axis outlines
        canvas.drawLine(xOrigin,yOrigin,screenWidth-paddingRight,
                yOrigin,graphOutlines);
        canvas.drawLine(xOrigin,paddingTop,screenWidth-paddingRight,
                paddingTop,graphOutlines);

        int interval=10;
        int ymin=50;
        int safeZoneMax = 160;

        //SafeZone
        RectF safeZoneRect = new RectF();
        safeZoneRect.set(xOrigin,
                (float)(yOrigin-yDivLength)-((float)(safeZoneMax-ymin)/interval)*yDivLength ,
                xOrigin+xAxisLength,
                yOrigin-yDivLength*8);//50
        canvas.drawRect(safeZoneRect, graphSafeZone);

        for(int i=1;i<=yDiv;i++){
            if(i%2==0) {
                canvas.drawLine(xOrigin, yOrigin - (yDivLength * i),
                        xOrigin + xAxisLength, yOrigin - (yDivLength * i), graphGridLines);

                canvas.drawText("" + (ymin + (interval * (i - 1))), paddingLeft,
                        yOrigin - (yDivLength * i) + axisFontSize / 2, graphAxisText);

                canvas.drawLine(xOrigin, yOrigin - (yDivLength * i)+yDivLength/2,
                        xOrigin + xAxisLength, yOrigin - (yDivLength * i)+yDivLength/2, graphGridSubLines);

            }else {
                canvas.drawLine(xOrigin, yOrigin - (yDivLength * i),
                        xOrigin + xAxisLength, yOrigin - (yDivLength * i), graphGridSubLines);
                canvas.drawLine(xOrigin, yOrigin - (yDivLength * i)+yDivLength/2,
                        xOrigin + xAxisLength, yOrigin - (yDivLength * i)+yDivLength/2, graphGridSubLines);
            }
        }



    }

    private void displayInformation(Canvas canvas){
        canvas.drawText("Hospital : ",
                pixelsPerOneCM ,
                pixelsPerOneCM,
                graphAxisText);
        canvas.drawText("Time : ",
                pixelsPerOneCM *5,
                pixelsPerOneCM,
                graphAxisText);

        canvas.drawText("Date : ",
                pixelsPerOneCM *10,
                pixelsPerOneCM,
                graphAxisText);

        canvas.drawText("X-Axis : 20 sec/div",
                pixelsPerOneCM *15,
                pixelsPerOneCM,
                graphAxisText);

        canvas.drawText("Y-Axis : 20 BPM/DIV",
                pixelsPerOneCM *20,
                pixelsPerOneCM,
                graphAxisText);

        canvas.drawText("Name : ",
                pixelsPerOneCM ,
                pixelsPerOneCM*2,
                graphAxisText);
        canvas.drawText("G Age : ",
                pixelsPerOneCM *5,
                pixelsPerOneCM *2,
                graphAxisText);

        canvas.drawText("Weight : ",
                pixelsPerOneCM *10,
                pixelsPerOneCM *2,
                graphAxisText);


    }

    private void drawBpmLine(Canvas canvas){
        if(bpmList==null)
            return;
        Path path = new Path();

        path.moveTo(xOrigin, getYValueFromBPM(bpmList.get(0)));
        float k = xOrigin+pixelsPerOneMM;//k <getWidth()-paddingLeft-paddingRight
        for(int i=1;i<bpmList.size();i++){

            path.lineTo(k, getYValueFromBPM(bpmList.get(i)));
            k+=pixelsPerOneMM/2;
        }

        canvas.drawPath(path, graphBpmLine);
    }

    public float random()
    {
        Random r=new Random();
        float next = 50+r.nextInt(120);
        Log.i("Random",next+"");
        next= (float) (next/2f * pixelsPerOneMM);

        Log.i("Random",next+"");
        return yOrigin-next;
    }
    private int scaleOrigin = 40;
    private float getYValueFromBPM(int bpm){
        float adjustedBPM = bpm - scaleOrigin;
        adjustedBPM=adjustedBPM/2; //scaled down version for mobile phone
        float y_value = yOrigin-(adjustedBPM * pixelsPerOneMM);
        Log.i("bpmvalue",bpm+" "+adjustedBPM+" "+y_value);
        return y_value;

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        // TODO Auto-generated method stub
        float swipe=getWidth()/4;
        if(event.getAction()==MotionEvent.ACTION_DOWN){
            touched_down=event.getX();

        }
        if(event.getAction()==MotionEvent.ACTION_UP){
            /*touched_up=event.getX();

            if(touched_up>(touched_down+swipe)){
                for(int i=0 ;i<n;i++){
                    if(history[i]<(value[i].length)-5)
                        history[i]++;
                }
            }else if(touched_down>(touched_up+swipe)){
                for(int i=0 ;i<n;i++){
                    if(history[i]>0)
                        history[i]--;
                }
            }

            touched_down=0;
            touched_up=0;*/

        }

        return true;
    }


    public void setBpmData(ArrayList<Integer> list) {
        bpmList = list;
        invalidate();
    }
}
