package com.carenx.fetonMom.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.carenx.fetonMom.R;
import com.carenx.fetonMom.model.Doctor;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

public class DoctorProfileActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private FirebaseFirestore mDatabase;
    private EditText hospital;
    private EditText name;
    private EditText mobile;
    private EditText email;
    private FloatingActionButton save;
    private String TAG = DoctorProfileActivity.class.getSimpleName();
    private String doctorId;
    private String motherId;
    private boolean handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseFirestore.getInstance();
        setContentView(R.layout.activity_doctor_profile);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        doctorId = getIntent().getStringExtra("doctorId");
        motherId = getIntent().getStringExtra("motherId");
        handler = getIntent().getBooleanExtra("handler",false);
        findViews();
        setListeners();
        setViews();
    }

    private void setViews() {
        Task<DocumentSnapshot> reference = mDatabase.collection("users").document(doctorId).get();

        try {
            reference.addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                @Override
                public void onSuccess(DocumentSnapshot documentSnapshot) {

                    try {

                        Doctor u = documentSnapshot.toObject(Doctor.class);
                        email.setText(u.getEmail());
                        name.setText(u.getEmail());
                        hospital.setText(u.getHospitalName());
                        mobile.setText(u.getMobileNo());

                        Toast.makeText(DoctorProfileActivity.this,"Retrieval Successful",Toast.LENGTH_SHORT).show();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }

            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void findViews() {
        name = (EditText) findViewById(R.id.edtNameOfDoctor);
        hospital = (EditText) findViewById(R.id.edtNameOfHospital);
        mobile = (EditText) findViewById(R.id.edtMobileNo);
        email = (EditText) findViewById(R.id.edtEmail);
        save = (FloatingActionButton) findViewById(R.id.fab);
        save.setVisibility(View.GONE);
    }

    private void setListeners() {

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(true) {
                    //long time = System.currentTimeMillis();
                    Doctor data = new Doctor();
                    //data.email = mAuth.getCurrentUser().getEmail();
                    data.setName(name.getText().toString());
                    data.setHospitalName(hospital.getText().toString());
                    data.setMobileNo(mobile.getText().toString());
                    updateDatabase(data);
                    finish();
                }
                //else
                //    Toast.makeText(RegistrationActivity.this, "Please fill the information", Toast.LENGTH_SHORT).show();
            }
        });

    }


    private void updateDatabase(final Doctor u) {

        Map<String,Object> map = new HashMap<>();
        map.put("name",u.getName());
        map.put("hospitalName",u.getHospitalName());
        map.put("mobileNo",u.getMobileNo());
        mDatabase.collection("users").document(doctorId).update(map);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if(handler){
            Intent intent = new Intent(DoctorProfileActivity.this, MotherMainActivity.class);
            intent.putExtra("user",motherId);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }else
            super.onBackPressed();
    }
}
