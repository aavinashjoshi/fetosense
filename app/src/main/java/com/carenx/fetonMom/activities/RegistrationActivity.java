package com.carenx.fetonMom.activities;


import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatRadioButton;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Toast;

import com.carenx.fetonMom.R;
import com.carenx.fetonMom.model.Mother;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.SetOptions;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class RegistrationActivity extends AppCompatActivity {

    private EditText lmp;
    private EditText name;
    private EditText age;
    private EditText email;
    private EditText mobile;
    private Calendar myCalendar;
    private DatePickerDialog.OnDateSetListener datePicker;
    private FloatingActionButton save;
    private AppCompatRadioButton female;
    private TextInputLayout textDob;
    private FirebaseFirestore mDatabase;
    private String TAG = RegistrationActivity.class.getSimpleName();
    private EditText weight;
    private String motherId;
    private int noOfTests = 0;
    private String doctorId;
    private Date lmpDate;
    private Mother mMother;
    private Date eddDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mDatabase = FirebaseFirestore.getInstance();
        setContentView(R.layout.activity_registration);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        myCalendar = Calendar.getInstance();

        datePicker = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {

                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = "dd/MM/yy"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());
                lmpDate = myCalendar.getTime();
                myCalendar.add(Calendar.MONTH,9);
                myCalendar.add(Calendar.DAY_OF_YEAR,9);
                eddDate = myCalendar.getTime();
                lmp.setText(sdf.format(lmpDate));
            }

        };

        doctorId = getIntent().getStringExtra("doctorId");
        motherId = getIntent().getStringExtra("motherId");

        findViews();
        setListeners();
        if (motherId != null && motherId.length() > 0) {
            setViews(motherId);
        }
    }


    private void setViews(String motherId) {
        Task<DocumentSnapshot> reference = mDatabase.collection("users").document(motherId).get();

        try {
            reference.addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                @Override
                public void onSuccess(DocumentSnapshot FirebaseFirestore) {

                    try {

                        mMother = FirebaseFirestore.toObject(Mother.class);
                        mobile.setText(mMother.getMobileNo());
                        name.setText(mMother.getName());
                        //textDob.setHint("Age");
                        //lmp.setText(m.getLmp()+"");
                        age.setText(mMother.getAge() + "");
                        weight.setText(mMother.getWeight() + "");
                        email.setText(mMother.getEmail());
                        lmpDate = mMother.getLmp();
                        eddDate = mMother.getEdd();
                        String myFormat = "MM/dd/yy"; //In which you need put here
                        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());
                        lmp.setText(sdf.format(lmpDate));
                        //noOfTests = m.getNoOfTest();


                        Toast.makeText(RegistrationActivity.this, "Retrieval Successful", Toast.LENGTH_SHORT).show();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }

            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void findViews() {
        name = (EditText) findViewById(R.id.edtNameOfMother);
        textDob = (TextInputLayout) findViewById(R.id.textDob);
        lmp = (EditText) findViewById(R.id.edtLmpDate);
        age = (EditText) findViewById(R.id.edtAge);
        weight = (EditText) findViewById(R.id.edtWeight);
        email = (EditText) findViewById(R.id.edtEmail);
        mobile = (EditText) findViewById(R.id.edtMobileNo);
        save = (FloatingActionButton) findViewById(R.id.fab);
    }

    private void setListeners() {

        Date today = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(today);
        c.add(Calendar.MONTH, -3); // Subtract 3 months after first trimester
        long maxDate = c.getTime().getTime(); // Twice!
        c.add(Calendar.MONTH, -7); // Subtract 7 months 3+7 = 10 months
        long minDate = c.getTime().getTime(); // Twice!

        final DatePickerDialog dpd = new DatePickerDialog(RegistrationActivity.this, datePicker, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH));
        dpd.getDatePicker().setMinDate(minDate);
        dpd.getDatePicker().setMaxDate(maxDate);
        lmp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dpd.show();
            }
        });

        age.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showNumberPicker();
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validate()) {
                    if (mMother == null)
                        mMother = new Mother();
                    mMother.setName(name.getText().toString());
                    mMother.setMobileNo(mobile.getText().toString());
                    mMother.setLmp(lmpDate);
                    mMother.setEdd(eddDate);
                    mMother.setEmail(email.getText().toString());
                    mMother.setAge(Integer.parseInt(age.getText().toString()));
                    mMother.setWeight(Integer.parseInt(weight.getText().toString()));
                    writeNewFileMeta(mMother);
                }
                //else
                //    Toast.makeText(RegistrationActivity.this, "Please fill the information", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    public void showNumberPicker() {

        final Dialog d = new Dialog(RegistrationActivity.this);
        d.setContentView(R.layout.dialog_number_picker);
        d.setTitle("Select Age");
        Button b1 = (Button) d.findViewById(R.id.apply_button);
        final NumberPicker np = d.findViewById(R.id.number_picker);
        np.setMaxValue(55);
        np.setMinValue(18);
        np.setWrapSelectorWheel(false);
        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int oldVal, int newVal) {
                age.setText(newVal + "");
            }

        });

        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                d.dismiss();
            }
        });
        d.show();
        d.getCurrentFocus();


    }

    private boolean validate() {

        if (name.getText().toString().isEmpty()) {
            name.setError("Name Required");
            return false;
        }
        if (lmp.getText().toString().isEmpty()) {
            lmp.setError("LMP Date Required");
            return false;
        }
        if (age.getText().toString().isEmpty() || age.getText().length() < 1) {
            age.setError("Age Required");
            return false;
        } else {
            try {
                int a = Integer.parseInt(age.getText().toString());
                if (a > 60 || a < 18) {
                    age.setError("Invalig Age");
                    return false;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                return false;
            }

        }
        if (weight.getText().toString().isEmpty() || weight.getText().length() < 2) {
            weight.setError("Weight Required");
            return false;
        } else {
            try {
                int a = Integer.parseInt(weight.getText().toString());
                if (a > 200 || a < 30) {
                    weight.setError("Invalid Weight");
                    return false;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                return false;
            }

        }
        if (!mobile.getText().toString().isEmpty() && mobile.getText().length() < 9) {
            mobile.setError("Invalid Mobile number");
            return false;
        }
        if (!email.getText().toString().isEmpty() &&
                !Patterns.EMAIL_ADDRESS.matcher(email.getText().toString()).matches()) {
            email.setError("Invalid email address");
            return false;
        }

        return true;
    }

    private void writeNewFileMeta(Mother mother) {
        DocumentReference reference;
        if (motherId == null || mother.getDocumentId()==null) {
            reference = mDatabase.collection("users").document();
            mother.setDocumentId(reference.getId());
            mother.setDoctorId(doctorId);
        } else
            reference = mDatabase.collection("users").document(mMother.getDocumentId());

        reference.set(mother, SetOptions.merge());

        Intent intent = new Intent(RegistrationActivity.this, MotherMainActivity.class);
        intent.putExtra("user",reference.getId());
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}