package com.carenx.fetonMom.model;

public class Doctor extends User {

    private String hospitalName;
    private String HospitalId;
    private int noOfMother;
    private int noOfTest;

    public String getHospitalName() {
        return hospitalName;
    }

    public void setHospitalName(String hospitalName) {
        this.hospitalName = hospitalName;
    }

    public String getHospitalId() {
        return HospitalId;
    }

    public void setHospitalId(String hospitalId) {
        HospitalId = hospitalId;
    }

    public int getNoOfMother() {
        return noOfMother;
    }

    public void setNoOfMother(int noOfMother) {
        this.noOfMother = noOfMother;
    }

    public int getNoOfTest() {
        return noOfTest;
    }

    public void setNoOfTest(int noOfTest) {
        this.noOfTest = noOfTest;
    }
}
