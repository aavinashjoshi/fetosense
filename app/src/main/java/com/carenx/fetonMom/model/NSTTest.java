package com.carenx.fetonMom.model;

/**
 * Created by avinash on 1/7/17.
 */
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.ArrayList;
import java.util.Date;

@IgnoreExtraProperties
public class NSTTest {

    private String id;
    private String motherId;
    private String doctorId;

    private int weight;
    private int gAge;

    private String motherName;
    private String doctorName;
    private String hospitalName;


    private String imageLocalPath;
    private String imageFirePath;
    private String audioLocalPath;
    private String audioFirePath;
    private  boolean isImgSynced;
    private  boolean isAudioSynced;

    private ArrayList<Integer> bpmEntries;
    private ArrayList<Integer> movementEntries;
    private int lengthOfTest;

    private  boolean testByMother;
    private long createdOn;
    private String createdBy;

    public String getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(String doctorId) {
        this.doctorId = doctorId;
    }

    public long getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(long createdOn) {
        this.createdOn = createdOn;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }


    public NSTTest() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMotherId() {
        return motherId;
    }

    public void setMotherId(String motherId) {
        this.motherId = motherId;
    }

    public String getImageLocalPath() {
        return imageLocalPath;
    }

    public void setImageLocalPath(String imageLocalPath) {
        this.imageLocalPath = imageLocalPath;
    }

    public String getImageFirePath() {
        return imageFirePath;
    }

    public void setImageFirePath(String imageFirePath) {
        this.imageFirePath = imageFirePath;
    }

    public String getAudioLocalPath() {
        return audioLocalPath;
    }

    public void setAudioLocalPath(String audioLocalPath) {
        this.audioLocalPath = audioLocalPath;
    }

    public String getAudioFirePath() {
        return audioFirePath;
    }

    public void setAudioFirePath(String audioFirePath) {
        this.audioFirePath = audioFirePath;
    }

    public boolean isImgSynced() {
        return isImgSynced;
    }

    public void setImgSynced(boolean imgSynced) {
        isImgSynced = imgSynced;
    }

    public boolean isAudioSynced() {
        return isAudioSynced;
    }

    public void setAudioSynced(boolean audioSynced) {
        isAudioSynced = audioSynced;
    }

    public ArrayList<Integer> getBpmEntries() {
        return this.bpmEntries;
    }

    public void setBpmEntries(ArrayList<Integer> bpmEntries) {
        this.bpmEntries = bpmEntries;
    }

    public ArrayList<Integer> getMovementEntries() {
        return this.movementEntries;
    }

    public void setMovementEntries(ArrayList<Integer> movementEntries) {
        this.movementEntries = movementEntries;
    }

    public int getLengthOfTest() {
        return lengthOfTest;
    }

    public void setLengthOfTest(int lengthOfTest) {
        this.lengthOfTest = lengthOfTest;
    }

    public boolean isTestByMother() {
        return testByMother;
    }

    public void setTestByMother(boolean testByMother) {
        this.testByMother = testByMother;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getgAge() {
        return gAge;
    }

    public void setgAge(int gAge) {
        this.gAge = gAge;
    }

    public String getMotherName() {
        return motherName;
    }

    public void setMotherName(String motherName) {
        this.motherName = motherName;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public String getHospitalName() {
        return hospitalName;
    }

    public void setHospitalName(String hospitalName) {
        this.hospitalName = hospitalName;
    }
}
