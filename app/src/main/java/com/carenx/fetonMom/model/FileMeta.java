package com.carenx.fetonMom.model;

/**
 * Created by avinash on 18/1/17.
 */
import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class FileMeta {
    public String id;
    public String localPath;
    public String firePath;
    public  boolean isSynced;

    public FileMeta() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public FileMeta(String id, String path, boolean isSynced) {
        this.id = id;
        this.localPath = path;
        this.firePath = "";
        this.isSynced = isSynced;
    }
}
