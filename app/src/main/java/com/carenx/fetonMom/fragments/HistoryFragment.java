package com.carenx.fetonMom.fragments;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Keep;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.carenx.beatsv3.NstView;
import com.carenx.fetonMom.BuildConfig;
import com.carenx.fetonMom.R;
import com.carenx.fetonMom.Utils.DividerItemDecoration;
import com.carenx.fetonMom.Utils.GraphView;
import com.carenx.fetonMom.activities.DetailsActivity;
import com.carenx.fetonMom.activities.GraphActivity;
import com.carenx.fetonMom.activities.HomeActivity;
import com.carenx.fetonMom.model.NSTTest;
import com.carenx.fetonMom.service.DownloadService;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;



/**
 * A simple {@link Fragment} subclass.
 * Use the {@link HistoryFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HistoryFragment extends Fragment {


    private static final String TAG = HistoryFragment.class.getSimpleName();
    public static ArrayList<NSTTest> selectedList = new ArrayList<>();
    private static OnClickEventListener listener;
    private RecyclerView recyclerView;
    //private FireBaseRecyclerAdapter adapter;
    private ArrayList<NSTTest> fireList;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter firebaseRecyclerAdapter;
    private Query reference;
    private BroadcastReceiver downloadReciver;
    private LocalBroadcastManager manager;
    private String motherId;
    private FirebaseFirestore mDatabase;

    public HistoryFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment HistoryFragment.
     * @param motherId
     */
    // TODO: Rename and change types and number of parameters
    public static HistoryFragment newInstance(String motherId) {
        HistoryFragment fragment = new HistoryFragment();
        Bundle bundle = new Bundle();
        bundle.putString("motherId",motherId);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        motherId = getArguments().getString("motherId");
        mDatabase = FirebaseFirestore.getInstance();
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_history, container, false);

        recyclerView = (RecyclerView) root.findViewById(R.id.list);
        //adapter = new FireBaseRecyclerAdapter(getActivity(), fireList, this);
        //setupListView();
        attachRecyclerViewAdapter();
        downloadReciver();
        return root;
    }

    private void downloadReciver() {
        downloadReciver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                switch (intent.getAction()) {
                    case DownloadService.DOWNLOAD_COMPLETED:
                        firebaseRecyclerAdapter.notifyDataSetChanged();
                }
            }

            ;
        };
        manager = LocalBroadcastManager.getInstance(getActivity());
        manager.registerReceiver(downloadReciver, DownloadService.getIntentFilter());
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);


        if (hidden) {
            manager.unregisterReceiver(downloadReciver);
        } else {
            manager.registerReceiver(downloadReciver, DownloadService.getIntentFilter());
        }
    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            listener = (OnClickEventListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement MyInterface ");
        }


    }

    private void attachRecyclerViewAdapter() {

        //mLayoutManager = new LinearLayoutManager(getActivity());
        mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST));

        reference = mDatabase.collection("Tests").whereEqualTo("motherId",motherId);
        FirestoreRecyclerOptions<NSTTest> options =
                new FirestoreRecyclerOptions.Builder<NSTTest>()
                        .setQuery(reference, NSTTest.class)
                        .setLifecycleOwner(this)
                        .build();

        firebaseRecyclerAdapter = newAdapter(options);

        /*// Scroll to bottom on new messages
        firebaseRecyclerAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                mLayoutManager.smoothScrollToPosition(recyclerView, null, firebaseRecyclerAdapter.getItemCount());
            }

        });*/

        recyclerView.setAdapter(firebaseRecyclerAdapter);

    }


    protected RecyclerView.Adapter newAdapter(FirestoreRecyclerOptions options) {

        return new FirestoreRecyclerAdapter<NSTTest, ViewHolder>(options) {
            @Override
            public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                return new ViewHolder(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_nst, parent, false));
            }

            @Override
            protected void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull NSTTest model) {
                //holder.bind(model);
                holder.mView.setTag(position);
                holder.file = model;

                //holder.mView.setTag(holder.path);
                holder.txtFhrName.setAlpha(0.5f);
                holder.ckhBoxSelected.setChecked(false);
                holder.imgSync.setImageResource(R.drawable.ic_file_upload_white_24dp);
                try {

                    String fileName = new Date(model.getCreatedOn()).toString();
                    fileName = fileName.replace(" GMT+05:30 "," ");
                    holder.txtFhrName.setText(fileName);
                            //String.format("Date : %s-%s-%s\nTime : %s:%s", fileName.substring(5, 7), fileName.substring(7, 9), fileName.substring(9, 13), fileName.substring(14, 16), fileName.substring(16, 18)));
                    /*File f = new File(HomeActivity.getExternalStoragePath(), fileName);
                    if (f.exists()) {
                        holder.txtFhrName.setText(String.format("Date : %s-%s-%s\nTime : %s:%s", fileName.substring(5, 7), fileName.substring(7, 9), fileName.substring(9, 13), fileName.substring(14, 16), fileName.substring(16, 18)));
                        if (model.isImgSynced()) {
                            holder.imgSync.setImageResource(R.drawable.ic_check_white_24);
                            holder.imgSync.setTag(0);
                        } else {
                            holder.imgSync.setImageResource(R.drawable.ic_file_upload_white_24dp);
                            holder.imgSync.setTag(1);
                        }
                    } else {
                        if (model.isImgSynced()) {
                            holder.txtFhrName.setText("Download");
                            holder.imgSync.setImageResource(R.drawable.ic_file_download_white_24dp);
                            holder.imgSync.setTag(2);
                        } else {
                            holder.txtFhrName.setText("File Not Uploaded");
                            holder.imgSync.setImageResource(R.drawable.ic_error_white_24dp);
                            holder.imgSync.setTag(-1);
                        }
                    }*/
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                if (selectedList.size() > 0) {
                    if (selectedList.contains(model)) {
                        holder.txtFhrName.setAlpha(1f);
                        holder.ckhBoxSelected.setChecked(true);
                    }
                }
            }

            @Override
            public void onDataChanged() {
                // If there are no chat messages, show a view that invites the user to add a message.
                //mEmptyListMessage.setVisibility(getItemCount() == 0 ? View.VISIBLE : View.GONE);
            }
        };
    }
    @Override
    public void onResume() {
        super.onResume();
        //getActivity().setTitle("Previous Recordings");
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private ArrayList<String> getFilesFromFolder() {

        ArrayList<String> mFiles = new ArrayList<>();
        File mDirectory;
        String folderPath = Environment.getExternalStorageDirectory().getPath() + "/" + HomeActivity.AUDIO_RECORDER_FOLDER;
        mDirectory = new File(folderPath);

        // Get the files in the directory
        File[] files = mDirectory.listFiles();
        if (files != null && files.length > 0) {
            for (File f : files) {
                if (f.getAbsolutePath().endsWith("wav")) {
                    mFiles.add(f.getAbsolutePath());
                }
            }

        }
        Collections.sort(mFiles, Collections.reverseOrder());
        return mFiles;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        // Inflate the menu; this adds items to the action bar if it is present.
        inflater.inflate(R.menu.history, menu);
        MenuItem share = menu.findItem(R.id.action_share);
        MenuItem delete = menu.findItem(R.id.action_delete);
        //MenuItem stetho = menu.findItem(R.id.stetho);
        if (HomeActivity.showMenuItems) {
            share.setVisible(true);
            delete.setVisible(true);
            //stetho.setVisible(false);
        } else {
            share.setVisible(false);
            delete.setVisible(false);
            //stetho.setVisible(true);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity_schedular in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id) {
            case R.id.action_share:
                shareFiles();
                return true;
            case R.id.action_delete:
                deleteFiles();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void shareFiles() {
        ArrayList<NSTTest> selectedPathList = selectedList;
        ArrayList<Uri> uriPathList = new ArrayList<>();
        //convert selected list string to uri
        for (NSTTest fileFile : selectedPathList) {
            File file = new File(fileFile.getImageLocalPath());
            Uri uri = FileProvider.getUriForFile(getActivity(),
                    BuildConfig.APPLICATION_ID + ".provider", file);
            /*
            Uri uri = FileProvider.getUriForFile(getActivity(),
                    BuildConfig.APPLICATION_ID + ".provider",file);
                    */
            uriPathList.add(uri);

        }
        selectedPathList.clear();
        // share audio
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND_MULTIPLE);
        intent.setType("application/pdf");
        intent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uriPathList);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        startActivity(intent);
        //adapter.notifyDataSetChanged();

    }

    private void deleteFiles() {
        ArrayList<NSTTest> selectedPathList = selectedList;
        for (NSTTest fireFile : selectedPathList) {
            try {
                File file = new File(fireFile.getImageLocalPath());
                file.delete();
            }catch (Exception ex){
                ex.printStackTrace();
            }
            try {
                File file2 = new File(fireFile.getAudioLocalPath());
                file2.delete();
            }catch (Exception ex){
                ex.printStackTrace();
            }
            mDatabase.collection("Tests").document(fireFile.getId()).delete();
        }
        selectedPathList.clear();

        //adapter.notifyDataSetChanged();
    }

    @Keep
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView txtFhrName;
        public final CheckBox ckhBoxSelected;
        public final ImageView imgSync;
        private final ImageView thumbnail;
        public NSTTest file;


        public ViewHolder(View view) {
            super(view);
            mView = view;
            txtFhrName = (TextView) view.findViewById(R.id.recName);
            ckhBoxSelected = (CheckBox) view.findViewById(R.id.ckhBoxSelected);
            imgSync = (ImageView) view.findViewById(R.id.imgSynced);
            thumbnail = (ImageView) itemView.findViewById(R.id.thumbnail);

            /*txtFhrName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null)
                        listener.OnFhrItemClick(file.getAudioLocalPath());
                }
            });*/

            mView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    ckhBoxSelected.performClick();
                    return true;
                }
            });

            ckhBoxSelected.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        txtFhrName.setAlpha(1f);
                        if (!HistoryFragment.selectedList.contains(file))
                            HistoryFragment.selectedList.add(file);
                    } else {
                        txtFhrName.setAlpha(0.5f);
                        if (HistoryFragment.selectedList.contains(file))
                            HistoryFragment.selectedList.remove(file);

                    }
                    if (listener != null)
                        listener.OnFrrItemsSelected(HistoryFragment.selectedList.size());
                }
            });


            mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //if (HistoryFragment.selectedList.size() == 0) {
                        try {
                            if(file.getBpmEntries()!=null && file.getBpmEntries().size()>0){
                                NstView.graphBpmEntries = file.getBpmEntries();
                                NstView.graphMovmentEntries = file.getMovementEntries();
                                NstView.time = file.getCreatedOn();

                                GraphView.mTest = file;
                                //listener.OnFhrItemClick("");
                                //dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_BACK));
                                v.getContext().startActivity(new Intent(v.getContext(), GraphActivity.class));
                            } else if (file.getImageLocalPath() != null && file.getImageLocalPath().length() > 10) {
                                File imgFile = new File(HomeActivity.getExternalStoragePath(), file.getImageLocalPath().substring(file.getImageLocalPath().lastIndexOf("/")));
                                Intent intent = new Intent();
                                intent.setAction(Intent.ACTION_VIEW);
                                //intent.setType("image/jpg");
                                if (file.getImageLocalPath().substring(file.getImageLocalPath().lastIndexOf(".")).contains("pdf"))
                                    intent.setDataAndType(FileProvider.getUriForFile(v.getContext(),
                                            BuildConfig.APPLICATION_ID + ".provider", imgFile), "application/pdf");
                                else
                                    intent.setDataAndType(FileProvider.getUriForFile(v.getContext(),
                                            BuildConfig.APPLICATION_ID + ".provider", imgFile), "image/*");
                                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);

                                //intent.setDataAndType(Uri.parse("file://" + file.getImageLocalPath()), "image/*");
                                //intent.setDataAndType(Uri.parse("file://" + file.getImageLocalPath()), "image/*");
                                v.getContext().startActivity(intent);
                            }
                        } catch (Exception ex) {
                            Toast.makeText(v.getContext(), "Image not found", Toast.LENGTH_SHORT).show();
                        }
                    /*} else {
                        ckhBoxSelected.performClick();
                    }*/


                }
            });


            thumbnail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(file.isTestByMother()) {
                        if (file.getAudioLocalPath() != null && file.getAudioLocalPath().length() > 10) {
                            File audio = new File(file.getAudioLocalPath());
                            if (audio.exists()) {
                                Intent intent = new Intent(v.getContext(), DetailsActivity.class);
                                intent.putExtra("path", file.getAudioLocalPath());
                                v.getContext().startActivity(intent);
                            }
                        }
                    }else {
                        Toast.makeText(v.getContext(),"No audio file to play",Toast.LENGTH_SHORT).show();
                    }
                }
            });

            imgSync.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.OnFhrItemStatusClick(file, (int) v.getTag());
                }
            });

        }

        @Override
        public String toString() {
            return super.toString();
        }
    }

    public interface OnClickEventListener {
        void OnFhrItemClick(String path);

        void OnFhrItemStatusClick(NSTTest file, int type);

        void OnFrrItemsSelected(int size);
    }

}
