package com.carenx.fetonMom.fragments;


import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.carenx.beatsv3.BeatsRecorder;
import com.carenx.beatsv3.GraphExtraData;
import com.carenx.beatsv3.NstView;
import com.carenx.fetonMom.BuildConfig;
import com.carenx.fetonMom.R;
import com.carenx.fetonMom.activities.HomeActivity;

import java.io.File;
import java.util.ArrayList;

import static android.content.Context.VIBRATOR_SERVICE;
import static com.carenx.fetonMom.activities.HomeActivity.recording;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link NstFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class NstFragment extends Fragment implements BeatsRecorder.RecordingEventListener {

    private static final String TAG = NstFragment.class.getSimpleName();
    private BeatsRecorder mBeatsRecorder;
    private static NstTestCompletionListener nstTestListner;
    private GraphExtraData data;
    private String pirntImagePath;
    private Vibrator vibrator;

    public NstFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment NstFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static NstFragment newInstance(NstTestCompletionListener listener) {
        NstFragment fragment = new NstFragment();
        nstTestListner = listener;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_nst, container, false);

        /*Animation rotateAnim = AnimationUtils.loadAnimation(getActivity(), R.anim.rotate_view);
        LayoutAnimationController animController = new LayoutAnimationController(rotateAnim, 0);
        RelativeLayout layout = (RelativeLayout) root.findViewById(R.id.parent);
        layout.setLayoutAnimation(animController);*/

        init(root);
        return root;
    }


    private void init(View r) {
        data = new GraphExtraData(HomeActivity.motherName, HomeActivity.doctorName, HomeActivity.hospitalName, HomeActivity.gestAge + " weeks", HomeActivity.motherWeight + " Kg");
        mBeatsRecorder = new BeatsRecorder(getActivity(), this, true, data);
        /*final BeatsVisualizer2 mBeatsVisualizer = (BeatsVisualizer2) findViewById(R.id.bvisualizer);
        mBeatsRecorder.setVisualizer(mBeatsVisualizer);*/
        final NstView dynamicView = (NstView) r.findViewById(R.id.visualizer);
        //mBeatsRecorder.setVisualizer(dynamicView);
        mBeatsRecorder.setVisualizer(dynamicView);
        mBeatsRecorder.setTestDetails(HomeActivity.motherName, HomeActivity.gestAge, HomeActivity.motherWeight + " Kg");
        //mWaveformView = (WaveformView) findViewById(R.id.waveform);
        mBeatsRecorder.setStorageFolderName(HomeActivity.AUDIO_RECORDER_FOLDER);


        /*BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (!mBluetoothAdapter.isEnabled()) {
            mBluetoothAdapter.enable();
        }*/
       /* listen = (CheckBox) r.findViewById(R.id.radioListen);
        listen.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    boolean start = mBeatsRecorder.startRecording();
                    if(!start)
                        listen.setChecked(false);
                }else {
                    if(mBeatsRecorder.isRecording())
                        mBeatsRecorder.stopRecording();
                    else
                        listen.setChecked(false);
                }
            }
        });*/
        recording = false;

    }


    @Override
    public void onPause() {
        super.onPause();

        //mBeatsRecorder.toggelRecordingState();
        mBeatsRecorder.changeRecordingNstGraphState(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        //mBeatsRecorder.toggelRecordingState();

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (!isVisibleToUser) {
            if (mBeatsRecorder != null && mBeatsRecorder.isRecording() && !mBeatsRecorder.isPause()) {
                mBeatsRecorder.setBleReceiver();
                mBeatsRecorder.changeRecordingNstGraphState(true);
            }
        }

    }

    @Override
    public void onStop() {
        super.onStop();

        if(mBeatsRecorder.isListening())
            mBeatsRecorder.stopListening();

        if (mBeatsRecorder.isRecording())
            mBeatsRecorder.stopRecording();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(mBeatsRecorder.isListening())
            mBeatsRecorder.stopListening();

        if (mBeatsRecorder.isRecording())
            mBeatsRecorder.stopRecording();
    }

    @Override
    public void onRecordingFinishListener(String audioPath, String imagePath) {

        String id = System.currentTimeMillis() + "";
        if (nstTestListner != null)
            nstTestListner.onNstTestFinished(audioPath, imagePath, id);
        //startActivity(new Intent(getActivity(),GraphActivity.class));
        recording = false;
        if(imagePath!=null&&!imagePath.isEmpty()){
            HomeActivity.showMenuItemsNST = true;
            pirntImagePath = imagePath;
            getActivity().invalidateOptionsMenu();
        }
        // Vibrate for 400 milliseconds
        //shakeItBaby();
    }

    @Override
    public void onRecordingFinishListener(String audioPath,String imagePath, ArrayList<Integer> bpm, ArrayList<Integer> movements) {
        String id = System.currentTimeMillis() + "";
        if (nstTestListner != null)
            nstTestListner.onNstTestFinished(id,audioPath, imagePath, bpm,movements);
        //startActivity(new Intent(getActivity(),GraphActivity.class));
        recording = false;
        if(imagePath!=null&&!imagePath.isEmpty()){
            HomeActivity.showMenuItemsNST = true;
            pirntImagePath = imagePath;
            getActivity().invalidateOptionsMenu();
        }
        // Vibrate for 400 milliseconds
        //shakeItBaby();
    }

    @Override
    public boolean onFhrButtonPressed() {
        if(!BeatsRecorder.getIsBluetoothDopplerConnected())
            startActivity(new Intent(Settings.ACTION_BLUETOOTH_SETTINGS));
        /*else {
            if(mBeatsRecorder.isListening() && !mBeatsRecorder.isRecording()){
                mBeatsRecorder.stopListening();
            }
        }*/
        return false;
    }

    @Override
    public boolean onMarkerButtonPressed() {
        ((HomeActivity)getActivity()).onScanStart();
        return false;
    }


    @Override
    public void onRecordingStartListener() {
        NstView.time = System.currentTimeMillis();
        nstTestListner.onNstTestStarted();
        recording = true;
        HomeActivity.showMenuItemsNST = false;
        getActivity().invalidateOptionsMenu();
        // Vibrate for 400 milliseconds
        //shakeItBaby();
    }

    @Override
    public void onRecordingFinishListener(String path) {
        // todo
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        // Inflate the menu; this adds items to the action bar if it is present.
        if(HomeActivity.showMenuItemsNST)
            inflater.inflate(R.menu.menu_nst, menu);/*
        MenuItem share = menu.findItem(R.id.action_share);
        MenuItem delete = menu.findItem(R.id.action_delete);*/
        //MenuItem stetho = menu.findItem(R.id.stetho);
       /* if (HomeActivity.showMenuItemsNST) {
            share.setVisible(true);
            delete.setVisible(true);
            //stetho.setVisible(false);
        } else {
            share.setVisible(false);
            delete.setVisible(false);
            //stetho.setVisible(true);
        }*/
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity_schedular in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id) {
            case R.id.action_share:
                shareFiles();
                return true;
            case R.id.action_print:
                printFiles();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void printFiles() {
       File file = new File(pirntImagePath);
        Uri uri = FileProvider.getUriForFile(getActivity(),
                BuildConfig.APPLICATION_ID + ".provider", file);


        // View
        Intent intent = new Intent();
        //intent.setAction("org.androidprinting.intent.action.PRINT");//Intent.ACTION_VIEW);
        intent.setAction(Intent.ACTION_VIEW);
        intent.setDataAndType(uri,"application/pdf");
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        startActivity(intent);
        //adapter.notifyDataSetChanged();

    }
    private void shareFiles() {
        File file = new File(pirntImagePath);
        Uri uri = FileProvider.getUriForFile(getActivity(),
                BuildConfig.APPLICATION_ID + ".provider", file);


        // share
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.setType("application/pdf");
        intent.putExtra(Intent.EXTRA_STREAM,uri);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        startActivity(intent);
        //adapter.notifyDataSetChanged();

    }

    private void shakeItBaby() {
        try {
            if (Build.VERSION.SDK_INT >= 26) {
                ((Vibrator) getActivity().getSystemService(VIBRATOR_SERVICE)).vibrate(VibrationEffect.createOneShot(200, VibrationEffect.DEFAULT_AMPLITUDE));
            } else {
                ((Vibrator) getActivity().getSystemService(VIBRATOR_SERVICE)).vibrate(200);
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    public interface NstTestCompletionListener {
        void onNstTestStarted();

        void onNstTestFinished(String audioPath, String imagePath, String id);

        void onNstTestFinished(String id, String audioPath, String imagePath, ArrayList<Integer> bpm, ArrayList<Integer> movements);
    }
}
