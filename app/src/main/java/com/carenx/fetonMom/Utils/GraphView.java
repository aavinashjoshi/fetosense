package com.carenx.fetonMom.Utils;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;

import com.carenx.fetonMom.R;
import com.carenx.fetonMom.model.NSTTest;

import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;


/**
 * Created by avinash on 24/11/17.
 */

public class GraphView extends View implements View.OnTouchListener {


    //private static int WIDTH_PX = 2560;
    //private static int HEIGHT_PX = 1440;

    public static NSTTest mTest;
    private static int gridPreMin = 3;
    float touched_down = 0;
    float touched_up = 0;
    /*public Bitmap[] getNSTGraph(ArrayList<Integer> mTest.getBpmEntries(),ArrayList<Integer> movementList,GraphExtraData data){

        mData = data;
        pointsPerPage = (int)(10*timeScaleFactor*XDIV);
        pointsPerDiv = (int)timeScaleFactor*10;
        int pages =mTest.getBpmEntries().size()/pointsPerPage;
        pages+=1;

        //bitmaps = new Bitmap[pages];
        //canvas = new Canvas[pages];

        drawGraph(pages);
        drawBpmLine(mTest.getBpmEntries(),pages);
        drawMovements(movementList,pages);
        return bitmaps;

    }*/
    float outLineFontSize = 3f;
    float graphGridLinesStrokeWidth = 2.5f;
    float graphGridSubLinesStrokeWidth = 1.5f;
    float graphBpmLineStrokeWidth = 3f;
    float graphAxisTextSize = 25f;
    String movmentImageBase64 = "iVBORw0KGgoAAAANSUhEUgAAABwAAAAoCAYAAADt5povAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH4QsbCTMS6Qes/wAAABl0RVh0Q29tbWVudABDcmVhdGVkIHdpdGggR0lNUFeBDhcAAAEySURBVFjD7dY/S0JRGMfxb9Kgi2A1CNIiiGM4NATtDUkvQoeW2hV6DUZDgm8g6BUEYkRLvgJBaVGDmmpQnCprOU9cLvee+9cL0nngN53n3A+c89zLhTWpTZXEqgvcJYVdAD8qjVVjhxZMcrAqbBeYO4AzoBA3tgE8OGCSXtzgtQaTXMWFnQDfPsAvoBoVKwNLH5hkCZTCYhlgGACTDIB0GPA2BCa5CYqdRcAkp36xihqAqOAnsOeF7QAfMWCSd2BbBz56TGCYtXs3rOOyYQScA2+ah76qnmeX9bYdO7Y1zIEWsK9GPAtMNeBY9aTVnktgYes5EqxgOZYnoAbkHO72RQNOgC3bnhxQB/qWI8/LC94Eih7DFBS0VlEZvj8IUcG/SiX9M2RAAxrQgAY0oAH/A/gLVhjmv8J4WgsAAAAASUVORK5CYII=";
    byte[] movementData = Base64.decode(movmentImageBase64, Base64.DEFAULT);
    Bitmap movementBitmap = BitmapFactory.decodeByteArray(movementData, 0, movementData.length);
    private float pixelsPerOneMM;
    private float pixelsPerOneCM;
    private float oPixelsPerOneMM;
    private float oPixelsPerOneCM;
    private Paint graphOutlines;
    private Paint graphGridLines;
    private Paint graphBackGround;
    private Paint graphSafeZone;
    private Paint graphAxisText;
    private Paint graphGridSubLines;
    private float yDivLength;
    private float yOrigin;
    //private float axisFontSize;
    private float paddingLeft;
    private float paddingTop;
    private float paddingBottom;
    private float paddingRight;
    private float xOrigin;
    private Paint graphBpmLine;
    private float xDivLength;
    private int xDiv;
    private int screenHeight;
    private int screenWidth;
    private float xAxisLength;
    private float yDiv;
    private float yAxisLength;
    private Canvas canvas;
    private float timeScaleFactor;
    private int pointsPerPage;
    private Paint informationText;
    //private int XDIV = 20;
    private int pointsPerDiv;
    private Bitmap bitmap;
    private float graphScaleFactor = 1f;
    private int startIndex = 0;
    private boolean mTouchMode = false;
    private float mTouchStart;
    private int mTouchInitialStartIndex;
    private int mOffset = 0;
    private float centerResumeX;
    private float centerResumeY;
    private int scaleOrigin = 40;


    public GraphView(Context context) {
        super(context);
        initialize();
    }


    public GraphView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initialize();
    }
    public GraphView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize();
    }
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public GraphView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initialize();
    }

    public int getGridPreMin() {
        return this.gridPreMin;
    }

    public void setGridPreMin(int gridPreMin) {
        this.gridPreMin = gridPreMin;
        invalidate();
    }

    public void setTest(NSTTest mTest) {
        this.mTest = mTest;
        invalidate();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        //WIDTH_PX = widthMeasureSpec;
        //HEIGHT_PX = heightMeasureSpec;

        screenHeight = getHeight();//1440
        screenWidth = getWidth();//2560

        oPixelsPerOneCM = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_MM, 10,
                getResources().getDisplayMetrics());

        oPixelsPerOneMM = oPixelsPerOneCM / 10;
        pixelsPerOneCM = oPixelsPerOneCM;
        pixelsPerOneMM = oPixelsPerOneMM;

       /* int graphHeight = (int) (screenHeight / pixelsPerOneCM);
        Log.d("dimensions", pixelsPerOneCM + "  " + pixelsPerOneMM + "  " + graphHeight);
        float temp=1;
        if (graphHeight != 0) {
            for (int i = 2; graphHeight < 10; i++) {
                graphScaleFactor = (1 + (0.1f * i));
                temp = pixelsPerOneCM / graphScaleFactor;

                graphHeight = (int) (screenHeight / temp);

                Log.d("graphHeight", pixelsPerOneCM + "  " + pixelsPerOneMM + "  " + graphHeight);
                if((screenHeight/(pixelsPerOneCM+0.05)) >= 10){
                    graphHeight = (int) (screenHeight / (pixelsPerOneCM+0.05));
                }else if((screenHeight/(pixelsPerOneCM+0.06)) >= 10){
                    graphHeight = (int) (screenHeight / (pixelsPerOneCM+0.06));
                }else if((screenHeight/(pixelsPerOneCM+0.07)) >= 10){
                    graphHeight = (int) (screenHeight / (pixelsPerOneCM+0.07));
                }else if((screenHeight/(pixelsPerOneCM+0.08)) >= 10){
                    graphHeight = (int) (screenHeight / (pixelsPerOneCM+0.08));
                }else if((screenHeight/(pixelsPerOneCM+0.09)) >= 10){
                    graphHeight = (int) (screenHeight / (pixelsPerOneCM+0.09));
                }
            }
        }
        pixelsPerOneCM = temp;
        pixelsPerOneMM = pixelsPerOneCM / 10f;

        Log.d("dimensions", pixelsPerOneCM + "  " + pixelsPerOneMM + "  " + graphHeight);
       */
        //Log.d("dimensions", screenWidth + "  " + screenHeight);
       /* if (screenHeight <= 1200) {
            pixelsPerOneCM /= 2f;
            pixelsPerOneMM /= 2f;
        } else {
            pixelsPerOneCM /= 1.3f;
            pixelsPerOneMM /= 1.3f;
        }*/
        pixelConfig();
        initialize();
    }

    void pixelConfig() {
        int screenSize = getResources().getConfiguration().screenLayout &
                Configuration.SCREENLAYOUT_SIZE_MASK;

        String toastMsg;
        float factor;
        switch (screenSize) {
            case Configuration.SCREENLAYOUT_SIZE_XLARGE:
                //toastMsg = "Small screen";
                factor = 1.25f;
                break;
            case Configuration.SCREENLAYOUT_SIZE_LARGE:
                //toastMsg = "Large screen";
                factor = 1.3f;
                break;
            case Configuration.SCREENLAYOUT_SIZE_NORMAL:
                //toastMsg = "Normal screen";
                factor = 1.5f;
                break;
            case Configuration.SCREENLAYOUT_SIZE_SMALL:
                //toastMsg = "Small screen";
                factor = 2f;
                break;
            default:
                factor = 1f;
                //toastMsg = "Screen size is neither large, normal or small";
        }
        //Toast.makeText(this, toastMsg, Toast.LENGTH_LONG).show();
        pixelsPerOneCM /= factor;
        pixelsPerOneMM /= factor;
        graphScaleFactor = factor;

        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int density = metrics.densityDpi;

        if (density <= DisplayMetrics.DENSITY_LOW) {
            //Toast.makeText(this, "DENSITY_HIGH... Density is " + String.valueOf(density), Toast.LENGTH_LONG).show();
            outLineFontSize = 1f;
            graphGridLinesStrokeWidth = 0.5f;
            graphGridSubLinesStrokeWidth = 0.2f;
            graphBpmLineStrokeWidth = 1f;
            graphAxisTextSize = 5f;
        } else if (density <= DisplayMetrics.DENSITY_MEDIUM) {
            //Toast.makeText(this, "DENSITY_HIGH... Density is " + String.valueOf(density), Toast.LENGTH_LONG).show();
            outLineFontSize = 2f;
            graphGridLinesStrokeWidth = 1.5f;
            graphGridSubLinesStrokeWidth = 1f;
            graphBpmLineStrokeWidth = 2f;
            graphAxisTextSize = 15f;
        } else if (density <= DisplayMetrics.DENSITY_HIGH) {
            //Toast.makeText(this, "DENSITY_MEDIUM... Density is " + String.valueOf(density), Toast.LENGTH_LONG).show();
            outLineFontSize = 2f;
            graphGridLinesStrokeWidth = 1.5f;
            graphGridSubLinesStrokeWidth = 1f;
            graphBpmLineStrokeWidth = 2f;
            graphAxisTextSize = 18f;
        } else {
            //Toast.makeText(this, "Density is neither HIGH, MEDIUM OR LOW.  Density is " + String.valueOf(density), Toast.LENGTH_LONG).show();
            outLineFontSize = 3f;
            graphGridLinesStrokeWidth = 2.5f;
            graphGridSubLinesStrokeWidth = 1.5f;
            graphBpmLineStrokeWidth = 3f;
            graphAxisTextSize = 25f;
        }
        Log.d("dimensions", density + "  " + screenSize + "  " + factor);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        this.canvas = canvas;

        init();

        drawGraph();

        if(mTest!=null) {
            drawBpmLine();

            drawMovements();

            if (mTouchMode)
                drawButton();
        }
        //invalidate();

    }

    private void initialize() {

        graphOutlines = new Paint();
        graphOutlines.setStrokeWidth(outLineFontSize);
        graphOutlines.setColor(Color.BLACK);

        graphGridLines = new Paint();
        graphGridLines.setStrokeWidth(graphGridLinesStrokeWidth);
        graphGridLines.setColor(Color.GRAY);

        graphGridSubLines = new Paint();
        graphGridSubLines.setStrokeWidth(graphGridSubLinesStrokeWidth);
        graphGridSubLines.setColor(Color.LTGRAY);

        graphBackGround = new Paint();
        graphBackGround.setColor(Color.WHITE);

        graphSafeZone = new Paint();
        graphSafeZone.setStrokeWidth(1f);
        graphSafeZone.setColor(Color.argb(40, 100, 200, 0));

        graphAxisText = new Paint();
        graphAxisText.setTextSize(graphAxisTextSize);
        graphAxisText.setColor(Color.BLACK);

        graphBpmLine = new Paint();
        graphBpmLine.setColor(Color.BLUE);
        graphBpmLine.setStrokeWidth(graphBpmLineStrokeWidth);
        graphBpmLine.setStyle(Style.STROKE);

        informationText = new Paint();
        informationText.setTextSize(40f);
        informationText.setColor(Color.BLACK);

        //axisFontSize = pixelsPerOneCM*2;
        //pixelConfig();

        setOnTouchListener(this);
    }

    void adjustFactor() {
        if ((screenHeight / (oPixelsPerOneCM / (graphScaleFactor + 0.05))) >= 10) {
            graphScaleFactor = (graphScaleFactor + 0.05f);
        } else if ((screenHeight / (oPixelsPerOneCM / (graphScaleFactor + 0.06))) >= 10) {
            graphScaleFactor = (graphScaleFactor + 0.06f);
        } else if ((screenHeight / (oPixelsPerOneCM / (graphScaleFactor + 0.07))) >= 10) {
            graphScaleFactor = (graphScaleFactor + 0.07f);
        } else if ((screenHeight / (oPixelsPerOneCM / (graphScaleFactor + 0.08))) >= 10) {
            graphScaleFactor = (graphScaleFactor + 0.08f);
        } else if ((screenHeight / (oPixelsPerOneCM / (graphScaleFactor + 0.09))) >= 10) {
            graphScaleFactor = (graphScaleFactor + 0.09f);
        } else if ((screenHeight / (oPixelsPerOneCM / (graphScaleFactor + 0.2))) >= 10) {
            graphScaleFactor = (graphScaleFactor + 0.2f);
        } else if ((screenHeight / (oPixelsPerOneCM / (graphScaleFactor + 0.21))) >= 10) {
            graphScaleFactor = (graphScaleFactor + 0.22f);
        } else if ((screenHeight / (oPixelsPerOneCM / (graphScaleFactor + 0.22))) >= 10) {
            graphScaleFactor = (graphScaleFactor + 0.25f);
        } else
            graphScaleFactor = (graphScaleFactor + 0.3f);
        pixelsPerOneCM = oPixelsPerOneCM / graphScaleFactor;
        pixelsPerOneMM = pixelsPerOneCM / 10;
    }

    void init() {

        screenHeight = getHeight();//1440
        screenWidth = getWidth();//2560

        int graphHeight = (int) (screenHeight / pixelsPerOneCM);
        if (graphHeight != 0 && graphHeight < 10) {
            adjustFactor();
        }
        paddingLeft = pixelsPerOneCM;
        paddingTop = pixelsPerOneMM * 7;
        paddingBottom = pixelsPerOneCM;
        paddingRight = pixelsPerOneMM;


        //drawGraph();
        timeScaleFactor = gridPreMin == 1 ? 6 : 2;


        xOrigin = paddingLeft;
        yOrigin = screenHeight - (paddingBottom);

        yAxisLength = yOrigin - paddingTop;
        xAxisLength = screenWidth - paddingLeft - paddingRight;


        xDivLength = pixelsPerOneCM;
        xDiv = (int) ((screenWidth - xOrigin - paddingRight) / pixelsPerOneCM);

        yDivLength = xDivLength / 2;
        yDiv = (screenHeight - paddingTop - paddingBottom) / pixelsPerOneCM * 2;

        pointsPerPage = (int) (10 * timeScaleFactor * xDiv);
        pointsPerDiv = (int) timeScaleFactor * 10;
    }

    private void drawGraph() {


        canvas.drawPaint(graphBackGround);

        drawXAxis();

        drawYAxis();

        //invalidate();
    }

    private void drawXAxis() {
        //int numberOffset = XDIV*(pageNumber);
        for (int j = 1; j < 2; j++) {

            canvas.drawLine(xOrigin + ((xDivLength / 2) * j),
                    paddingTop,
                    xOrigin + ((xDivLength / 2) * j),
                    yOrigin, graphGridSubLines);


        }

        for (int i = 1; i <= xDiv; i++) {
            canvas.drawLine(xOrigin + (xDivLength * i), paddingTop,
                    xOrigin + (xDivLength * i), yOrigin, graphGridLines);

            for (int j = 1; j < 2; j++) {
                canvas.drawLine(xOrigin + (xDivLength * i) + ((xDivLength / 2) * j),
                        paddingTop,
                        xOrigin + (xDivLength * i) + ((xDivLength / 2) * j),
                        yOrigin, graphGridSubLines);
            }
            int offset = (int) (startIndex / pointsPerDiv);
            if ((i + offset) % gridPreMin == 0)
                canvas.drawText(String.format(Locale.ENGLISH,"%2d", (i + offset) / gridPreMin),
                        xOrigin + (xDivLength * i) -
                                (graphAxisText.measureText("00") / 2),
                    /*yOrigin + pixelsPerOneCM - pixelsPerOneMM - pixelsPerOneMM*/ pixelsPerOneCM / 2, graphAxisText);


        }

    }

    private void drawYAxis() {
        //y-axis outlines
        canvas.drawLine(xOrigin, yOrigin, screenWidth - paddingRight,
                yOrigin, graphOutlines);
        canvas.drawLine(xOrigin, paddingTop, screenWidth - paddingRight,
                paddingTop, graphOutlines);
        canvas.drawLine(xOrigin, yOrigin + (pixelsPerOneCM - pixelsPerOneMM), screenWidth - paddingRight,
                yOrigin + (pixelsPerOneCM - pixelsPerOneMM), graphOutlines);

        int interval = 10;
        int ymin = 50;
        int safeZoneMax = 160;

        //SafeZone
        RectF safeZoneRect = new RectF();
        safeZoneRect.set(xOrigin,
                (float) (yOrigin - yDivLength) - ((float) (safeZoneMax - ymin) / interval) * yDivLength,
                xOrigin + xAxisLength,
                yOrigin - yDivLength * 8);//50
        canvas.drawRect(safeZoneRect, graphSafeZone);

        for (int i = 1; i <= yDiv; i++) {
            if (i % 2 == 0) {
                canvas.drawLine(xOrigin, yOrigin - (yDivLength * i),
                        xOrigin + xAxisLength, yOrigin - (yDivLength * i), graphGridLines);

                canvas.drawText("" + (ymin + (interval * (i - 1))), pixelsPerOneMM * 2,
                        yOrigin - (yDivLength * i - (pixelsPerOneMM)), graphAxisText);

                canvas.drawLine(xOrigin, yOrigin - (yDivLength * i) + yDivLength / 2,
                        xOrigin + xAxisLength, yOrigin - (yDivLength * i) + yDivLength / 2, graphGridSubLines);

            } else {
                canvas.drawLine(xOrigin, yOrigin - (yDivLength * i),
                        xOrigin + xAxisLength, yOrigin - (yDivLength * i), graphGridSubLines);
                canvas.drawLine(xOrigin, yOrigin - (yDivLength * i) + yDivLength / 2,
                        xOrigin + xAxisLength, yOrigin - (yDivLength * i) + yDivLength / 2, graphGridSubLines);
            }
        }


    }

    private void displayInformation(int pageNumber) {

        int rows = 3;

        String now = new Date().toString();
        String date = String.format("%s %s %s", now.substring(11, 16), now.substring(4, 10), now.substring(now.lastIndexOf(" ")));

        float rowLength = (screenWidth - (pixelsPerOneCM * 2)) / rows;

        canvas.drawText("Hospital : " + mTest.getHospitalName(),
                pixelsPerOneCM,
                pixelsPerOneCM,
                informationText);
        canvas.drawText("Doctor : " + mTest.getDoctorName(),
                pixelsPerOneCM,
                pixelsPerOneCM * 2,
                informationText);

        canvas.drawText("Mother : " + mTest.getMotherName(),
                pixelsPerOneCM,
                pixelsPerOneCM * 3,
                informationText);

        canvas.drawText("Gest. Week : " + mTest.getgAge(),
                pixelsPerOneCM + rowLength,
                pixelsPerOneCM,
                informationText);
        canvas.drawText("Weight : " + mTest.getWeight(),
                pixelsPerOneCM + rowLength,
                pixelsPerOneCM * 2,
                informationText);

        canvas.drawText(String.format("OB. Hist : G __ P __ A __ L __"),
                pixelsPerOneCM + rowLength,
                pixelsPerOneCM * 3,
                informationText);


        canvas.drawText("Date Time : " + date,
                pixelsPerOneCM + rowLength * 2,
                pixelsPerOneCM,
                informationText);

        canvas.drawText(String.format("X-Axis : %s sec/div", timeScaleFactor * 10),
                pixelsPerOneCM + rowLength * 2,
                pixelsPerOneCM * 2,
                informationText);

        canvas.drawText("Y-Axis : 20 BPM/DIV",
                pixelsPerOneCM + rowLength * 2,
                pixelsPerOneCM * 3,
                informationText);


    }

    private void drawBpmLine() {
        if (mTest.getBpmEntries() == null || mTest.getBpmEntries().size() <= 0)
            return;

        float increment = (pixelsPerOneMM / timeScaleFactor);

        //for (int pageNumber = 0;pageNumber<pages;pageNumber++) {

        Path path = new Path();
        float k = xOrigin + increment * 4;
        int i = 0;
        startIndex = 0;
        if(mTouchMode &&!(mTest.getBpmEntries().size()>pointsPerPage))
            mTouchMode= false;
        if (!mTouchMode) {
            if (mTest.getBpmEntries().size() - pointsPerPage > 0) {
                //startIndex = mTest.getBpmEntries().size() - pointsPerPage;
                if (startIndex > 0)
                    startIndex = startIndex + pointsPerDiv - (startIndex % pointsPerDiv);
                if(startIndex == pointsPerDiv)
                    startIndex = 0;

            }else {
                mTouchMode=false;
            }

        } else {
            startIndex = mOffset;
        }

        i = startIndex;

        path.moveTo(k, getYValueFromBPM(mTest.getBpmEntries().get(i++)));
        for (; i < mTest.getBpmEntries().size(); i++) {

            path.lineTo(k, getYValueFromBPM(mTest.getBpmEntries().get(i)));
            k += increment;

            //int xLabel = (i/minPerGrid);
            /*if ((i + 4) % 60 == 0)
                canvas.drawText(String.format("%2d", (i + 4) / 60),
                        k -
                                (graphAxisText.measureText("00") / 2),
                    *//*yOrigin + pixelsPerOneCM - pixelsPerOneMM - pixelsPerOneMM*//* pixelsPerOneCM / 2, graphAxisText);*/
        }

        canvas.drawPath(path, graphBpmLine);


            /*canvas.drawText(String.format("page %2d of %d",pageNumber+1,pages),
                    screenWidth-paddingRight-
                            (graphAxisText.measureText("page 1 of 10")) ,
                    screenHeight-pixelsPerOneMM*2, graphAxisText);*/
        // }

    }

    private void drawMovements() {
        ArrayList<Integer> movementList = mTest.getMovementEntries();
        if (movementList == null || movementList.size() <= 0)
            return;

        float increment = (pixelsPerOneMM / timeScaleFactor);
        for (int i = 0; i < movementList.size(); i++) {
            int movement = movementList.get(i);
            if (movement > 0 && movement > startIndex) {
                movement -= startIndex;
                canvas.drawBitmap(movementBitmap,
                        xOrigin + (increment * (movement) - (movementBitmap.getWidth() / 2)),
                        yOrigin + pixelsPerOneMM, null);
            }
        }


        //Testing dummy movements
       /* for (int pageNumber = 0;pageNumber<pages;pageNumber++) {
            int move[] = {2, 12, 24,60, 120, 240, 300, 420, 600,690, 1220, 1240, 1300, 1420, 1600};
            for (int i = 0; i < move.length; i++) {

               if (move[i]-(pageNumber*pointsPerPage) > 0 && move[i]-(pageNumber*pointsPerPage) < pointsPerPage)
                    canvas.drawBitmap(movementBitmap,
                            xOrigin+(pixelsPerOneMM/timeScaleFactor*(move[i]-(pageNumber*pointsPerPage))-(movementBitmap.getWidth()/2)),
                            yOrigin+pixelsPerOneMM, null);


            }
        }*/
    }

    private void drawButton() {
        /*Paint paint = new Paint();
        paint.setColor(Color.BLUE);
        paint.setStrokeWidth(10);
        paint.setStyle(Style.FILL);
        int width = (int)pixelsPerOneCM*2;
        int height = (int)pixelsPerOneCM;
        canvas.drawCircle(xOrigin, yOrigin, height, paint);*/
        int left = (int) (xOrigin + pixelsPerOneMM);
        int top = (int) (yOrigin - pixelsPerOneCM * 1.8);
        int right = (int) (xOrigin + pixelsPerOneMM + pixelsPerOneCM * 1.8);
        int bottom = (int) (yOrigin - pixelsPerOneMM);
        centerResumeX = (float) (xOrigin + pixelsPerOneMM + pixelsPerOneCM * 0.9);
        centerResumeY = (float) (yOrigin - pixelsPerOneCM * 0.9);
        Drawable d = getResources().getDrawable(R.drawable.ic_fab_resume);
        d.setBounds(left, top, right, bottom);
        d.draw(canvas);

    }

    private float getYValueFromBPM(int bpm) {
        float adjustedBPM = bpm - scaleOrigin;
        adjustedBPM = adjustedBPM / 2; //scaled down version for mobile phone
        float y_value = yOrigin - (adjustedBPM * pixelsPerOneMM);
        Log.i("bpmvalue", bpm + " " + adjustedBPM + " " + y_value);
        return y_value;

    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        float touchX = event.getX();
        float touchY = event.getY();

        // Distance between two points formula
        float touchRadius = (float) Math.sqrt(Math.pow(touchX - centerResumeX, 2) + Math.pow(touchY - centerResumeY, 2));
        if (touchRadius < pixelsPerOneCM * 0.9) {
            mTouchMode = false;
            invalidate();
            return true;
        }

        if (mTest.getBpmEntries().size() < pointsPerPage)
            return true;

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                nstTouchStart(event.getX());
                break;
            case MotionEvent.ACTION_MOVE:
                //Log.i("Touch","ACTION_MOVE");
                nstTouchMove(event.getX());
                break;
            case MotionEvent.ACTION_UP:
                //Log.i("Touch","ACTION_UP");
                nstTouchEnd();
                break;
        }
        return true;
    }


    private void nstTouchStart(float x) {
        mTouchMode = true;
        mTouchStart = x;
        mTouchInitialStartIndex = startIndex;

        Log.i("Touch start", mTouchInitialStartIndex + "");
    }

    private void nstTouchMove(float x) {
        /*int positon = 0;
        if(mTouchStart-x>pointsPerDiv) {
            positon = ((int) (mTouchStart - x) / pointsPerDiv) * pointsPerDiv;
            mOffset = trap((int) (mTouchInitialStartIndex + (mTouchStart - x)/2 ));
        }*/

        mOffset = trap((int) (mTouchInitialStartIndex + ((mTouchStart - x)/pointsPerDiv)));
        if (mOffset != 0) {
            if(mOffset<pointsPerDiv)
                mOffset = 0;
            else
                mOffset = mOffset + pointsPerDiv - (mOffset % pointsPerDiv);

            invalidate();
        }

        //if(mOffset==0)
        //    mTouchMode = false;
        Log.i("Touch move", mOffset + "");
    }

    private void nstTouchEnd() {
        //mTouchMode = true;
    }

    private int trap(int pos) {
        if (pos < 0)
            return 0;
        else if (pos > mTest.getBpmEntries().size() - pointsPerPage)
            pos = mTest.getBpmEntries().size() > pointsPerPage ? mTest.getBpmEntries().size() - pointsPerPage : 0;

        return pos;
    }


}
