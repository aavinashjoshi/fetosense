package com.carenx.fetonMom.adapters;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.carenx.fetonMom.R;

/**
 * Created by avinash on 5/12/16.
 */

public class NavigationRecylerViewAdapter extends RecyclerView.Adapter<NavigationRecylerViewAdapter.ViewHolder> {

    NavigationClickListener listener;
    String names[] = {"NST","History"};
    int icons[] = {R.drawable.ic_nst_24dp,R.drawable.ic_history_24dp};
    private int selectedPosition = 0;

    public NavigationRecylerViewAdapter(NavigationClickListener _listner){
        listener = _listner;
    }

    @Override
    public  ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_tab_view, parent, false);
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(NavigationRecylerViewAdapter.ViewHolder holder, final int position) {
        holder.mView.setTag(position);
        holder.txtName.setText(names[position]);
        holder.ivIcon.setImageResource(icons[position]);

        if(selectedPosition==position){
            holder.mView.setBackgroundColor(Color.RED);
        }else
            holder.mView.setBackgroundColor(Color.TRANSPARENT);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedPosition = position;
                listener.OnNavigationItemClick(position);
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return 2;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public View mView;
        public final TextView txtName;
        public final ImageView ivIcon;

        public ViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
            txtName = (TextView) itemView.findViewById(R.id.title);
            ivIcon = (ImageView) itemView.findViewById(R.id.icon);


        }
    }


    public int getSelectedPosition(){
        return selectedPosition;
    }
    public void setSelectedPosition(int position){
        selectedPosition = position;
    }

    public interface NavigationClickListener {
        void OnNavigationItemClick(int position);
    }


}
