package com.carenx.fetonMom.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.carenx.fetonMom.R;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class CalenderAdapter extends BaseAdapter {

    private final Context mContext;
    private int today;
    private int mStart;
    private int mMonth;
    private Date todayDate;
    private Calendar mCalendar;
    private int mYear;

    private Date startDate;
    private Date endDate;
    private CalenderEventListener listener;
    private ArrayList<Date> hilightedDates;

    public CalenderAdapter(Context context) {
        listener = (CalenderEventListener) context;
        mContext = context;
        mCalendar = Calendar.getInstance();
        todayDate = mCalendar.getTime();
        today = mCalendar.get(Calendar.DAY_OF_YEAR);
        initialize();
    }

    public static int compareDate(Date date1, Date date2) {
        if (date1.getYear() == date2.getYear() &&
                date1.getMonth() == date2.getMonth() &&
                date1.getDate() == date2.getDate()) {
            return 0 ;
        }
        else if (date1.getYear() < date1.getYear() ||
                (date1.getYear() == date2.getYear() &&
                        date1.getMonth() < date2.getMonth()) ||
                (date1.getYear() == date2.getYear() &&
                        date1.getMonth() == date2.getMonth() &&
                        date1.getDate() < date2.getDate())) {
            return -1 ;
        }
   else {
            return 1 ;
        }
    }

    public void setLimits(Date lmpDate, Date eddDate) {
        this.startDate = lmpDate;
        this.endDate = eddDate;
    }

    private long getDifference() {
        // 23328000000 9months in mili
        // 777600000 9days in mili

        return 23328000000L + 777600000L;
    }

    private void initialize() {

        mMonth = mCalendar.get(Calendar.MONTH);
        mYear = mCalendar.get(Calendar.YEAR);
        ;
        mCalendar.set(Calendar.DAY_OF_MONTH, 1);
        int day = mCalendar.get(Calendar.DAY_OF_WEEK);
        if (day != Calendar.SUNDAY) {
            mCalendar.add(Calendar.DAY_OF_YEAR, -day + 1);
        }
        mStart = mCalendar.get(Calendar.DAY_OF_YEAR);
    }

    public String setMonth(int add) {

        if (add != 0 && startDate != null && endDate != null) {
            boolean update = true;

            mCalendar.add(Calendar.MONTH, add);

            if (mCalendar.getTime().after(endDate)) {
                mCalendar.setTime(endDate);
                update = false;
            } else if (mCalendar.getTime().before(startDate)) {
                mCalendar.setTime(startDate);
                update = false;
            }
        } else if (add == 0) {
            mCalendar.setTimeInMillis(System.currentTimeMillis());
            mCalendar.set(Calendar.DAY_OF_YEAR, today);
        }
        String month = String.format(Locale.ENGLISH, "%tB %tY", mCalendar, mCalendar);
        initialize();
        notifyDataSetChanged();
        return month;
    }

    public int getMonth() {
        return mMonth;
    }

    public int getYear() {
        return mYear;
    }

    @Override
    public int getCount() {
        return 42;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = convertView;
        ViewHolder viewHolder;
        if (convertView == null) {
            rowView = inflater.inflate(R.layout.item_calender, parent, false);

            viewHolder = new ViewHolder();

            rowView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        mCalendar.set(Calendar.DAY_OF_YEAR, mStart + position);
        final String date = String.format(Locale.ENGLISH, "%d", mCalendar.get(Calendar.DAY_OF_MONTH));
        viewHolder.layout = rowView.findViewById(R.id.layout);
        viewHolder.ivTest = rowView.findViewById(R.id.ivTest);
        viewHolder.date = rowView.findViewById(R.id.txtDate);
        viewHolder.date.setText(date);

        if (mCalendar.getTime().compareTo(todayDate) == 0) {
            viewHolder.date.setTextColor(mContext.getResources().getColor(R.color.colorAccent));
            viewHolder.date.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
            viewHolder.layout.setEnabled(true);
        } else if (mCalendar.get(Calendar.MONTH) != mMonth) {
            viewHolder.date.setTextColor(Color.GRAY);
            viewHolder.date.setTypeface(Typeface.DEFAULT, Typeface.NORMAL);
            viewHolder.layout.setEnabled(false);
        } else {
            viewHolder.date.setTextColor(Color.BLACK);
            viewHolder.date.setTypeface(Typeface.DEFAULT, Typeface.NORMAL);
            viewHolder.layout.setEnabled(true);
        }


        viewHolder.ivTest.setVisibility(View.GONE);
        viewHolder.date.setBackgroundColor(Color.TRANSPARENT);
        if (hilightedDates != null && hilightedDates.size() > 0) {
            for (Date d : hilightedDates) {
                //if (mCalendar.getTime().compareTo(d) == 0) {
                if (compareDate(d,mCalendar.getTime())==0){
                    viewHolder.ivTest.setVisibility(View.VISIBLE);
                }
            }
        }


        viewHolder.layout.setTag(Integer.parseInt(date));
        viewHolder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //if (listener != null)
                    //listener.onDateSelected((int) v.getTag(), mMonth, mYear);

            }
        });

        mCalendar.set(Calendar.MONTH, mMonth);
        mCalendar.set(Calendar.YEAR, mYear);
        return rowView;
    }

    public void setHighlightedDates(ArrayList<Date> hilightedDates) {
        this.hilightedDates = hilightedDates;
        notifyDataSetChanged();
    }
    public interface CalenderEventListener {
        public void onDateSelected(int date, int month, int year);
    }

    public class ViewHolder {
        RelativeLayout layout;
        TextView date;
        ImageView ivTest;
        /*ViewHolder(View v){
            date = v.findViewById(R.id.txtDate);
            date.setText("x");
        }*/
    }
}
