package com.carenx.fetonMom.service;

import android.app.ActivityManager;
import android.content.Context;
import android.database.ContentObserver;
import android.media.AudioManager;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import java.util.List;

/**
 * Created by avinash on 21/4/17.
 */

public class SettingsContentObserver extends ContentObserver {
    int previousVolume;
    Context context;

    public SettingsContentObserver(Context c, Handler handler) {
        super(handler);
        context=c;

        AudioManager audio = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        previousVolume = audio.getStreamVolume(AudioManager.STREAM_MUSIC);

       /* ActivityManager manager =(ActivityManager) c.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> tasks = manager.getRunningTasks(1);
        String topActivityName = tasks.get(0).topActivity.getPackageName();
        if(!(topActivityName.equalsIgnoreCase(c.getApplicationContext().getPackageName()))){
            Toast.makeText(context,"Increased",Toast.LENGTH_SHORT).show();
        }*/
            //enter notification code here
    }

    @Override
    public boolean deliverSelfNotifications() {
        return super.deliverSelfNotifications();
    }


    @Override
    public void onChange(boolean selfChange) {
        super.onChange(selfChange);

        AudioManager audio = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        int currentVolume = audio.getStreamVolume(AudioManager.STREAM_MUSIC);

        int delta=previousVolume-currentVolume;

        /*if(delta>0)
        {
            Log.i("observer","Decreased");
            previousVolume=currentVolume;
            Toast.makeText(context,"Decreased",Toast.LENGTH_SHORT).show();
        }
        else if(delta<0)
        {
            Log.i("observer","Increased");
            previousVolume=currentVolume;
            Toast.makeText(context,"Increased",Toast.LENGTH_SHORT).show();
        }*/
    }


}