package com.carenx.fetonMom.service;

import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Environment;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.carenx.fetonMom.BuildConfig;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;


public class DownloadService extends BaseTaskService {

    private static final String TAG = "Storage#DownloadService";

    /** Actions **/
    public static final String ACTION_DOWNLOAD = "action_download";
    public static final String DOWNLOAD_COMPLETED = "download_completed";
    public static final String DOWNLOAD_ERROR = "download_error";

    /** Extras **/
    public static final String EXTRA_DOWNLOAD_PATH = "extra_download_path";
    public static final String EXTRA_DOWNLOAD_ID = "extra_download_id";
    public static final String EXTRA_LOCAL_PATH = "extra_local_path";
    public static final String EXTRA_DOWNLOAD_URI = "extra_local_uri";


    public static final String EXTRA_FILE_TYPE = "extra_file_type";
    public static final String EXTRA_FILE_AUDIO = "beats";
    public static final String EXTRA_FILE_IMAGE = "reports";

    private StorageReference mStorageRef;


    @Override
    public void onCreate() {
        super.onCreate();

        // Initialize Storage
        mStorageRef = FirebaseStorage.getInstance().getReference();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand:" + intent + ":" + startId);

        if (ACTION_DOWNLOAD.equals(intent.getAction())) {
            // Get the path to download from the intent
            String downloadPath = intent.getStringExtra(EXTRA_DOWNLOAD_PATH);
            String localPath = intent.getStringExtra(EXTRA_LOCAL_PATH);
            int downloadId = intent.getIntExtra(EXTRA_DOWNLOAD_ID,-1);
            downloadFromPath(downloadPath,localPath,downloadId);

        }

        return START_REDELIVER_INTENT;
    }

    private void downloadFromPath(final String downloadPath,final String localPath, final int id) {
        Log.d(TAG, "downloadFromPath:" + downloadPath);

        // Mark task started
        taskStarted();

        String filePath =   downloadPath.substring(downloadPath.lastIndexOf("/"));
        Log.d(TAG, "downloadToPath:" + localPath);
        StorageReference mFileRef = mStorageRef.child(downloadPath);
        final File file = new File(localPath, filePath);
        //file.mkdirs();
        final Uri uri = FileProvider.getUriForFile(DownloadService.this,
                BuildConfig.APPLICATION_ID + ".provider",file);

        mFileRef.getFile(file).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                Log.d(TAG, "download:SUCCESS");

                // Send success broadcast with number of bytes downloaded
                Intent broadcast = new Intent(DOWNLOAD_COMPLETED);
                broadcast.putExtra(EXTRA_DOWNLOAD_PATH, file.getAbsolutePath());
                broadcast.putExtra(EXTRA_DOWNLOAD_URI, uri);//Uri.fromFile(file));
                broadcast.putExtra(EXTRA_DOWNLOAD_ID, id);
                broadcast.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                broadcast.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                LocalBroadcastManager.getInstance(getApplicationContext())
                        .sendBroadcast(broadcast);

                // Mark task completed
                taskCompleted();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                Log.w(TAG, "download:FAILURE", exception);

                // Send failure broadcast
                Intent broadcast = new Intent(DOWNLOAD_ERROR);
                broadcast.putExtra(EXTRA_DOWNLOAD_PATH, downloadPath);
                broadcast.putExtra(EXTRA_DOWNLOAD_ID, id);
                LocalBroadcastManager.getInstance(getApplicationContext())
                        .sendBroadcast(broadcast);

                // Mark task completed
                taskCompleted();
            }
        });

       /* // Download and get total bytes
        mStorageRef.child(downloadPath).getStream(
                new StreamDownloadTask.StreamProcessor() {
                    @Override
                    public void doInBackground(StreamDownloadTask.TaskSnapshot taskSnapshot,
                                               InputStream inputStream) throws IOException {
                        // Close the stream at the end of the Task
                        inputStream.close();
                    }
                })
                .addOnSuccessListener(new OnSuccessListener<StreamDownloadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(StreamDownloadTask.TaskSnapshot taskSnapshot) {
                        Log.d(TAG, "download:SUCCESS");

                        // Send success broadcast with number of bytes downloaded
                        Intent broadcast = new Intent(DOWNLOAD_COMPLETED);
                        broadcast.putExtra(EXTRA_DOWNLOAD_PATH, downloadPath);
                        broadcast.putExtra(EXTRA_DOWNLOAD_ID, id);
                        broadcast.putExtra(EXTRA_BYTES_DOWNLOADED, taskSnapshot.getTotalByteCount());
                        LocalBroadcastManager.getInstance(getApplicationContext())
                                .sendBroadcast(broadcast);

                        // Mark task completed
                        taskCompleted();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        Log.w(TAG, "download:FAILURE", exception);

                        // Send failure broadcast
                        Intent broadcast = new Intent(DOWNLOAD_ERROR);
                        broadcast.putExtra(EXTRA_DOWNLOAD_PATH, downloadPath);
                        broadcast.putExtra(EXTRA_DOWNLOAD_ID, id);
                        LocalBroadcastManager.getInstance(getApplicationContext())
                                .sendBroadcast(broadcast);

                        // Mark task completed
                        taskCompleted();
                    }
                });*/
    }

    public static IntentFilter getIntentFilter() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(DOWNLOAD_COMPLETED);
        filter.addAction(DOWNLOAD_ERROR);

        return filter;
    }
}
