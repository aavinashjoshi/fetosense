package com.carenx.fetonMom.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AsyncPlayer;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Handler;
import android.os.SystemClock;
import android.view.KeyEvent;

/**
 * Receives media button events and calls to PlaybackService to respond
 * appropriately.
 */
public class MediaButtonReceiver extends BroadcastReceiver {
	/**
	 * If another button event is received before this time in milliseconds
	 * expires, the event with be considered a double click.
	 */
	private static final int DOUBLE_CLICK_DELAY = 600;

	/**
	 * Whether the headset controls should be used. 1 for yes, 0 for no, -1 for
	 * uninitialized.
	 */
	private static int sUseControls = -1;
	/**
	 * Time of the last play/pause click. Used to detect double-clicks.
	 */
	private static long sLastClickTime = 0;
	/**
	 * Whether a beep should be played in response to double clicks be used.
	 * 1 for yes, 0 for no, -1 for uninitialized.
	 */
	private static int sBeep = -1;
	/**
	 * Lazy-loaded AsyncPlayer for beep sounds.
	 */
	private static AsyncPlayer sBeepPlayer;
	/**
	 * Lazy-loaded URI of the beep resource.
	 */
	private static Uri sBeepSound;

	/**
	 * Play a beep sound.
	 */
	private static void beep(Context context)
	{
		/*if (sBeep == -1) {
			SharedPreferences settings = PlaybackService.getSettings(context);
			sBeep = settings.getBoolean(PrefKeys.MEDIA_BUTTON_BEEP, PrefDefaults.MEDIA_BUTTON_BEEP) ? 1 : 0;
		}*/

		if (sBeep == 1) {
			if (sBeepPlayer == null) {
				sBeepPlayer = new AsyncPlayer("BeepPlayer");
				sBeepSound = Uri.parse("android.resource://ch.blinkenlights.android.vanilla/raw/beep");
			}
			sBeepPlayer.play(context, sBeepSound, false, AudioManager.STREAM_MUSIC);
		}
	}

	/**
	 * Reload the preferences and enable/disable buttons as appropriate.
	 *
	 * @param context A context to use.
	 */
	public static void reloadPreference(Context context)
	{
		sUseControls = -1;
		sBeep = -1;
	}

	/**
	 * Return whether headset controls should be used, loading the preference
	 * if necessary.
	 *
	 * @param context A context to use.
	 */
	public static boolean useHeadsetControls(Context context)
	{
		/*if (sUseControls == -1) {
			SharedPreferences settings = PlaybackService.getSettings(context);
			sUseControls = settings.getBoolean(PrefKeys.MEDIA_BUTTON, PrefDefaults.MEDIA_BUTTON) ? 1 : 0;
		}*/

		return sUseControls == 1;
	}

	/**
	 * Process a media button key press.
	 *
	 * @param context A context to use.
	 * @param event The key press event.
	 * @return True if the event was handled and the broadcast should be
	 * aborted.
	 */
	public static boolean processKey(Context context, KeyEvent event)
	{
		if (event == null)
			return false;

		int action = event.getAction();
		String act = null;

		switch (event.getKeyCode()) {

		case KeyEvent.KEYCODE_MEDIA_NEXT:
			if (action == KeyEvent.ACTION_DOWN)

			break;
		case KeyEvent.KEYCODE_MEDIA_PREVIOUS:
			if (action == KeyEvent.ACTION_DOWN)

			break;
		case KeyEvent.KEYCODE_MEDIA_PLAY:
			if (action == KeyEvent.ACTION_DOWN)

			break;
		case KeyEvent.KEYCODE_MEDIA_PAUSE:
			if (action == KeyEvent.ACTION_DOWN)

			break;
		default:
			return false;
		}

		runAction(context, act);
		return true;
	}


	/**
	 * Passes an action to PlaybackService
	 *
	 * @param context the context to use
	 * @param act the action to pass on
	 */
	private static void runAction(Context context, String act) {
		if (act == null)
			return;


	}


	@Override
	public void onReceive(Context context, Intent intent)
	{
		if (Intent.ACTION_MEDIA_BUTTON.equals(intent.getAction())) {
			KeyEvent event = intent.getParcelableExtra(Intent.EXTRA_KEY_EVENT);
			boolean handled = processKey(context, event);
			if (handled && isOrderedBroadcast())
				abortBroadcast();
		}
	}
}
