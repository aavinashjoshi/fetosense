package com.carenx.beatsv3;

/**
 * Created by avinash on 6/1/18.
 */

public class GraphExtraData {


    String motherName;
    String doctorName;
    String hospitalName;
    String gestWeek;
    String weight;

    public String getMotherName() {
        return motherName;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public String getHospitalName() {
        return hospitalName;
    }

    public String getGestWeek() {
        return gestWeek;
    }

    public String getWeight() {
        return weight;
    }

    public GraphExtraData(String motherName, String doctorName, String hospitalName, String gestWeek, String weight) {
        this.motherName = motherName;
        this.doctorName = doctorName;
        this.hospitalName = hospitalName;
        this.gestWeek = gestWeek;
        this.weight = weight;
    }

}
