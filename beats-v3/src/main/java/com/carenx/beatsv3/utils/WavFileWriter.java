package com.carenx.beatsv3.utils;

import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

/**
 * Created by avinash on 11/6/16.
 */
public class WavFileWriter {

    private String fPath;
    private RandomAccessFile output;
    private int audioLen;

    public WavFileWriter(String path,int sampleRate, int sampleSizeInBits){
        audioLen=0;
        fPath = path;
        try {
            output = new RandomAccessFile(path, "rw");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            File f= new File(path);
            try {
                output = new RandomAccessFile(new File(path), "rw");
            } catch (FileNotFoundException e1) {
                e1.printStackTrace();
            }
        }
        prepareFile(sampleRate,sampleSizeInBits );
    }

    private void prepareFile(int sRate, int bSamples) {


        try {
            output.setLength(0); // Set file length to 0, to

            // prevent unexpected behavior
            // in case the file already
            // existed
            output.writeBytes("RIFF");
            output.writeInt(0); // Final file size not known yet,
            // write 0
            output.writeBytes("WAVE");
            output.writeBytes("fmt ");
            output.writeInt(Integer.reverseBytes(16)); // Sub-chunk
            // size, 16
            // for PCM
            output.writeShort(Short.reverseBytes((short) 1)); // DSPAudioFormat,
            // 1
            // for
            // PCM
            output.writeShort(Short.reverseBytes((short) 1));// Number nChannels
            // of
            // channels,
            // 1
            // for
            // mono,
            // 2
            // for
            // stereo
            output.writeInt(Integer.reverseBytes(sRate)); // Sample
            // rate
            output.writeInt(Integer.reverseBytes(sRate * bSamples
                    * 1 / 8)); // Byte rate,
            // SampleRate*NumberOfChannels*BitsPerSample/8
            output.writeShort(Short
                    .reverseBytes((short) (1 * bSamples / 8))); // Block
            // align,
            // NumberOfChannels*BitsPerSample/8
            output.writeShort(Short.reverseBytes((short) bSamples)); // Bits
            // per
            // sample
            output.writeBytes("data");
            output.writeInt(0); // Data chunk size not known yet,
            // write 0
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void writeToFile(byte[] buffer){
        //writing to sd card
        audioLen += buffer.length;
        //write audio to the output
        try {
            output.write(buffer);
            updateWritingFile();
        } catch (IOException e) {

        }
    }

    public String getFilePath(){
        return fPath;

    }

    public String updateWritingFile(){
        try {
            long now = output.getFilePointer();
            output.seek(4); // Write size to RIFF header
            output.writeInt(Integer.reverseBytes(36 + audioLen));

            output.seek(40); // Write size to SubChunk2Size field
            output.writeInt(Integer.reverseBytes(audioLen));
            output.seek(now);


        } catch (IOException e) {
            Log.e("WavFileWriter",
                    "I/O exception occured while closing output file");
        }
        return this.fPath;
    }

    public String finishWritingFile(){
        try {
            output.seek(4); // Write size to RIFF header
            output.writeInt(Integer.reverseBytes(36 + audioLen));

            output.seek(40); // Write size to SubChunk2Size field
            output.writeInt(Integer.reverseBytes(audioLen));
            output.close();
            audioLen = 0;

        } catch (IOException e) {
            Log.e("WavFileWriter",
                    "I/O exception occured while closing output file");
        }
        return this.fPath;
    }

}
