package com.carenx.beatsv3.utils;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by avinash on 14/1/16.
 */
public class DeviceConnectionReceiver extends BroadcastReceiver {

    OnHeadsetStateChangedListener onHeadsetStateChangedListener;

    @Override
    public void onReceive(Context context, Intent intent) {

        int mic = intent.getIntExtra("microphone",0);
        int state = intent.getIntExtra("state",0);
        if(( state & mic )!=0){
            if(onHeadsetStateChangedListener!=null)
                onHeadsetStateChangedListener.OnHeadSetWithMicPluggedIn();
        }else if( state ==1 ){
            if(onHeadsetStateChangedListener!=null)
                onHeadsetStateChangedListener.OnHeadSetPluggedIn();
        }else{
            if(onHeadsetStateChangedListener!=null)
                onHeadsetStateChangedListener.OnHeadSetPluggedOut();
        }

    }

    public void setOnHeadsetStateChangedListener(OnHeadsetStateChangedListener listener){
        onHeadsetStateChangedListener = listener;
    }

    public interface OnHeadsetStateChangedListener{
        void OnHeadSetWithMicPluggedIn();
        void OnHeadSetPluggedIn();
        void OnHeadSetPluggedOut();

    }


}
