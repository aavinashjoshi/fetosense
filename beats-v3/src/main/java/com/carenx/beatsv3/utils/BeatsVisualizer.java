package com.carenx.beatsv3.utils;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.media.audiofx.Visualizer;
import android.util.AttributeSet;
import android.view.View;

import com.carenx.beatsv3.R;


public class BeatsVisualizer extends View {

    private Paint mForePaint = new Paint();
    private byte[] mBytes;
    private Visualizer mVisualizer;

    public BeatsVisualizer(Context context) {
        super(context);
        initialize();
    }

    public BeatsVisualizer(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize();
    }

    public BeatsVisualizer(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize();
    }

    @TargetApi(21)
    public BeatsVisualizer(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    void initialize() {
        mBytes = null;
        mForePaint.setStrokeWidth(1f);
        mForePaint.setAntiAlias(true);
        mForePaint.setColor(this.getResources().getColor( R.color.waveform_selected));
    }

    public void setEnabled(boolean enable) {
        mVisualizer.setEnabled(enable);
    }

    public void updateVisualizer(byte[] bytes) {
        mBytes = bytes;
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (mBytes == null) {
            return;
        }
        int x = 0;
        try {
            for (int i = 0; i < (mBytes.length/6); i++) {

                int downy =  100 - (mBytes[i] * 2);
                int upy = 100+(mBytes[i] * 2);

                canvas.drawLine(x, downy, x+3, upy, mForePaint);
                canvas.drawLine(x+3, upy, x+6, downy, mForePaint);
                x+=6;

            }
        }catch (Exception ex){
            ex.printStackTrace();
        }

    }
    public void setupVisualizerFxAndUI(int sessionId) {

        // Create the Visualizer object and attach it to our media player.
        this.mVisualizer = new Visualizer(sessionId);
        this.mVisualizer.setCaptureSize(Visualizer.getCaptureSizeRange()[1]);
        this.mVisualizer.setDataCaptureListener(
                new Visualizer.OnDataCaptureListener() {
                    public void onWaveFormDataCapture(Visualizer visualizer, byte[] bytes, int samplingRate) {
                        //updateVisualizer(bytes);
                    }

                    public void onFftDataCapture(Visualizer visualizer,
                                                 byte[] bytes, int samplingRate) {
                        updateVisualizer(bytes);
                    }
                }, Visualizer.getMaxCaptureRate() / 2, false, true);
    }

}
