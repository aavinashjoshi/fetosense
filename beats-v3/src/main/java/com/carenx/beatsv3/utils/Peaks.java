package com.carenx.beatsv3.utils;

/**
 * Created by avinash on 13/10/16.
 */

import android.util.Log;

import java.util.LinkedList;

public class Peaks {
    public static boolean debug = false;
    public static int pre = 1;
    public static int post = 1;

    public Peaks() {
    }

    public static int findPeaks(double[] var0, int[] var1, int var2) {
        int var3 = 0;
        boolean var4 = false;
        int var5 = 0;

        for(int var6 = var0.length; var5 < var6; ++var5) {
            int var7 = var5 - var2;
            if(var7 < 0) {
                var7 = 0;
            }

            int var8 = var5 + var2 + 1;
            if(var8 > var0.length) {
                var8 = var0.length;
            }

            int var10;
            for(var10 = var7++; var7 < var8; ++var7) {
                if(var0[var7] > var0[var10]) {
                    var10 = var7;
                }
            }

            if(var10 == var5) {
                int var9;
                for(var9 = var3; var9 > 0 && var0[var10] > var0[var1[var9 - 1]]; --var9) {
                    if(var9 < var1.length) {
                        var1[var9] = var1[var9 - 1];
                    }
                }

                if(var9 != var1.length) {
                    var1[var9] = var10;
                }

                if(var3 != var1.length) {
                    ++var3;
                }
            }
        }

        return var3;
    }
    /** General peak picking method for finding local maxima in an array
     *  @param data input data
     *  @param width minimum distance between peaks
     *  @param threshold minimum value of peaks
     *  @return list of peak indexes
     */
    public static LinkedList<Integer> findPeaks(double[] data, int width, double threshold) {
        return findPeaks(data, width, threshold, 0.3D, true);
    }

    /** General peak picking method for finding local maxima in an array
     *  @param data input data
     *  @param width minimum distance between peaks
     *  @param threshold minimum value of peaks
     *  @param decayRate how quickly previous peaks are forgotten
     *  @param isRelative minimum value of peaks is relative to local average
     *  @return list of peak indexes
     */
    public static LinkedList<Integer> findPeaks(double[] data, int width, double threshold, double decayRate, boolean isRelative) {
        LinkedList peaks = new LinkedList();
        boolean var8 = false;
        int mid = 0;
        int end = data.length;

        for(double av = data[0]; mid < end; ++mid) {
            av = decayRate * av + (1.0D - decayRate) * data[mid];
            if(av < data[mid]) {
                av = data[mid];
            }

            int start = mid - width;
            if(start < 0) {
                start = 0;
            }

            int stop = mid + width + 1;
            if(stop > data.length) {
                stop = data.length;
            }

            int maxp;
            for(maxp = start++; start < stop; ++start) {
                if(data[start] > data[maxp]) {
                    maxp = start;
                }
            }

            if(maxp == mid) {
                if(overThreshold(data, maxp, width, threshold, isRelative, av)) {
                    if(debug) {
                        System.out.println(" peak");
                    }

                    peaks.add(new Integer(maxp));
                } else if(debug) {
                    System.out.println();
                }
            }
        }

        return peaks;
    }

    public static double expDecayWithHold(double var0, double var2, double[] var4, int var5, int var6) {
        for(; var5 < var6; ++var5) {
            var0 = var2 * var0 + (1.0D - var2) * var4[var5];
            if(var0 < var4[var5]) {
                var0 = var4[var5];
            }
        }

        return var0;
    }

    public static boolean overThreshold(double[] data, int max, int width, double threshold, boolean isRelative, double av) {
        if(debug) {
            System.out.printf("%4d : %6.3f     Av1: %6.3f    ", new Object[]{Integer.valueOf(max), Double.valueOf(data[max]), Double.valueOf(av)});
        }

        if(data[max] < av) {
            return false;
        } else if(!isRelative) {
            return data[max] > threshold;
        } else {
            int beginIndex = max - pre * width;
            if(beginIndex < 0) {
                beginIndex = 0;
            }

            int endIndex = max + post * width;
            if(endIndex > data.length) {
                endIndex = data.length;
            }

            double sum = 0.0D;

            int var12;
            for(var12 = endIndex - beginIndex; beginIndex < endIndex; sum += data[beginIndex++]) {
                ;
            }

            if(debug) {
                System.out.printf("    %6.3f    %6.3f   ", new Object[]{Double.valueOf(sum / (double)var12), Double.valueOf(data[max] - sum / (double)var12 - threshold)});
            }

            Log.i("peakaverage",sum / (double)var12+ threshold +"      "+ data[max]);
            return data[max] > (sum / (double)var12 + threshold);
        }
    }

    public static void normalise(double[] var0) {
        double var1 = 0.0D;
        double var3 = 0.0D;

        for(int var5 = 0; var5 < var0.length; ++var5) {
            var1 += var0[var5];
            var3 += var0[var5] * var0[var5];
        }

        double var10 = var1 / (double)var0.length;
        double var7 = Math.sqrt((var3 - var1 * var10) / (double)var0.length);
        if(var7 == 0.0D) {
            var7 = 1.0D;
        }

        for(int var9 = 0; var9 < var0.length; ++var9) {
            var0[var9] = (var0[var9] - var10) / var7;
        }

    }

    public static void getSlope(double[] var0, double var1, int var3, double[] var4) {
        int var5 = 0;
        int var6 = 0;
        double var9 = 0.0D;
        double var11 = 0.0D;
        double var13 = 0.0D;

        double var15;
        for(var15 = 0.0D; var5 < var3; ++var5) {
            double var7 = (double)var5 * var1;
            var9 += var7;
            var11 += var7 * var7;
            var13 += var0[var5];
            var15 += var7 * var0[var5];
        }

        double var17;
        for(var17 = (double)var3 * var11 - var9 * var9; var6 < var3 / 2; ++var6) {
            var4[var6] = ((double)var3 * var15 - var9 * var13) / var17;
        }

        while(var6 < var0.length - (var3 + 1) / 2) {
            var4[var6] = ((double)var3 * var15 - var9 * var13) / var17;
            var13 += var0[var5] - var0[var5 - var3];
            var15 += var1 * ((double)var3 * var0[var5] - var13);
            ++var6;
            ++var5;
        }

        while(var6 < var0.length) {
            var4[var6] = ((double)var3 * var15 - var9 * var13) / var17;
            ++var6;
        }

    }

    public static double min(double[] var0) {
        return var0[imin(var0)];
    }

    public static double max(double[] var0) {
        return var0[imax(var0)];
    }

    public static int imin(double[] var0) {
        int var1 = 0;

        for(int var2 = 1; var2 < var0.length; ++var2) {
            if(var0[var2] < var0[var1]) {
                var1 = var2;
            }
        }

        return var1;
    }

    public static int imax(double[] var0) {
        int var1 = 0;

        for(int var2 = 1; var2 < var0.length; ++var2) {
            if(var0[var2] > var0[var1]) {
                var1 = var2;
            }
        }

        return var1;
    }
}
