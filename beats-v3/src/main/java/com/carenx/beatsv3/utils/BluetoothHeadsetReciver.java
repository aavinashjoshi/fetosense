package com.carenx.beatsv3.utils;

import android.bluetooth.BluetoothHeadset;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by avinash on 28/8/17.
 */

public class BluetoothHeadsetReciver extends BroadcastReceiver {


    public static final String ACTION_ON_DOPPLER_CONNECTED = "com.carenx.feto.doppler.state_connected";
    public static final String ACTION_ON_DOPPLER_DISCONNECTED = "com.carenx.feto.doppler.state_disconnected";

    @Override
    public void onReceive(Context context, Intent intent) {

        String action = intent.getAction();

        if (BluetoothHeadset.ACTION_CONNECTION_STATE_CHANGED.equals(action)) {
            int bluetoothHeadsetState = intent.getIntExtra(BluetoothHeadset.EXTRA_STATE,
                    BluetoothHeadset.STATE_DISCONNECTED);
            Log.i("headsetAudioState", bluetoothHeadsetState + " ");
            //Device found
            if (bluetoothHeadsetState == BluetoothHeadset.STATE_CONNECTED) {
                context.sendBroadcast(new Intent(ACTION_ON_DOPPLER_CONNECTED));
                Log.i("headsetAudioState", bluetoothHeadsetState + " connected");
            }
            if (bluetoothHeadsetState == BluetoothHeadset.STATE_DISCONNECTED) {
                context.sendBroadcast(new Intent(ACTION_ON_DOPPLER_DISCONNECTED));
                Log.i("headsetAudioState", bluetoothHeadsetState + " disconnected");
            }
        }

    }
}
