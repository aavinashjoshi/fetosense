package com.carenx.beatsv3.utils;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.media.audiofx.Visualizer;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import com.carenx.beatsv3.R;

import java.util.ArrayList;

import static android.R.attr.height;


/**
 * Created by avinash on 10/11/16.
 */

public class BeatsVisualizer3 extends View {
    ArrayList<Double> smoothedGains = new ArrayList<>();
    private int offset;
    private float mTouchStart;
    private boolean playing;
    private boolean mTouchDragging;
    private Paint mForePaint = new Paint();
    private float[] mBytes;
    private Visualizer mVisualizer;
    private ArrayList<Double> mValues;
    private int mLength = 0;
    private ArrayList<Integer> mHeights;
    private Paint selectedBkgndLinePaint;
    private Paint selectedLinePaint;
    private Paint mPlaybackLinePaint;
    private boolean plot = true;
    private int count = 0;
    private double front;
    private double center;
    private double last;
    private int parentHeight;

    public BeatsVisualizer3(Context context) {
        super(context);
        this.initialize();
    }

    public BeatsVisualizer3(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.initialize();
    }

    public BeatsVisualizer3(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.initialize();
    }

    @TargetApi(21)
    public BeatsVisualizer3(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    void initialize() {

        this.mBytes = null;
        this.mForePaint.setStrokeWidth(1.0F);
        this.mForePaint.setAntiAlias(true);
        this.mForePaint.setColor(this.getResources().getColor(R.color.waveform_selected));
        this.selectedLinePaint = new Paint();
        this.selectedLinePaint.setStrokeWidth(1.0F);
        this.selectedLinePaint.setAntiAlias(false);
        this.selectedLinePaint.setColor(this.getResources().getColor(R.color.waveform_selected));
        this.selectedBkgndLinePaint = new Paint();
        this.selectedBkgndLinePaint.setAntiAlias(false);
        this.selectedBkgndLinePaint.setColor(this.getResources().getColor(R.color.waveform_unselected_bkgnd_overlay));
        this.mPlaybackLinePaint = new Paint();
        this.mPlaybackLinePaint.setAntiAlias(false);
        this.mPlaybackLinePaint.setColor(this.getResources().getColor(R.color.playback_indicator));
    }

    public void setPlaying(Boolean isplaying) {
        this.playing = isplaying;
    }

    public void setEnabled(boolean enable) {
        this.mVisualizer.setEnabled(enable);
    }

    public void updateVisualizer() {
        this.invalidate();
    }

    public void updateVisualizer(ArrayList<Integer> heights) {
        mHeights = heights;

    }

    public void adjustHeight(int halfHeight){
        parentHeight = halfHeight;

    }

    public int getViewHeight(){
        return getMeasuredHeight();
    }

    private int getAdjustedHeight(int i,int halfHeight){
        return (int)(((mHeights.get(i)/2.0)/parentHeight)*halfHeight);
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if(this.mHeights != null) {
            int measuredWidth = this.getMeasuredWidth() / 2;
            int measuredHeight = this.getMeasuredHeight();
            int start = this.offset;
            if(this.playing && measuredWidth < this.mHeights.size()) {
                start = this.mHeights.size() - measuredWidth;
            }

            int width = this.mHeights.size() - start;
            int ctr = measuredHeight / 2;
            if(width > measuredWidth) {
                width = measuredWidth;
            }

            int i;
            for(i = 0; i < width; ++i) {
                this.drawWaveformLine(canvas, i, 0, measuredHeight, this.selectedBkgndLinePaint);
                Paint paint = this.selectedLinePaint;
                int height = getAdjustedHeight(start+i,ctr);//mHeights.get(start + i);
                //height = height > ctr/6  ?height:0;//filter output
                this.drawWaveformLine(canvas, i, ctr - height, ctr + 1 + height, paint);
            }

            for(i = width; i < measuredWidth; ++i) {
                this.drawWaveformLine(canvas, i, 0, measuredHeight, this.selectedBkgndLinePaint);
            }

        }
    }

    protected void drawWaveformLine(Canvas canvas, int x, int y0, int y1, Paint paint) {
        canvas.drawLine((float)(2 * x), (float)y0, (float)(2 * x), (float)y1, paint);
        canvas.drawLine((float)(2 * x - 1), (float)y0, (float)(2 * x - 1), (float)y1, paint);
    }




    public boolean onTouchEvent(MotionEvent event) {
        switch(event.getAction()) {
            case 0:
                this.waveformTouchStart(event.getX());
                break;
            case 2:
                this.waveformTouchMove(event.getX());
        }

        return true;
    }

    public void setupVisualizerFxAndUI(int sessionId) {
        try {
            this.mVisualizer = new Visualizer(sessionId);
            this.mVisualizer.setCaptureSize(Visualizer.getCaptureSizeRange()[1]);
            this.mVisualizer.setDataCaptureListener(new Visualizer.OnDataCaptureListener() {
                public void onWaveFormDataCapture(Visualizer visualizer, byte[] bytes, int samplingRate) {
                    BeatsVisualizer3.this.updateVisualizer();
                }

                public void onFftDataCapture(Visualizer visualizer, byte[] bytes, int samplingRate) {
                    BeatsVisualizer3.this.updateVisualizer();
                }
            }, Visualizer.getMaxCaptureRate() / 2, false, true);
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    public void waveformTouchStart(float x) {
        this.mTouchDragging = true;
        this.mTouchStart = x;
    }

    public void waveformTouchMove(float x) {
        this.offset = this.trap((int)((float)this.offset + (this.mTouchStart - x) / 10.0F));
        this.invalidate();
    }

    private int trap(int pos) {
        return pos < 0?0:(pos > this.mLength?this.mLength:pos);
    }
}

