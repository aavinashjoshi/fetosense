package com.carenx.beatsv3.utils;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.media.audiofx.Visualizer;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import com.carenx.beatsv3.R;

import java.util.ArrayList;


/**
 * Created by avinash on 4/8/16.
 */

public class BeatsVisualizer2 extends View {
    ArrayList<Double> smoothedGains = new ArrayList<>();
    private int offset;
    private float mTouchStart;
    private boolean playing;
    private boolean mTouchDragging;
    private Paint mForePaint = new Paint();
    private float[] mBytes;
    private Visualizer mVisualizer;
    private ArrayList<Double> mValues;
    private int mLength = 0;
    private ArrayList<Integer> mHeights;
    private Paint selectedBkgndLinePaint;
    private Paint selectedLinePaint;
    private Paint mPlaybackLinePaint;
    private boolean plot = true;
    private int count = 0;
    private double front;
    private double center;
    private double last;
    private double previousGain = 0;
    private double alfa = 0.3;

    public BeatsVisualizer2(Context context) {
        super(context);
        this.initialize();
    }

    public BeatsVisualizer2(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.initialize();
    }

    public BeatsVisualizer2(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.initialize();
    }

    @TargetApi(21)
    public BeatsVisualizer2(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    void initialize() {
        this.mBytes = null;
        this.mForePaint.setStrokeWidth(1.0F);
        this.mForePaint.setAntiAlias(true);
        this.mForePaint.setColor(this.getResources().getColor(R.color.waveform_selected));
        this.selectedLinePaint = new Paint();
        this.selectedLinePaint.setStrokeWidth(1.0F);
        this.selectedLinePaint.setAntiAlias(false);
        this.selectedLinePaint.setColor(this.getResources().getColor(R.color.waveform_selected));
        this.selectedBkgndLinePaint = new Paint();
        this.selectedBkgndLinePaint.setAntiAlias(false);
        this.selectedBkgndLinePaint.setColor(this.getResources().getColor(R.color.waveform_unselected_bkgnd_overlay));
        this.mPlaybackLinePaint = new Paint();
        this.mPlaybackLinePaint.setAntiAlias(false);
        this.mPlaybackLinePaint.setColor(this.getResources().getColor(R.color.playback_indicator));
    }

    public void setPlaying(Boolean isplaying) {
        this.playing = isplaying;
    }

    public void setEnabled(boolean enable) {
        this.mVisualizer.setEnabled(enable);
    }

    public void updateVisualizer() {
        this.invalidate();
    }

    public void updateVisualizer(float[] bytes) {
        Log.i("Frame time",System.currentTimeMillis()+"");
        this.mBytes = bytes;
        double gain = this.getFrameGain(bytes);
        if(gain < 0.05D) {
            gain = 0.0D;
        }
        /*if(previousGain==0)
            previousGain=gain;
        else {
            gain = (int) (gain * alfa + (1 - alfa) * previousGain);
            previousGain = gain;
        }*/

        this.smoothedGains.add(gain);
        this.smoothedGains.add(gain);
        //this.smoothedGains.add(gain);
        this.mLength ++; // +2
        this.mLength ++;
        //this.mLength ++;
        this.smoothGains();
        this.computeDoubles();
        this.computeInts();
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if(this.mBytes != null) {
            int measuredWidth = this.getMeasuredWidth() / 2;
            int measuredHeight = this.getMeasuredHeight();
            int start = this.offset;
            if(this.playing && measuredWidth < this.mHeights.size()) {
                start = this.mHeights.size() - measuredWidth;
            }

            int width = this.mHeights.size() - start;
            int ctr = measuredHeight / 2;
            if(width > measuredWidth) {
                width = measuredWidth;
            }

            int i;
            for(i = 0; i < width; ++i) {
                this.drawWaveformLine(canvas, i, 0, measuredHeight, this.selectedBkgndLinePaint);
                Paint paint = this.selectedLinePaint;
                int height = mHeights.get(start + i);
                //height = height > ctr/4  ?height:0;//filter output
                this.drawWaveformLine(canvas, i, ctr - height, ctr + 1 + height, paint);
            }

            for(i = width; i < measuredWidth; ++i) {
                this.drawWaveformLine(canvas, i, 0, measuredHeight, this.selectedBkgndLinePaint);
            }

        }
    }

    protected void drawWaveformLine(Canvas canvas, int x, int y0, int y1, Paint paint) {
        canvas.drawLine((float)(2 * x), (float)y0, (float)(2 * x), (float)y1, paint);
        canvas.drawLine((float)(2 * x - 1), (float)y0, (float)(2 * x - 1), (float)y1, paint);
    }

    public double getFrameGain(float[] bytes) {
        float maxGain = 0.0F;
        int length = bytes.length;

        for(int j = 1; j < length; j += 4) {
            float val = Math.abs(bytes[j]);
            if(val > maxGain) {
                maxGain = val;
            }
        }

        Log.i("Test", "" + maxGain);
        return (double)maxGain;
    }

    private void computeDoubles() {
        double value = (smoothedGains.get(mLength - 1));// range;
        double value2 = (smoothedGains.get(mLength - 2));// range;
        //double value3 = (smoothedGains.get(mLength - 3));// range;

        Log.i("Test double", value + "");

       /* if(value < 0.0D) {
            value = 0.0D;
        }

        if(value > 1.0D) {
            value = 1.0D;
        }

        if(value2 < 0.0D) {
            value2 = 0.0D;
        }

        if(value2 > 1.0D) {
            value2 = 1.0D;
        }*/

        //mValues.add(value3 * value3);
        mValues.add(value2 * value2);
        mValues.add(value * value);
    }

    public void resetVisualizer() {
        mHeights = new ArrayList<>();
        mValues = new ArrayList<>();
        smoothedGains = new ArrayList<>();
        mLength = 0;
    }

    private void smoothGains() {
        if (mLength - 10 > 0) {
            for (int i = mLength - 9; i < mLength - 1; i++) {
                smoothedGains.set(i, (double) (
                        (smoothedGains.get(i - 1) / 3.0) +
                                (smoothedGains.get(i) / 3.0) +
                                (smoothedGains.get(i + 1) / 3.0)));
            }
            smoothedGains.set(mLength - 1, (double) (
                    (smoothedGains.get(mLength - 2) / 2.0) +
                            (smoothedGains.get(mLength - 1) / 2.0)));
        } else if (mLength > 2) {
            smoothedGains.set(0, (double) (
                    (smoothedGains.get(0) / 2.0) +
                            (smoothedGains.get(1) / 2.0)));
            for (int i = 1; i < mLength - 1; i++) {
                smoothedGains.set(i, (double) (
                        (smoothedGains.get(i - 1) / 3.0) +
                                (smoothedGains.get(i) / 3.0) +
                                (smoothedGains.get(i + 1) / 3.0)));
            }
            smoothedGains.set(mLength - 1, (double) (
                    (smoothedGains.get(mLength - 2) / 2.0) +
                            (smoothedGains.get(mLength - 1) / 2.0)));
        }

    }

    private void smoothHeights() {
        if (mLength - 10 > 0) {
            for (int i = mLength - 9; i < mLength - 1; i++) {
                mHeights.set(i, (
                        (mHeights.get(i - 1) / 3) +
                                (mHeights.get(i) / 3) +
                                (mHeights.get(i + 1) / 3)));

            }
            mHeights.set(mLength - 1,  (
                    (mHeights.get(mLength - 2) / 2) +
                            (mHeights.get(mLength - 1) / 2)));
        } else if (mLength > 2) {
            mHeights.set(0, (int) (
                    (mHeights.get(0) / 2.0) +
                            (mHeights.get(1) / 2.0)));
            for (int i = 1; i < mLength - 1; i++) {
                mHeights.set(i,   (
                        (mHeights.get(i - 1) / 3) +
                                (mHeights.get(i) / 3) +
                                (mHeights.get(i + 1) / 3)));
            }
            mHeights.set(mLength - 1,  (
                    (mHeights.get(mLength - 2) / 2 ) +
                            (mHeights.get(mLength - 1) / 2)));
        }

    }

    private void computeInts() {
        int halfHeight = this.getMeasuredHeight()/2;
        for (int i = 2; i >0; i--){
            /*if(((mValues.get(mLength - i) * halfHeight)*8)<=halfHeight)
                mHeights.add((int) (mValues.get(mLength - i) * halfHeight)*8);
            else if(((mValues.get(mLength - i) * halfHeight)*6)<=halfHeight)
                mHeights.add((int) (mValues.get(mLength - i) * halfHeight)*6);
            else if (((mValues.get(mLength - i) * halfHeight)*4)<=halfHeight)
                mHeights.add((int) (mValues.get(mLength - i) * halfHeight)*4);
            else if (((mValues.get(mLength - i) * halfHeight)*2)<=halfHeight)
                mHeights.add((int) (mValues.get(mLength - i) * halfHeight)*2);
            else
                mHeights.add((int) (mValues.get(mLength - i) * halfHeight));*/

            mHeights.add((int) (mValues.get(mLength - i) * halfHeight));
            Log.i("Testheight", mHeights.get(mLength - i)+","+halfHeight);
        }
        /*if()
        mHeights.add((int) (mValues.get(mLength - 3) * halfHeight)*3);
        mHeights.add((int) (mValues.get(mLength - 2) * halfHeight)*5);
        mHeights.add((int) (mValues.get(mLength - 1) * halfHeight)*3);*/
        smoothHeights();
        //Log.i("Test height", mValues.get(mLength - 1) * halfHeight + "");
    }

    public boolean onTouchEvent(MotionEvent event) {
        switch(event.getAction()) {
            case 0:
                this.waveformTouchStart(event.getX());
                break;
            case 2:
                this.waveformTouchMove(event.getX());
        }

        return true;
    }

    public void setupVisualizerFxAndUI(int sessionId) {
        try {
            this.mVisualizer = new Visualizer(sessionId);
            this.mVisualizer.setCaptureSize(Visualizer.getCaptureSizeRange()[1]);
            this.mVisualizer.setDataCaptureListener(new Visualizer.OnDataCaptureListener() {
                public void onWaveFormDataCapture(Visualizer visualizer, byte[] bytes, int samplingRate) {
                    BeatsVisualizer2.this.updateVisualizer();
                }

                public void onFftDataCapture(Visualizer visualizer, byte[] bytes, int samplingRate) {
                    BeatsVisualizer2.this.updateVisualizer();
                }
            }, Visualizer.getMaxCaptureRate() / 2, false, true);
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    public void waveformTouchStart(float x) {
        this.mTouchDragging = true;
        this.mTouchStart = x;
    }

    public void waveformTouchMove(float x) {
        this.offset = this.trap((int)((float)this.offset + (this.mTouchStart - x) / 20.0F));
        this.invalidate();
    }

    private int trap(int pos) {
        return pos < 0?0:(pos > this.mLength?this.mLength:pos);
    }
}
