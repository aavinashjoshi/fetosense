package com.carenx.beatsv3.utils;


import java.util.ArrayList;

public class CircularArrayList<E> extends ArrayList<E> {
    private int mSize;

    CircularArrayList(int size){
        mSize = size;
    }
    public void setSize(int size){
        mSize = size;
        while (this.size()>mSize)
            remove(0);
    }
    @Override
    public boolean add(E value) {
        if(this.size()==mSize)
            remove(0);
        return super.add(value);
    }
}
