package com.carenx.beatsv3.utils;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.media.audiofx.Visualizer;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.carenx.beatsv3.R;
import com.carenx.beatsv3.dsp.AudioEvent;
import com.carenx.beatsv3.dsp.AudioProcessor;

import java.util.ArrayList;


public class BeatsVisualizer4 extends View implements AudioProcessor {

    private Paint mForePaint = new Paint();
    private Visualizer mVisualizer;
    private CircularArrayList<Integer> mHeights = new CircularArrayList<>(350);
    private CircularArrayList<Double> smoothedGains = new CircularArrayList<>(350);
    private Paint selectedBkgndLinePaint;
    private Paint selectedLinePaint;
    private Paint mPlaybackLinePaint;

    public BeatsVisualizer4(Context context) {
        super(context);
        initialize();
    }

    public BeatsVisualizer4(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize();
    }

    public BeatsVisualizer4(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize();
    }

    @TargetApi(21)
    public BeatsVisualizer4(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    void initialize() {

        this.mForePaint.setStrokeWidth(1.0F);
        this.mForePaint.setAntiAlias(true);
        this.mForePaint.setColor(this.getResources().getColor(R.color.waveform_selected));
        this.selectedLinePaint = new Paint();
        this.selectedLinePaint.setStrokeWidth(1.0F);
        this.selectedLinePaint.setAntiAlias(false);
        this.selectedLinePaint.setColor(this.getResources().getColor(R.color.waveform_selected));
        this.selectedBkgndLinePaint = new Paint();
        this.selectedBkgndLinePaint.setAntiAlias(false);
        this.selectedBkgndLinePaint.setColor(this.getResources().getColor(R.color.waveform_unselected_bkgnd_overlay));
        this.mPlaybackLinePaint = new Paint();
        this.mPlaybackLinePaint.setAntiAlias(false);
        this.mPlaybackLinePaint.setColor(this.getResources().getColor(R.color.playback_indicator));
    }

    private int getAdjustedHeight(int i,int halfHeight){
        try {
            return mHeights.get(i);
        }catch(Exception ex){
            return 0;
        }
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if(this.mHeights != null) {
            int measuredWidth = this.getMeasuredWidth()/2 ;
            int measuredHeight = this.getMeasuredHeight();
            int start = 0;
            if(measuredWidth < this.mHeights.size()) {
                start = this.mHeights.size() - measuredWidth;
            }

            int width = this.mHeights.size() - start;
            int ctr = measuredHeight / 2;
            if(width > measuredWidth) {
                width = measuredWidth;
            }

            int i;
            for(i = 0; i < width; ++i) {
                this.drawWaveformLine(canvas, i, 0, measuredHeight, this.selectedBkgndLinePaint);
                Paint paint = this.selectedLinePaint;
                int height = getAdjustedHeight(start+i,ctr);//mHeights.get(start + i);
                this.drawWaveformLine(canvas, i, ctr - height, ctr + 1 + height, paint);
            }

            for(i = width; i < measuredWidth; ++i) {
                this.drawWaveformLine(canvas, i, 0, measuredHeight, this.selectedBkgndLinePaint);
            }

        }
        invalidate();
    }

    protected void drawWaveformLine(Canvas canvas, int x, int y0, int y1, Paint paint) {
        canvas.drawLine((float)(2 * x), (float)y0, (float)(2 * x), (float)y1, paint);
        canvas.drawLine((float)(2 * x - 1), (float)y0, (float)(2 * x - 1), (float)y1, paint);
    }



    @Override
    public boolean process(AudioEvent ae) {
        Log.i("Frame time", System.currentTimeMillis() + "");
        double gain = this.getFrameGain(ae.getFloatBuffer());
        Log.i("gain", gain + "");
        if (gain < 0.05D) {
            gain = 0.0D;
        }


        this.smoothedGains.add(gain*gain);
        this.smoothedGains.add(gain*gain);


        this.smoothGains();
        this.computeInts();

        return true;
    }

    @Override
    public void processingFinished() {


    }


    private double getFrameGain(float[] bytes) {
        float maxGain = 0.0F;
        int length = bytes.length;

        for (int j = 1; j < length; j += 4) {
            float val = Math.abs(bytes[j]);
            if (val > maxGain) {
                maxGain = val;
            }
        }

        Log.i("Test", "" + maxGain);
        return (double) maxGain;
    }



    private void smoothGains() {
        int length = smoothedGains.size();
        if (length - 10 > 0) {
            for (int i = length - 9; i < length - 1; i++) {
                smoothedGains.set(i, (double) (
                        (smoothedGains.get(i - 1) / 3.0) +
                                (smoothedGains.get(i) / 3.0) +
                                (smoothedGains.get(i + 1) / 3.0)));
            }
            smoothedGains.set(length - 1, (double) (
                    (smoothedGains.get(length - 2) / 2.0) +
                            (smoothedGains.get(length - 1) / 2.0)));
        } else if (length > 2) {
            smoothedGains.set(0, (double) (
                    (smoothedGains.get(0) / 2.0) +
                            (smoothedGains.get(1) / 2.0)));
            for (int i = 1; i < length - 1; i++) {
                smoothedGains.set(i, (double) (
                        (smoothedGains.get(i - 1) / 3.0) +
                                (smoothedGains.get(i) / 3.0) +
                                (smoothedGains.get(i + 1) / 3.0)));
            }
            smoothedGains.set(length - 1, (double) (
                    (smoothedGains.get(length - 2) / 2.0) +
                            (smoothedGains.get(length - 1) / 2.0)));
        }

    }

    private void smoothHeights() {
        int length = mHeights.size();
        if (length - 10 > 0) {
            for (int i = length - 9; i < length - 1; i++) {
                mHeights.set(i, (
                        (mHeights.get(i - 1) / 3) +
                                (mHeights.get(i) / 3) +
                                (mHeights.get(i + 1) / 3)));

            }
            mHeights.set(length - 1, (
                    (mHeights.get(length - 2) / 2) +
                            (mHeights.get(length - 1) / 2)));
        } else if (length > 2) {
            mHeights.set(0, (int) (
                    (mHeights.get(0) / 2.0) +
                            (mHeights.get(1) / 2.0)));
            for (int i = 1; i < length - 1; i++) {
                mHeights.set(i, (
                        (mHeights.get(i - 1) / 3) +
                                (mHeights.get(i) / 3) +
                                (mHeights.get(i + 1) / 3)));
            }
            mHeights.set(length - 1, (
                    (mHeights.get(length - 2) / 2) +
                            (mHeights.get(length - 1) / 2)));
        }

    }

    private void computeInts() {
        int halfHeight = this.getMeasuredHeight() / 2;
        for (int i = 2; i > 0; i--) {
            mHeights.add((int) (smoothedGains.get(smoothedGains.size() - i) * halfHeight) );
            Log.i("Testheight", mHeights.get(smoothedGains.size() - i) + "," + halfHeight);
        }
        smoothHeights();
    }

}
