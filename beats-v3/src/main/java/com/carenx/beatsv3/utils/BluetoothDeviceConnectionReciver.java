package com.carenx.beatsv3.utils;

import android.bluetooth.BluetoothHeadset;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

/**
 * Created by avinash on 29/12/16.
 */

public class BluetoothDeviceConnectionReciver extends BroadcastReceiver {


    public static final String GATT_CONNECTED = "GATT_CONNECTED";
    public static final String GATT_DISCONNECTED = "GATT_DISCONNECTED";

    public static final String ACTION_ON_DOPPLER_CONNECTED = "com.carenx.feto.doppler.state_connected";
    public static final String ACTION_ON_DOPPLER_DISCONNECTED = "com.carenx.feto.doppler.state_disconnected";
    public static final String CUSTOM_ACTION_ON_CONNECTED = "com.carenx.feto.marker.state_connected";
    public static final String CUSTOM_ACTION_ON_SINGLE_CLICK = "com.carenx.feto.marker.single_click";
    public static final String CUSTOM_ACTION_ON_DOUBLE_CLICK = "com.carenx.feto.marker.double_click";
    public static final String CUSTOM_ACTION_ON_DISCONNECTED = "com.carenx.feto.marker.state_disconnected";
    private LocalBroadcastManager bm;

    void BluetoothDeviceConnectionReciver(Context context) {
        /*bm = LocalBroadcastManager.getInstance(context);
        bm.registerReceiver(this, new IntentFilter(GATT_CONNECTED));
        bm.registerReceiver(this, new IntentFilter(GATT_DISCONNECTED));*/
    }

    OnBluetoothDeviceStateChangeLisener onBluetoothStateChangedListener;

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        //int headsetAudioState = intent.getIntExtra("android.bluetooth.headset.extra.AUDIO_STATE", -2);

        //Toast.makeText(context, "New Test", Toast.LENGTH_SHORT).show();

        //BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
        Log.i("headsetAudioState", "in broadcast");


        /*if (BluetoothHeadset.ACTION_CONNECTION_STATE_CHANGED.equals(action)) {
            int bluetoothHeadsetState = intent.getIntExtra(BluetoothHeadset.EXTRA_STATE,
                    BluetoothHeadset.STATE_DISCONNECTED);
            Log.i("headsetAudioState", bluetoothHeadsetState + " ");
            //Device found
            if (bluetoothHeadsetState == BluetoothHeadset.STATE_CONNECTED) {
                if (onBluetoothStateChangedListener != null)
                    onBluetoothStateChangedListener.OnBluetoothHeadsetConnected();
                Log.i("headsetAudioState", bluetoothHeadsetState + " connected");
            }
            if (bluetoothHeadsetState == BluetoothHeadset.STATE_DISCONNECTED) {
                if (onBluetoothStateChangedListener != null)
                    onBluetoothStateChangedListener.OnBluetoothHeadsetDisconnected();
                Log.i("headsetAudioState", bluetoothHeadsetState + " disconnected");
            }
        }*/

        if (ACTION_ON_DOPPLER_CONNECTED.equals(action)) {
            if (onBluetoothStateChangedListener != null)
                onBluetoothStateChangedListener.OnBluetoothHeadsetConnected();
        } else if (ACTION_ON_DOPPLER_DISCONNECTED.equals(action)) {
            if (onBluetoothStateChangedListener != null)
                onBluetoothStateChangedListener.OnBluetoothHeadsetDisconnected();
        }

        /*if (BluetoothDevice.ACTION_FOUND.equals(action)) {
            //Device found
            if (onBluetoothStateChangedListener != null)
                onBluetoothStateChangedListener.OnBluetoothHeadsetConnected();
        } else*/
        /*if (BluetoothDevice.ACTION_ACL_CONNECTED.equals(action)) {
            //Device is now connected
            if (onBluetoothStateChangedListener != null)
                onBluetoothStateChangedListener.OnBluetoothHeadsetConnected();
        } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
            //Done searching
        } else if (BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED.equals(action)) {
            //Device is about to disconnect
        } else if (BluetoothDevice.ACTION_ACL_DISCONNECTED.equals(action)) {
            //Device has disconnected
            if (onBluetoothStateChangedListener != null)
                onBluetoothStateChangedListener.OnBluetoothHeadsetDisconnected();

        } else */
        if (action.equals(CUSTOM_ACTION_ON_SINGLE_CLICK)) {
            //Toast.makeText(context, "inside movement brodcast", Toast.LENGTH_SHORT).show();
            if (onBluetoothStateChangedListener != null)
                onBluetoothStateChangedListener.OnBluetoothSingleKeyPress();
        } else if (action.equals(CUSTOM_ACTION_ON_DOUBLE_CLICK)) {
            //Toast.makeText(context, "inside movement brodcast", Toast.LENGTH_SHORT).show();
            if (onBluetoothStateChangedListener != null)
                onBluetoothStateChangedListener.OnBluetoothDoubleKeyPress();
        }else if (action.equals(CUSTOM_ACTION_ON_CONNECTED)) {
            //Toast.makeText(context, "inside movement brodcast", Toast.LENGTH_SHORT).show();
            if (onBluetoothStateChangedListener != null)
                onBluetoothStateChangedListener.OnBluetoothMarkerStateChanged(true);
        }else if (action.equals(CUSTOM_ACTION_ON_DISCONNECTED)) {
            //Toast.makeText(context, "inside movement brodcast", Toast.LENGTH_SHORT).show();
            if (onBluetoothStateChangedListener != null)
                onBluetoothStateChangedListener.OnBluetoothMarkerStateChanged(false);
        }

        }

    public void setOnBluetoothStateChangeListener(OnBluetoothDeviceStateChangeLisener listener) {
        onBluetoothStateChangedListener = listener;
    }

    public interface OnBluetoothDeviceStateChangeLisener {
        void OnBluetoothHeadsetConnected();

        void OnBluetoothHeadsetDisconnected();

        void OnBluetoothMarkerStateChanged(boolean state);

        void OnBluetoothSingleKeyPress();

        void OnBluetoothDoubleKeyPress();
    }
}
