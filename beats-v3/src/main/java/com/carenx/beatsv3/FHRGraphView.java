package com.carenx.beatsv3;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.RectF;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Base64;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

import static com.carenx.beatsv3.NstView.minPerGrid;

/**
 * Created by avinash on 24/11/17.
 */

public class FHRGraphView {


    private static final int WIDTH_PX = 2620;
    private static final int HEIGHT_PX = 1770;

    private float pixelsPerOneMM;
    private float pixelsPerOneCM;


    float touched_down=0;
    float touched_up=0;
    private Paint graphOutlines;
    private Paint graphGridLines;
    private Paint graphBackGround;
    private Paint graphSafeZone;
    private Paint graphAxisText;
    private Paint graphGridSubLines;
    private float yDivLength;
    private float yOrigin;
    private float axisFontSize;
    private float paddingLeft;
    private float paddingTop;
    private float paddingBottom;
    private float paddingRight;
    private float xOrigin;
    private Paint graphBpmLine;
    private float xDivLength;
    private float xDiv;
    private int screenHeight;
    private int screenWidth;
    private float xAxisLength;
    private float yDiv;
    private float yAxisLength;
    private Canvas canvas[];
    private Bitmap bitmaps[];
    private float timeScaleFactor;
    private int pointsPerPage;
    private Paint informationText;
    private int XDIV =25;
    private int pointsPerDiv;
    private GraphExtraData mData;

    public FHRGraphView() {
        initialize();

    }



    public Bitmap[] getNSTGraph(ArrayList<Integer> bpmList,ArrayList<Integer> movementList,GraphExtraData data){

        mData = data;
        pointsPerPage = (int)(10*timeScaleFactor*XDIV);
        pointsPerDiv = (int)timeScaleFactor*10;
        int pages =bpmList.size()/pointsPerPage;
        pages+=1;

        bitmaps = new Bitmap[pages];
        canvas = new Canvas[pages];

        drawGraph(pages);
        drawBpmLine(bpmList,pages);
        drawMovements(movementList,pages);
        return bitmaps;

    }

    private void initialize(){

        timeScaleFactor = minPerGrid==1?6:2;

        pixelsPerOneCM = 100f;
        pixelsPerOneMM = 10f;

        graphOutlines = new Paint();
        graphOutlines.setStrokeWidth(3f);
        graphOutlines.setColor(Color.BLACK);

        graphGridLines = new Paint();
        graphGridLines.setStrokeWidth(2.5f);
        graphGridLines.setColor(Color.GRAY);

        graphGridSubLines = new Paint();
        graphGridSubLines.setStrokeWidth(1.5f);
        graphGridSubLines.setColor(Color.LTGRAY);

        graphBackGround = new Paint();
        graphBackGround.setColor(Color.WHITE);

        graphSafeZone = new Paint();
        graphSafeZone.setStrokeWidth(1f);
        graphSafeZone.setColor(Color.argb(40,100,200,0));

        graphAxisText = new Paint();
        graphAxisText.setStrokeWidth(1f);
        graphAxisText.setTextSize(30f);
        graphAxisText.setColor(Color.BLACK);

        graphBpmLine = new Paint();
        graphBpmLine.setColor(Color.BLUE);
        graphBpmLine.setStrokeWidth(5);
        graphBpmLine.setStyle(Style.STROKE);

        informationText = new Paint();
        informationText.setTextSize(40f);
        informationText.setColor(Color.BLACK);

        axisFontSize=pixelsPerOneMM*5;

        paddingLeft= pixelsPerOneCM;
        paddingTop=pixelsPerOneCM*5;
        paddingBottom=pixelsPerOneCM*3;
        paddingRight=pixelsPerOneMM;

    }

    private void drawGraph(int pages) {


        screenHeight = HEIGHT_PX;//canvas.getHeight();//1440
        screenWidth = WIDTH_PX;//canvas.getWidth();//2560

        xOrigin = paddingLeft;
        yOrigin = screenHeight - (paddingBottom + axisFontSize);

        yAxisLength = yOrigin - paddingTop;
        xAxisLength = screenWidth - paddingLeft - paddingRight;


        xDivLength = pixelsPerOneCM;
        xDiv = (screenWidth - xOrigin - paddingRight) / pixelsPerOneCM;

        yDivLength = xDivLength / 2;
        yDiv = (screenHeight - paddingTop - paddingBottom - axisFontSize) / pixelsPerOneCM * 2;

        for(int pageNumber = 0;pageNumber<pages;pageNumber++) {
            Bitmap.Config conf = Bitmap.Config.ARGB_8888; // see other conf types
            bitmaps[pageNumber] = Bitmap.createBitmap(screenWidth, screenHeight, conf); // this creates a MUTABLE bitmap
            canvas[pageNumber] = new Canvas(bitmaps[pageNumber]);
            canvas[pageNumber].drawPaint(graphBackGround);

            displayInformation(pageNumber);

            drawXAxis(pageNumber);

            drawYAxis(pageNumber);

        }
        //invalidate();
    }

    private void drawXAxis(int pageNumber){
        int numberOffset = XDIV*(pageNumber);
        for(int j=1;j<2;j++){

            canvas[pageNumber].drawLine(xOrigin+((xDivLength/2)*j),
                    paddingTop,
                    xOrigin+((xDivLength/2)*j),
                    yOrigin, graphGridSubLines);


        }

        for(int i=1;i<=xDiv;i++){
            canvas[pageNumber].drawLine(xOrigin+(xDivLength*i), paddingTop,
                    xOrigin+(xDivLength*i), yOrigin, graphGridLines);

            for(int j=1;j<2;j++){
                canvas[pageNumber].drawLine(xOrigin+(xDivLength*i)+((xDivLength/2)*j),
                        paddingTop,
                        xOrigin+(xDivLength*i)+((xDivLength/2)*j),
                        yOrigin, graphGridSubLines);
            }

            //if(i!=1)
            canvas[pageNumber].drawText(String.format("%2d",i+numberOffset),
                    xOrigin+(xDivLength*i)-
                            (graphAxisText.measureText("00")/2) ,
                    yOrigin+axisFontSize*3, graphAxisText);


        }

    }

    private void drawYAxis(int pageNumber){
        //y-axis outlines
        canvas[pageNumber].drawLine(xOrigin,yOrigin,screenWidth-paddingRight,
                yOrigin,graphOutlines);
        canvas[pageNumber].drawLine(xOrigin,paddingTop,screenWidth-paddingRight,
                paddingTop,graphOutlines);

        int interval=10;
        int ymin=50;
        int safeZoneMax = 160;

        //SafeZone
        RectF safeZoneRect = new RectF();
        safeZoneRect.set(xOrigin,
                (float)(yOrigin-yDivLength)-((float)(safeZoneMax-ymin)/interval)*yDivLength ,
                xOrigin+xAxisLength,
                yOrigin-yDivLength*8);//50
        canvas[pageNumber].drawRect(safeZoneRect, graphSafeZone);

        for(int i=1;i<=yDiv;i++){
            if(i%2==0) {
                canvas[pageNumber].drawLine(xOrigin, yOrigin - (yDivLength * i),
                        xOrigin + xAxisLength, yOrigin - (yDivLength * i), graphGridLines);

                canvas[pageNumber].drawText("" + (ymin + (interval * (i - 1))), pixelsPerOneMM,
                        yOrigin - (yDivLength * i) + axisFontSize / 2, graphAxisText);

                canvas[pageNumber].drawLine(xOrigin, yOrigin - (yDivLength * i)+yDivLength/2,
                        xOrigin + xAxisLength, yOrigin - (yDivLength * i)+yDivLength/2, graphGridSubLines);

            }else {
                canvas[pageNumber].drawLine(xOrigin, yOrigin - (yDivLength * i),
                        xOrigin + xAxisLength, yOrigin - (yDivLength * i), graphGridSubLines);
                canvas[pageNumber].drawLine(xOrigin, yOrigin - (yDivLength * i)+yDivLength/2,
                        xOrigin + xAxisLength, yOrigin - (yDivLength * i)+yDivLength/2, graphGridSubLines);
            }
        }



    }

    private void displayInformation(int pageNumber){

        int rows = 3;

        String now = new Date(NstView.time).toString();
        String date = String.format("%s %s %s", now.substring(11, 16), now.substring(4, 10), now.substring(now.lastIndexOf(" ")));

        float rowLength =  (screenWidth-(pixelsPerOneCM*2))/rows;

        canvas[pageNumber].drawText("Hospital : "+mData.getHospitalName(),
                pixelsPerOneCM ,
                pixelsPerOneCM,
                informationText);
        canvas[pageNumber].drawText("Doctor : "+mData.getDoctorName(),
                pixelsPerOneCM,
                pixelsPerOneCM*2,
                informationText);

        canvas[pageNumber].drawText("Mother : "+mData.getMotherName(),
                pixelsPerOneCM ,
                pixelsPerOneCM * 3,
                informationText);

        canvas[pageNumber].drawText("Gest. Week : "+mData.getGestWeek(),
                pixelsPerOneCM + rowLength,
                pixelsPerOneCM,
                informationText);
        canvas[pageNumber].drawText("Weight : "+mData.getWeight(),
                pixelsPerOneCM + rowLength,
                pixelsPerOneCM*2,
                informationText);

        canvas[pageNumber].drawText(String.format("OB. Hist : G __ P __ A __ L __"),
                pixelsPerOneCM + rowLength,
                pixelsPerOneCM * 3,
                informationText);


        canvas[pageNumber].drawText("Date Time : "+date,
                pixelsPerOneCM + rowLength*2 ,
                pixelsPerOneCM ,
                informationText);

        canvas[pageNumber].drawText(String.format("X-Axis : %s sec/div", timeScaleFactor*10),
                pixelsPerOneCM + rowLength*2,
                pixelsPerOneCM * 2,
                informationText);

        canvas[pageNumber].drawText("Y-Axis : 20 BPM/DIV",
                pixelsPerOneCM +rowLength*2,
                pixelsPerOneCM * 3,
                informationText);


    }

    private void drawBpmLine(ArrayList<Integer> bpmList,int pages){
        if(bpmList==null || bpmList.size()==0)
            return;

        float increment = (pixelsPerOneMM /timeScaleFactor) ;

        for (int pageNumber = 0;pageNumber<pages;pageNumber++) {

            Path path = new Path();
            float k = xOrigin+pixelsPerOneMM;
            path.moveTo(k, getYValueFromBPM(bpmList.get(pageNumber*pointsPerPage)));

            for (int i = 1+(pageNumber*pointsPerPage),j=0; i<bpmList.size() && j < pointsPerPage; i++,j++) {

                path.lineTo(k, getYValueFromBPM(bpmList.get(i)));
                k += increment;
            }

            canvas[pageNumber].drawPath(path, graphBpmLine);
            canvas[pageNumber].drawText(String.format("page %2d of %d",pageNumber+1,pages),
                    screenWidth-paddingRight-
                            (graphAxisText.measureText("page 1 of 10")) ,
                    screenHeight-pixelsPerOneMM*2, graphAxisText);
        }


    }

    private void drawMovements(ArrayList<Integer> movementList,int pages){

        for (int pageNumber = 0;pageNumber<pages;pageNumber++) {
            if(movementList==null || movementList.size()==0)
                return;
            for(int i=0;i<movementList.size();i++) {
                int movement = movementList.get(i)-(pageNumber*pointsPerPage);
                if (movement > 0 && movement < pointsPerPage)
                    canvas[pageNumber].drawBitmap(movementBitmap,
                            xOrigin+(pixelsPerOneMM/timeScaleFactor*(movement)-(movementBitmap.getWidth()/2)),
                            yOrigin+pixelsPerOneMM, null);

            }
        }

        //Testing dummy movements
       /* for (int pageNumber = 0;pageNumber<pages;pageNumber++) {
            int move[] = {2, 12, 24,60, 120, 240, 300, 420, 600,690, 1220, 1240, 1300, 1420, 1600};
            for (int i = 0; i < move.length; i++) {

               if (move[i]-(pageNumber*pointsPerPage) > 0 && move[i]-(pageNumber*pointsPerPage) < pointsPerPage)
                    canvas[pageNumber].drawBitmap(movementBitmap,
                            xOrigin+(pixelsPerOneMM/timeScaleFactor*(move[i]-(pageNumber*pointsPerPage))-(movementBitmap.getWidth()/2)),
                            yOrigin+pixelsPerOneMM, null);


            }
        }*/
    }

    private int scaleOrigin = 40;
    private float getYValueFromBPM(int bpm){
        float adjustedBPM = bpm - scaleOrigin;
        adjustedBPM=adjustedBPM/2; //scaled down version for mobile phone
        float y_value = yOrigin-(adjustedBPM * pixelsPerOneMM);
        Log.i("bpmvalue",bpm+" "+adjustedBPM+" "+y_value);
        return y_value;

    }



    String movmentImageBase64 = "iVBORw0KGgoAAAANSUhEUgAAABwAAAAoCAYAAADt5povAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH4QsbCTMS6Qes/wAAABl0RVh0Q29tbWVudABDcmVhdGVkIHdpdGggR0lNUFeBDhcAAAEySURBVFjD7dY/S0JRGMfxb9Kgi2A1CNIiiGM4NATtDUkvQoeW2hV6DUZDgm8g6BUEYkRLvgJBaVGDmmpQnCprOU9cLvee+9cL0nngN53n3A+c89zLhTWpTZXEqgvcJYVdAD8qjVVjhxZMcrAqbBeYO4AzoBA3tgE8OGCSXtzgtQaTXMWFnQDfPsAvoBoVKwNLH5hkCZTCYhlgGACTDIB0GPA2BCa5CYqdRcAkp36xihqAqOAnsOeF7QAfMWCSd2BbBz56TGCYtXs3rOOyYQScA2+ah76qnmeX9bYdO7Y1zIEWsK9GPAtMNeBY9aTVnktgYes5EqxgOZYnoAbkHO72RQNOgC3bnhxQB/qWI8/LC94Eih7DFBS0VlEZvj8IUcG/SiX9M2RAAxrQgAY0oAH/A/gLVhjmv8J4WgsAAAAASUVORK5CYII=";
    byte[] movementData = Base64.decode(movmentImageBase64, Base64.DEFAULT);

    Bitmap movementBitmap = BitmapFactory.decodeByteArray(movementData, 0, movementData.length);

}
