package com.carenx.beatsv3;

import android.animation.ObjectAnimator;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.SystemClock;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Html;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.CheckBox;
import android.widget.Chronometer;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.carenx.beatsv3.utils.BeatsVisualizer3;
import com.carenx.beatsv3.utils.BeatsVisualizer4;
import com.carenx.beatsv3.utils.Peaks;

import java.io.File;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import static android.content.Context.VIBRATOR_SERVICE;
import static com.carenx.beatsv3.BeatsRecorder.SAMPLE_RATE;

/**
 * Created by avinash on 10/11/16.
 */


public class NstView extends LinearLayout {


    public static TextView txtBpm;
    private static int nstTime = 21;
    public static int minPerGrid = 1;
    ArrayList<Double> smoothedGains = new ArrayList<>();
    private float[] mBytes;
    private int mLength;
    private ArrayList<Integer> mHeights;
    private ArrayList<Double> mValues;
    private int count = 0;
    private int lastIndex = 0;
    private int frameRate;
    private int threshold;
    //private GraphView graph;
    private BeatsVisualizer4 visualizer;
    private Handler handler;
    private AlphaAnimation beatAnimation;
    private CheckBox radioListen;
    private NSTViewTouchListener listener;
    private int previous = 0;
    private double alfa = 0.6;
    private int fhr = 0;
    private double previousGain = 0;
    private double previousHeight = 0;
    public static String NST_MOVEMENTS = "com.carenx.beatsv3.movements";
    private BroadcastReceiver movementsReciver;
    private TextView txtMovements;
    private int movementCount = 0;
    private int graphFHREnteryCount = 0;
    private LocalBroadcastManager bm;
    private CheckBox radioPause;
    private ProgressBar timeProgressBar;
    private long startTime;
    private Chronometer chTime;
    private long mLastStopTime = 0;
    private long totalPauseTime = 0;
    private boolean playing = false;
    private long lastMovementEntryTime = 0;
    private long firstBPMEntryTime = 0;
    //private TextView txtName;
    //private TextView txtAge;
    //private LinearLayout llPresonalInfo;
    //private TextView txtMobileNo;
    //private TextView txtDate;
    //private TextView tvTime;
    private static int NSTTIME = 4 * 20;
    private RelativeLayout movementTile;
    private RelativeLayout fhrTile;
    private long clickTime;
    //private Ringtone alert;
    private Ringtone timer;
    public static ArrayList<Integer> graphBpmEntries ;
    public static ArrayList<Integer> graphMovmentEntries;
    public static long time;
    private LinearLayout llStartStop;
    private boolean mDopplerState;
    private boolean mMarkerState;
    private ProgressBar movementProgressBar;
    //private AnimationDrawable animationDrawable;

    /*int[] array = {164,152,147,144,142,141,139,139,139,139,139,139,138,137,140,142,142,142,145,143,142,140,140,139,137,139,142,
            142,142,143,141,139,140,138,138,138,137,137,140,139,139,140,141,144,142,140,140,140,139,138,139,137,136,136,134,137,
            137,138,135,137,141,134,131,132,132,135,135,136,139,140,152,152,155,152,158,163,155,153,150,144,144,144,144,141,139,
            136,134,130,130,131,132,133,131,128,131,131,133,135,140,164,152,147,144,142,141,139,139,139,139,139,139,138,137,140,
            142,142,142,145,143,142,140,140,139,137,139,142,142,142,143,141,139,140,138,138,138,137,137,140,139,139,140,141,144,
            142,140,140,140,139,138,139,137,136,136,134,137,137,138,135,137,141,134,131,132,132,135,135,136,139,140,152,152,155,
            152,158,163,155,153,150,144,144,144,144,141,139,136,134,130,130,131,132,133,131,128,131,131,133,135,140,140,141,147,
            150,151,147,150,148,141,139,139,140,140,140,141,139,141,141,139,145,149,149,147,148,145,142,140,141,147,153,155,156,
            159,163,163,154,150,145,143,140,141,141,144,147,148,147,142,141,139,140,154,148,146,143,142,141,139,139,139,154,148,
            146,143,142,141,139,139,139,139,139,139,138,137,140,142,142,142,145,143,142,140,140,139,137,139,142,142,142,143,141,
            139,140,138,138,138,137,137,140,139,139,140,141,144,142,140,140,140,139,138,139,137,136,136,134,137,137,138,135,137,
            141,134,131,132,132,135,135,136,139,140,152,152,155,152,158,163,155,153,150,144,144,144,144,141,139,136,134,130,130,
            131,132,133,131,164,152,147,144,142,141,139,139,139,139,139,139,138,137,140,142,142,142,145,143,142,140,140,139,137,
            139,142,142,142,143,141,139,140,138,138,138,137,137,140,139,139,140,141,144,142,140,140,140,139,138,139,137,136,136,
            134,152,147,145,143,142,141,139,139,139,139,139,139,138,137,140,142,142,142,145,143,142,140,140,139,137,164,152,164,
            152,147,144,142,141,139,139,139,139,139,139,138,137,140,142,142,142,145,143,142,140,140,139,137,139,142,142,142,143,
            141,139,140,138,138,138,137,137,140,139,139,140,141,144,142,140,140,140,139,138,139,137,136,136,134,137,137,138,135,
            137,141,134,131,132,132,135,135,136,139,140,152,152,155,152,158,163,155,153,150,144,144,144,144,141,139,136,134,130,
            130,131,132,133,131,128,131,131,133,135,140,140,141,147,150,151,147,150,148,141,139,139,140,140,140,141,139,141,141,
            139,145,149,149,147,148,145,142,140,141,147,153,155,156,159,163,163,154,150,145,143,140,141,141,144,147,148,147,142,
            141,139,140,141,144,143,143,143,141,142,141,141,143,142,141,140,140,141,141,141,143,143,142,143,143,146,148,149,150,
            150,150,153,154,154,150,150,144,138,134,128,131,135,143,158,165,175,169,165,169,164,156,145,142,136,134,130,131,129,
            133,138,143,144,145,148,151,154,153,153,153,156,152,149,146,142,137,137,140,148,151,159,168,168,168,161,162,154,141,
            143,164,152,147,144,142,141,139,139,139,139,139,139,154,148,146,143,164,152,147,144,142,141,139,139,139,139,139,139,
            138,137,140,142,142,142,145,164,152,147,144,142,141,139,164,152,164,152,147,144,142,141,139,139,139,139,139,139,164,
            152,147,144,142,141,139,139,139,139,139,139,138,137,140,142,142,142,145,143,142,143,141,139,137,139,142,142,142,143,
            141,139,140,138,138,138,137,137,140,139,139,140,141,141,141,140,140,140,139,138,139,137,136,136,134,137,137,137,135,
            137,141,134,131,132,132,135,135,136,140,153,167,170,173,177,181,172,158,155,156,153,153,153,154,151,145,138,135,130,
            130,131,132,133,131,128,131,131,133,135,140,140,141,148,151,164,152,147,144,142,141,139,154,148,146,143,136,142,142,
            141,142,142,144,144,142,144,145,146,145,140,138,133,136,137,132,136,136,138,142,144,145,146,147,148,151,149,148,148,
            149,150,147,144,142,138,138,136,137,135,143,146,145,139,139,136,136,136,139,145,143,146,145,144,144,144,145,143,146,
            145,144,144,144,145,143,139,137,137,143,144,137,135,133,123,130,131,136,145,142,141,142,142,146,140,145,151,155,156,
            157,157,151,145,142,142,142,141,151,151,145,148,143,141,142,143,139,137,137,137,134,131,126,128,129,130,130,127,128,
            134,135,136,136,135,136,137,139,145,145,146,143,141,142,134,134,132,135,137,138,138,139,139,139,140,140,140,141,142,
            140,141,140,140,141,141,141,141,141,141,141,139,141,141,140,141,141,141,141,140,140,140,140,138,139,139,140,140,139,
            137,139,137,129,127,124,121,116,115,115,116,126,125,130,124,131,132,134,141,143,141,136,135,135,127,125,129,129,131,
            135,136,136,137,138,138,138,138,138,141,141,142,142,142,145,144,144,144,144,144,143,145,145,145,144,143,146,145,145,
            145,145,146,146,147,146,147,146,145,146,145,146,144,144,144,144,144,142,145,145,143,145,143,143,143,141,144,145,144,
            140,143,144,144,146,146,152,149,148,147,146,142,141,138,136,137,141,144,145,152,152,146,143,141,141,139,132,133,136,
            131,127,131,134,137,140,143,143,146,147,149,150,150,151,150,151,141,139,140,141,136,128,137,138,130,138,142,145,144,
            144,142,142,141,139,140,142,143,141,143,143,141,139,140,140,139,140,142,143,142,145,144,144,144,145,146,146,146,146,
            146,145,145,145,145,145,145,145,144,142,140,136,132,127,128,128,129,131,134,136,136,138,138,138,136,134,136,135,135,
            137,137,137,137,137,138,138,135,133,133,130,128,126,125,123,120,123,123,124,125,126,129,129,129,129,129,131,123,124,
            130,131,132,131,136,138,139,140,140,140,140,135,132,131,129,133,131,134,137,138,137,137,137,135,135,134,135,135,134,
            136,136,136,136,137,136,134,135,138,136,135,135,137,135,135,134,135,135,135,137,137,138,138,139,138,139,138,136,137,
            138,139,139,140,140,141,140,142,142,147,149,150,151,149,149,143,139,139,139,139,139,143,141,137,133,133,134,131,132,
            137,142,142,143,145,148,164,152,147,144,142,141,139,139,139,139,139,139,138,137,140,142,142,142,145,143,142,140,140,
            139,137,139,142,142,142,143,141,139,140,138,138,138,137,137,140,139,139,140,141,144,142,140,140,140,139,138,139,137,
            136,136,134,137,137,138,135,137,141,134,131,132,132,135,135,136,139,140,152,152,155,152,158,163,155,153,150,144,144,
            144,144,141,139,136,134,130,130,131,132,164,152,147,144,142,141,139,139,139,139,139,139,138,137,140,142,142,142,145,
            143,142,140,140,139,137,139,142,142,142,143,141,139,140,138,138,138,137,137,140,139,139,140,141,144,142,140,140,140,
            139,138,139,137,136,136,134,137,137,138,135,137,141,134,131,132,132,135,135,136,139,140,152,152,155,152,158,163,155,
            153,150,144,144,144,144,141,139,136,134,130,130,131,132,133,131,128,131,131,133,135,140,140,141,147,150,151,147,150,
            };
    int[] marray = {20,60,99,144,178,190,222,265,298,333,340,377,400,430,459,600,700,777,
            800,840,900,950,1000,1100,1190,1230,1290,1330,1440,1500};

    void initArray(){
        for(int a : array){
            graphBpmEntries.add(a);
        }
        for(int a : marray){
            graphMovmentEntries.add(a);
        }
    }*/

    public NstView(Context context) {
        super(context);
        initializeLayoutBasics(context);
    }

    public NstView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initializeLayoutBasics(context);
    }

    public NstView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initializeLayoutBasics(context);
    }

    private void initializeLayoutBasics(Context context) {
        setOrientation(HORIZONTAL);
        final LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.nst_view, this);

        this.invalidate();

        findViews();
        setListeners();
        initialize();

    }


    public static void setNstTime(int time) {
        NSTTIME = 4 * time;
        nstTime = time;
    }

    public static void setMinPerGridValue(int value) {
        minPerGrid = value;
    }

    public static int getMinPerGridValue() {
        return minPerGrid;
    }

    public void setTouchListener(NSTViewTouchListener _listener) {
        listener = _listener;
    }


    final Handler startHandler = new Handler();
    Runnable enableStartButton = new Runnable() {
        @Override
        public void run() {
            radioListen.setEnabled(true);
        }
    };

    private void setListeners() {
        /*radioListen.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                doClickAction();

            }
        });*/
        llStartStop.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                shakeItBaby();
                radioListen.performClick();
                return false;
            }
        });
        radioListen.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                compoundButton.setEnabled(false);
                startHandler.postDelayed(enableStartButton, 1000);
                displayPersonalInfo();
                boolean state = listener.onButtonPressedListener(b);
                if (state) {
                    compoundButton.setChecked(true);

                } else {
                    compoundButton.setChecked(false);

                }
                compoundButton.setText(state ? "stop" : "start");

            }
        });
        radioPause.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                resumePauseNSTGraph(b);
                if (timer.isPlaying())
                    timer.stop();
                /*if (alert.isPlaying())
                    alert.stop();*/
            }
        });

        chTime.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
            @Override
            public void onChronometerTick(Chronometer chronometer) {

                long elapsedMillis = SystemClock.elapsedRealtime() - chronometer.getBase();
                timeCount++; //= (int) elapsedMillis%1000;
                /*if (timeCount+10 >= (nstTime)*60) {
                    //progressbar refresh rate 4 in one min
                    int increases = ((nstTime) >= 6 ? 3 :  1);
                    nstTime+=increases;
                    //timeProgressBar.setMax(nstTime * 4);
                    //timeProgressBar.invalidate();
                }*/

                int milliseconds = 0;
                String chronoText = chronometer.getText().toString();
                String array[] = chronoText.split(":");
                if (array.length == 2) {
                    milliseconds = Integer.parseInt(array[0]) * 60
                            + Integer.parseInt(array[1]);
                } else if (array.length == 3) {
                    milliseconds = Integer.parseInt(array[0]) * 60 * 60
                            + Integer.parseInt(array[1]) * 60
                            + Integer.parseInt(array[2]);
                }

                //Log.i("timecounter", timeCount + "----" + milliseconds);
                if (milliseconds + 5 >= (nstTime) * 60) {
                    if (!timer.isPlaying())
                        timer.play();
                }
                if (milliseconds % 15 == 0)
                    timeProgressBar.setProgress(milliseconds / 15);
            }
        });

        chTime.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (playing) {
                    //chTime.setVisibility(GONE);
                    radioPause.performClick();
                    //radioPause.setVisibility(VISIBLE);
                }
            }
        });

        timeProgressBar.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (playing) {
                    radioPause.performClick();
                }
            }
        });
        fhrTile.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                //if(mDopplerState){
                    listener.onFhrButtonPressed();
                //}
            }
        });

        movementTile.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!mMarkerState){
                    listener.onMarkerButtonPressed();
                    movementProgressBar.setVisibility(VISIBLE);
                }
            }
        });
    }


    public boolean toggelNstPlayState(boolean state) {
        if (radioPause.isChecked() != state) {
            radioPause.performClick();
            return true;
        }
        return false;
    }

    private boolean resumePauseNSTGraph(boolean state) {
        /*if(timer.isPlaying()){
            timer.stop();
            return state;
        }*/
        if (playing) {
            boolean newState = listener.onPauseButtonPressedListener(state);

            if (newState) {
                //radioPause.setChecked(true);
                radioPause.setVisibility(VISIBLE);
                radioPause.startAnimation(beatAnimation);
                //txtBpm.clearAnimation();
                //animationDrawable.stop();
                chTime.setVisibility(GONE);
            } else {
                //radioPause.setChecked(false);
                radioPause.setVisibility(GONE);
                radioPause.clearAnimation();
                //txtBpm.setAnimation(beatAnimation);
                //animationDrawable.start();
                chTime.setVisibility(VISIBLE);
            }

            return newState;
        } else {
            radioPause.setVisibility(GONE);
            radioPause.clearAnimation();
            chTime.setVisibility(VISIBLE);
            return false;
        }
    }

    private void displayPersonalInfo() {
        if (playing) {
            //llPresonalInfo.setVisibility(VISIBLE);
            //llFhr.setVisibility(GONE);

           /* graph.zoomIn();
            graph.fitScreen();
            graph.invalidate();
            graph.moveViewTo((float) (graph.getData().getEntryCount()- 7),150f, YAxis.AxisDependency.LEFT);
            graph.zoom(0,  0,  0,  0);
            graph.fitScreen();

            graph.moveViewTo((float) (graph.getData().getEntryCount()- 7),150f, YAxis.AxisDependency.LEFT);
            graph.invalidate();*/

        }
    }



    public void timerStart() {
        if (playing) {
            // on first start
            if (mLastStopTime == 0)
                chTime.setBase(SystemClock.elapsedRealtime());
                // on resume after pause
            else {
                long intervalOnPause = (SystemClock.elapsedRealtime() - mLastStopTime);
                chTime.setBase(chTime.getBase() + intervalOnPause);
                totalPauseTime += intervalOnPause;
            }

            chTime.start();
        }
    }

    public void timerPause() {
        if (playing) {
            chTime.stop();
            mLastStopTime = SystemClock.elapsedRealtime();

        }
    }

    private void shakeItBaby() {
        try {
            if (Build.VERSION.SDK_INT >= 26) {
                ((Vibrator) getContext().getSystemService(VIBRATOR_SERVICE)).vibrate(VibrationEffect.createOneShot(200, VibrationEffect.DEFAULT_AMPLITUDE));
            } else {
                ((Vibrator) getContext().getSystemService(VIBRATOR_SERVICE)).vibrate(200);
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
    /*public void setNstTestDetails(String name, int age, String mobileNo) {
        txtName.setText(name);
        txtAge.setText("" + age);
        txtMobileNo.setText(mobileNo);
        String now = new Date().toString();

        txtDate.setText(String.format("%s %s\n Time : %s", now.substring(4, 10),
                now.substring(now.lastIndexOf(" ")),now.substring(11,16)));
    }*/

    public void toggleDeviceState(boolean dopplerState, boolean markerState) {
        mDopplerState = dopplerState;
        mMarkerState = markerState;
        if (markerState) {
            //ivMove.setImageResource(R.drawable.movement);
            movementTile.setBackgroundResource(R.drawable.tiles_nst_bg);
            movementProgressBar.setVisibility(GONE);
        } else {
            //ivMove.setImageResource(R.drawable.movement_disconnected);
            movementTile.setBackgroundResource(R.drawable.tiles_nst_disconnected_bg);
            movementProgressBar.setVisibility(GONE);
        }
        if (dopplerState) {
            fhrTile.setBackgroundResource(R.drawable.tiles_nst_bg);
        } else {
            fhrTile.setBackgroundResource(R.drawable.tiles_nst_disconnected_bg);
        }

    }

    public String getImageFilename() {
        String filepath = Environment.getExternalStorageDirectory().getPath();
        File file = new File(filepath, BeatsRecorder.AUDIO_RECORDER_FOLDER);
        if (!file.exists()) {
            file.mkdirs();
        }

        SimpleDateFormat format = new SimpleDateFormat("ddMMyyyy_HHmmss", Locale.ENGLISH);
        Date now = Calendar.getInstance().getTime();
        return file.getAbsolutePath() + "/report_" + format.format(now) + ".jpg";
    }

/*    @Override
    protected void onLayout(final boolean changed, final int l, final int t, final int r, final int b) {
        super.onLayout(changed, l, t, r, b);

        final int width = getWidth();
        final int height = getHeight();
        final int offset = Math.abs(width - height) / 2;
        setTranslationX(-offset);
        setTranslationY(offset);
    }

    @Override
    protected synchronized void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(heightMeasureSpec, widthMeasureSpec);
        setMeasuredDimension(getMeasuredHeight(), getMeasuredWidth());
    }*/

    private void initialize() {
        //setRotation(90f);
        //this.threshold = (int) ((this.getMeasuredHeight() - visualizer.getViewHeight()) / 8.0);

        //initMovementsBroadcastListener();
        //resetVisualizer();
        handler = new Handler();
        setAnimation();
        graphBpmEntries = new ArrayList<>();
        graphMovmentEntries = new ArrayList<>();
        //initArray();
        /*parent.setDrawingCacheEnabled(true);
        // this is the important code :)
        // Without it the view will have a dimension of 0,0 and the bitmap will be null
        parent.measure(MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED),
                MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED));
        parent.layout(0, 0, parent.getMeasuredWidth(), parent.getMeasuredHeight());

        parent.buildDrawingCache(true);*/


        //Uri alertTone = Uri.parse("android.resource://" + getContext().getPackageName() + "/" + R.raw.red_alert);
        Uri timerTone = Uri.parse("android.resource://" + getContext().getPackageName() + "/" + R.raw.timer_tone);
        //alert = RingtoneManager.getRingtone(getContext(), alertTone);
        timer = RingtoneManager.getRingtone(getContext(), timerTone);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        this.threshold = (int) (((visualizer.getMeasuredHeight() * 2) * 2) / 100);// - visualizer.getViewHeight()) / 10.0);
    }

    public void prepareVisualizer(int audioSessionId) {
        resetVisualizer();

        //graph.resetView(System.currentTimeMillis(),nstTime);
        //visualizer.setupVisualizerFxAndUI(audioSessionId);
        //visualizer.setPlaying(true);
        //visualizer.setEnabled(true);
        //txtBpm.startAnimation(beatAnimation);
        //radioPause.startAnimation(beatAnimation);
        //startTime = System.currentTimeMillis();

        //timeHandler.post(updateTime);
        timeCount = 0;
        chTime.start();
        chTime.setBase(SystemClock.elapsedRealtime());


    }

    private int timeCount = 0;

    private void findViews() {
        txtBpm = (TextView) findViewById(R.id.txtBpm);
        txtMovements = (TextView) findViewById(R.id.txtMovements);
        visualizer = (BeatsVisualizer4) findViewById(R.id.bvisualizer);
        radioListen = (CheckBox) findViewById(R.id.radioListen);
        radioPause = (CheckBox) findViewById(R.id.radioPause);
        timeProgressBar = (ProgressBar) findViewById(R.id.pbtime);
        chTime = (Chronometer) findViewById(R.id.chTime);
        chTime.setFormat("%s");

        llStartStop = (LinearLayout) findViewById(R.id.llStartStop);
        movementTile = findViewById(R.id.movementTile);
        fhrTile = findViewById(R.id.fhrTile);
        movementProgressBar = (ProgressBar) findViewById(R.id.movementProgressBar);

        radioPause.setAnimation(beatAnimation);
    }


    public void updateView(float[] bytes) {
        //Log.i("Frame time", System.currentTimeMillis() + "");
        this.mBytes = bytes;
        double gain = this.getFrameGain(bytes);
        //Log.i("gain", gain + "");
        if (gain < 0.05D) {
            gain = 0.0D;
        }

        /*if(previousGain==0)
            previousGain=gain;
        else {
            gain = (int) (gain * alfa + (1 - alfa) * previousGain);
            previousGain = gain;
        }*/

        this.smoothedGains.add(gain);
        this.smoothedGains.add(gain);
        //this.smoothedGains.add(gain);
        this.mLength += 2; // +2

        this.smoothGains();
        this.computeDoubles();
        this.computeInts();
        count++;
        lastIndex = 0;

        if (count == (int) (frameRate)) {
            /*int size = Peaks.findPeaks(convertIntToDoubles(), 5, 10.0).size();
            //peakCount++;
            float s1 = size*60;
            addEntry(s1);*/
            getHeartRate();

            count = 0;
        }

    }


    private void getHeartRate() {

        /*int length = (int)(frameRate*3.75);
        if(mHeights.size() <length)
            return;*/
        int length;
        if ((mHeights.size() - frameRate * 2 * 7) > 0)
            length = frameRate * 2 * 7 - frameRate;
        else if ((mHeights.size() - frameRate * 2 * 6) > 0)
            length = frameRate * 2 * 6 - frameRate;
        else if ((mHeights.size() - frameRate * 2 * 5) > 0)
            length = frameRate * 2 * 5 - frameRate;
        else if ((mHeights.size() - frameRate * 2 * 4) > 0)
            length = frameRate * 2 * 4 - frameRate;
        else if ((mHeights.size() - frameRate * 2 * 3) > 0)
            length = frameRate * 2 * 3 - frameRate;
        else if ((mHeights.size() - frameRate * 2 * 2) > 0)
            length = frameRate * 2 * 2 - frameRate;
        else
            return;

        List list = Peaks.findPeaks(convertIntToDoubles(length), (frameRate / 2) - 5, threshold);
        if (list == null)
            return;

        //Log.i("size ", list.size() + "");
        //if (list.size() >= 1) {


        if (list.size() >= 6) {
            float diff = 0;
            int counter = 0;
            int listSize = list.size() > 20 ? 20 : list.size();
            //Log.i("listsize ", listSize + " ");
            for (int i = listSize; i > 1; i--) {
                int sum = (int) list.get(list.size() - (i - 1));
                sum -= (int) list.get(list.size() - i);
                //sum/=2;
                float var1 = (((diff + sum) / (count + 1)) - (diff / count));
                if (diff == 0 || var1 < 1.25) {
                    //Log.i("valid ", (var1 < 1.25) + " " + var1);
                    diff += sum;
                    counter++;
                } //else
                    //Log.i("invalid ", var1 + " ");
            }

            //Log.i("return", "  " + counter);
            if (counter <= 0)
                return;
            diff /= counter;
            //Log.i("return", diff + "  " + counter);
            if (diff < 1)
                return;
            //diff = round(diff,2);

            /*if(list.size()>=10){
                int diff = 0;
                int size = list.size();
                size--;
                int b1 = (int) list.get(list.size() - size--);
                int b2 = (int) list.get(list.size() - size--);
                int b3 = (int) list.get(list.size() - size--);
                int b4 = (int) list.get(list.size() - size--);
                int b5 = (int) list.get(list.size() - size--);
                int b6 = (int) list.get(list.size() - size--);
                int b7 = (int) list.get(list.size() - size--);
                int b8 = (int) list.get(list.size() - size--);
                int b9 = (int) list.get(list.size() - size--);
                diff += b2 - b1;
                diff += b3 - b2;
                diff += b4 - b3;
                diff += b5 - b4;
                diff += b6 - b5;
                diff += b7 - b6;
                diff += b8 - b7;
                diff += b9 - b8;


                diff/=8;
                Log.i("Big",diff+"");*/
            //((Sampling Rate * 60)/Number of samples between two consecutive S1’s)
            // no of samples = (diff / 2) ----- it is divided by coz it was multiliped by 2 before to visualize
            // samples per frame = (SAMPLE_RATE / frameRate)


            final int bps = (int) (SAMPLE_RATE * 60 / ((round(diff / 2, 2)) * (SAMPLE_RATE / frameRate)));
            /*Log.i("skipped", diff / 2 + " " + counter);
            Log.i("skipped", round(diff / 2, 2) + " " + counter);
            Log.i("skipped", bps + "");*/

            if (previous == 0) {
                fhr = bps;
                previous = fhr;
            } else
                fhr = (int) (bps * alfa + (1 - alfa) * previous);
            previous = fhr;
            //previous += fhr;
            //previous/=2;

            /*if ((previous > 160 || previous < 120) && !alert.isPlaying()) {
                Log.i("alert", "over threshhold");
                try {
                    //alert.play();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {

                try {
                    if (alert.isPlaying())
                        alert.stop();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }*/

            //graph.addEntry(previous);
            if (firstBPMEntryTime == 0)
                firstBPMEntryTime = TimeUnit.MILLISECONDS.toMillis(SystemClock.elapsedRealtime());
            graphBpmEntries.add(previous);
            graphFHREnteryCount++;
            //Log.i("entry", previous + "");


            handler.post(new Runnable() {

                @Override
                public void run() {
                    //txtBpm.setAlpha(0.2f);
                    //graph.invalidate();
                    txtBpm.setText(Html.fromHtml("<b>" + fhr + "</b>"));//<br/>FHR"));

                }
            });
            /*}else if (list.size() >= 5) {
                int size = list.size();
                int b1 = (int) list.get(list.size() - size--);
                int b2 = (int) list.get(list.size() - size--);
                int b3 = (int) list.get(list.size() - size--);
                int b4 = (int) list.get(list.size() - size--);
                int diff1 = b2 - b1;
                int diff2 = b3 - b2;
                int diff3 = b4 - b3;
                int diff = (diff1 + diff2 + diff3) / 3;
                final int bps = SAMPLE_RATE * 60 / ((diff / 2) * (SAMPLE_RATE / frameRate));
                Log.i("bps", bps + "");

                graph.addEntry(bps);
                handler.post(new Runnable() {

                    @Override
                    public void run() {
                        //txtBpm.setAlpha(0.2f);
                        txtBpm.setText(Html.fromHtml("<b><h1>" + bps + "</h1></b><br/>FHR"));

                    }
                });
                //((TextView)findViewById(R.id.txtBpm)).setText(bps+"\nbpm" );

            }
            */
        }else {
            handler.post(new Runnable() {

                @Override
                public void run() {
                    //txtBpm.setAlpha(0.2f);
                    //graph.invalidate();
                    txtBpm.setText(Html.fromHtml("<b>" + 0 + "</b>"));//<br/>FHR"));

                }
            });
        }
    }

    private float getValidCompareValue() {
        if (fhr <= 80)
            return 1.5f;
        else if (fhr <= 100)
            return 1.2f;
        else return 1.0f;
    }

    public float round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.floatValue();
    }

    private void setAnimation() {

        beatAnimation = new AlphaAnimation(1f, 0.1f); // Change alpha from fully visible to invisible
        beatAnimation.setDuration(300); // duration
        beatAnimation.setStartOffset(1000);
        beatAnimation.setInterpolator(new LinearInterpolator()); // do not alter animation rate
        beatAnimation.setRepeatCount(Animation.INFINITE); // Repeat animation infinitely
        beatAnimation.setRepeatMode(Animation.REVERSE);
    }

    private double[] convertIntToDoubles(int length) {

        if (mHeights == null && length < 0) {
            return null; // Or throw an exception - your choice
        }
        int startIndex = (mHeights.size() - (length + frameRate));
        double[] output = new double[length];
        for (int i = 0; i < length; i++) {
            output[i] = mHeights.get(startIndex + i);
        }
        return output;
    }

    public double getFrameGain(float[] bytes) {
        float maxGain = 0.0F;
        int length = bytes.length;

        for (int j = 1; j < length; j += 4) {
            float val = Math.abs(bytes[j]);
            if (val > maxGain) {
                maxGain = val;
            }
        }

        //Log.i("Test", "" + maxGain);
        return (double) maxGain;
    }

    private void computeDoubles() {
        double value = (smoothedGains.get(mLength - 1));// range;
        double value2 = (smoothedGains.get(mLength - 2));// range;
        //double value3 = (smoothedGains.get(mLength - 3));// range;

        //Log.i("Test double", value + "");

        /*if (value < 0.0D) {
            value = 0.0D;
        }

        if (value > 1.0D) {
            value = 1.0D;
        }*/

       /* if (value2 < 0.0D) {
            value2 = 0.0D;
        }

        if (value2 > 1.0D) {
            value2 = 1.0D;
        }*/

        mValues.add(value2 * value2);
        mValues.add(value * value);
        //mValues.add(value * value);
    }


    public void resetVisualizer() {
        playing = true;
        mHeights = new ArrayList<>();
        mValues = new ArrayList<>();
        smoothedGains = new ArrayList<>();
        mLength = 0;
        count = 0;
        timeCount = 0;
        graphFHREnteryCount = 0;
        firstBPMEntryTime = 0;
        lastMovementEntryTime = 0;
        graphBpmEntries.clear();
        graphMovmentEntries.clear();
        //visualizer.updateVisualizer(mHeights);
        //visualizer.adjustHeight(getMeasuredHeight() / 2);
        txtBpm.setText(Html.fromHtml("<b>0</b>"));
        initMovementsBroadcastListener();
        movementCount = 0;
        txtMovements.setText(movementCount + "");
        bm = LocalBroadcastManager.getInstance(getContext());
        bm.registerReceiver(movementsReciver,
                new IntentFilter(NST_MOVEMENTS));

        timeProgressBar.setProgress(0);
        mLastStopTime = 0;
        totalPauseTime = 0;
        //tvTime.setText("00:00");
        nstTime = NSTTIME / 4;
        timeProgressBar.setMax(NSTTIME);

    }

    private void smoothGains() {
        if (mLength - 10 > 0) {
            for (int i = mLength - 9; i < mLength - 1; i++) {
                smoothedGains.set(i, (double) (
                        (smoothedGains.get(i - 1) / 3.0) +
                                (smoothedGains.get(i) / 3.0) +
                                (smoothedGains.get(i + 1) / 3.0)));
            }
            smoothedGains.set(mLength - 1, (double) (
                    (smoothedGains.get(mLength - 2) / 2.0) +
                            (smoothedGains.get(mLength - 1) / 2.0)));
        } else if (mLength > 2) {
            smoothedGains.set(0, (double) (
                    (smoothedGains.get(0) / 2.0) +
                            (smoothedGains.get(1) / 2.0)));
            for (int i = 1; i < mLength - 1; i++) {
                smoothedGains.set(i, (double) (
                        (smoothedGains.get(i - 1) / 3.0) +
                                (smoothedGains.get(i) / 3.0) +
                                (smoothedGains.get(i + 1) / 3.0)));
            }
            smoothedGains.set(mLength - 1, (double) (
                    (smoothedGains.get(mLength - 2) / 2.0) +
                            (smoothedGains.get(mLength - 1) / 2.0)));
        }

    }

    private void smoothHeights() {
        if (mLength - 10 > 0) {
            for (int i = mLength - 9; i < mLength - 1; i++) {
                mHeights.set(i, (
                        (mHeights.get(i - 1) / 3) +
                                (mHeights.get(i) / 3) +
                                (mHeights.get(i + 1) / 3)));

            }
            mHeights.set(mLength - 1, (
                    (mHeights.get(mLength - 2) / 2) +
                            (mHeights.get(mLength - 1) / 2)));
        } else if (mLength > 2) {
            mHeights.set(0, (int) (
                    (mHeights.get(0) / 2.0) +
                            (mHeights.get(1) / 2.0)));
            for (int i = 1; i < mLength - 1; i++) {
                mHeights.set(i, (
                        (mHeights.get(i - 1) / 3) +
                                (mHeights.get(i) / 3) +
                                (mHeights.get(i + 1) / 3)));
            }
            mHeights.set(mLength - 1, (
                    (mHeights.get(mLength - 2) / 2) +
                            (mHeights.get(mLength - 1) / 2)));
        }

    }

    private void computeInts() {
        int halfHeight = this.getMeasuredHeight() / 2;
        //double height1 = (mValues.get(mLength - 2) * halfHeight)*2;
        //double height2 = (mValues.get(mLength - 1) * halfHeight)*2;
        /*for (int i = 3; i >0; i--) {
            mHeights.add((int) (mValues.get(mLength - i) * halfHeight) * 2);
        }*/
        for (int i = 2; i > 0; i--) {
            /*if (((mValues.get(mLength - i) * halfHeight) * 8) <= halfHeight)
                mHeights.add((int) (mValues.get(mLength - i) * halfHeight) * 8);
            else if (((mValues.get(mLength - i) * halfHeight) * 6) <= halfHeight)
                mHeights.add((int) (mValues.get(mLength - i) * halfHeight) * 6);
            else if (((mValues.get(mLength - i) * halfHeight) * 4) <= halfHeight)
                mHeights.add((int) (mValues.get(mLength - i) * halfHeight) * 4);
            else if (((mValues.get(mLength - i) * halfHeight) * 2) <= halfHeight)
                mHeights.add((int) (mValues.get(mLength - i) * halfHeight)*2);
            else
                mHeights.add((int) (mValues.get(mLength - i) * halfHeight));*/

            mHeights.add((int) (mValues.get(mLength - i) * halfHeight) * 2);
            Log.i("Testheight", mHeights.get(mLength - i) + "," + halfHeight);
        }
        /*if(previousHeight==0)
            previousHeight=height1;
        else {
            height1 = (int) (height1 * alfa + (1 - alfa) * previousHeight);
            previousHeight = height1;
        }

        height2 = (int) (height2 * alfa + (1 - alfa) * previousHeight);
        previousHeight = height2;
*/

        //mHeights.add((int) (height1));
        //mHeights.add((int) (height2));
        smoothHeights();
    }

    public void stopNstView() {
        playing = false;
        //togglePersonalInfoDisplay();
        //BeatsRecorder.appendLog(graphBpmEntries);
        //visualizer.setPlaying(Boolean.valueOf(false));
        //txtBpm.clearAnimation();
        bm.unregisterReceiver(movementsReciver);
        //timeHandler.removeCallbacks(updateTime);
        timeCount = 0;
        chTime.stop();
        playing = false;
        /*if (alert.isPlaying())
            alert.stop();*/
        if (timer.isPlaying())
            timer.stop();
        handler.post(new Runnable() {

            @Override
            public void run() {
                //txtBpm.setAlpha(0.2f);
                //graph.invalidate();
                txtBpm.setText(Html.fromHtml("<b>" + 0 + "</b>"));//<br/>FHR"));

            }
        });

        //getBitmapFromView();
        //listener.onPauseButtonPressedListener(false);
        radioPause.setChecked(false);

    }

    public int getFrameRate() {
        return frameRate;
    }

    public void setFrameRate(int frameRate) {
        this.frameRate = frameRate;
        //Log.i("Frame Rate", frameRate + "");
    }

    public BeatsVisualizer4 getVisualizer() {
        return visualizer;
    }

    public interface NSTViewTouchListener {
        boolean onButtonPressedListener(boolean state);

        boolean onPauseButtonPressedListener(boolean state);

        boolean onFhrButtonPressed();
        boolean onMarkerButtonPressed();
    }

    private void initMovementsBroadcastListener() {
        movementsReciver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                int keyDown = intent.getIntExtra("key", -1);
                Log.i("marker", "nst view broadcast: " + keyDown);
                //movementCount++;
                //txtMovements.setText(movementCount+"");

                if (KeyEvent.KEYCODE_MEDIA_NEXT == keyDown) {
                    //Log.d("Marker", "Keycode state: " + keyDown);

                    long now = TimeUnit.MILLISECONDS.toMillis(SystemClock.elapsedRealtime());

                    //int move = (int) (now - firstBPMEntryTime) / 1000;

                    if (lastMovementEntryTime != 0 && (graphBpmEntries.size() -graphMovmentEntries.get(movementCount-1)) < 5) {
                        Toast.makeText(context, "Marker has already been drawn", Toast.LENGTH_SHORT).show();
                        Log.d("marker", "Not Drawn " + now);
                        return;
                    }
                    lastMovementEntryTime = now;
                    movementCount += 1;// add;
                    txtMovements.setText(movementCount + "");
                    graphMovmentEntries.add(graphBpmEntries.size());
                    //Log.d("marker", move + " Drawn " + now);
                    /*if (graphFHREnteryCount > 0) {
                        if (graph != null) {

                            float add = graph.addMovementsEntry();


                            if(add!=0) {
                                lastMovementEntryTime = now;
                                movementCount +=1;// add;
                                Toast.makeText(context, "Movement", Toast.LENGTH_SHORT).show();
                                txtMovements.setText(movementCount + "");
                                graphMovmentEntries.add((int) add);
                            }else{
                                Toast.makeText(context, "FHR input has stoped", Toast.LENGTH_SHORT).show();
                            }
                        }
                    } else {

                        Toast.makeText(context, "Please wait for the NST to start", Toast.LENGTH_SHORT).show();
                    }*/
                }

            }
        };

        Log.d("D", "starting bluetooth");
    }


}
