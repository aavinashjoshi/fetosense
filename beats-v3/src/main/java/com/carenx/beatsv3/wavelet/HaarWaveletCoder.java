package com.carenx.beatsv3.wavelet;

/**
 * Created by avinash on 29/9/16.
 */


import com.carenx.beatsv3.dsp.AudioEvent;
import com.carenx.beatsv3.dsp.AudioProcessor;

import java.util.Arrays;

public class HaarWaveletCoder implements AudioProcessor {
    private final HaarWaveletTransform transform;
    private int compression;

    public HaarWaveletCoder() {
        this(8);
    }

    public HaarWaveletCoder(int var1) {
        this.transform = new HaarWaveletTransform();
        this.compression = var1;
    }

    public boolean process(AudioEvent var1) {
        float[] var2 = var1.getFloatBuffer();
        float[] var3 = new float[var2.length];
        this.transform.transform(var1.getFloatBuffer());

        for(int var4 = 0; var4 < var3.length; ++var4) {
            var3[var4] = Math.abs(var2[var4]);
        }

         Arrays.sort(var3);
        double var7 = (double)var3[this.compression];

        for(int var6 = 0; var6 < var2.length; ++var6) {
            if((double)Math.abs(var2[var6]) <= var7) {
                var2[var6] = 0.0F;
            }
        }

        return true;
    }

    public void processingFinished() {

    }

    public void setCompression(int var1) {
        this.compression = var1;
    }

    public int getCompression() {
        return this.compression;
    }
}
