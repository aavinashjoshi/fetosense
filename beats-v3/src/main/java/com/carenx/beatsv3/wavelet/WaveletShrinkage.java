
/*******************************************************************************
 * Copyright (c) 2010 Haifeng Li
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/

package com.carenx.beatsv3.wavelet;

import android.util.Log;

import com.carenx.beatsv3.dsp.AudioEvent;
import com.carenx.beatsv3.dsp.AudioProcessor;

/**
 * The wavelet shrinkage is a signal denoising technique based on the idea of
 * thresholding the wavelet coefficients. Wavelet coefficients having small
 * absolute value are considered to encode mostly noise and very fine details
 * of the signal. In contrast, the important information is encoded by the
 * coefficients having large absolute value. Removing the small absolute value
 * coefficients and then reconstructing the signal should produce signal with
 * lesser amount of noise. The wavelet shrinkage approach can be summarized as
 * follows:
 * <ol>
 * <li> Apply the wavelet transform to the signal.
 * <li> Estimate a threshold value.
 * <li> The so-called hard thresholding method zeros the coefficients that are
 * smaller than the threshold and leaves the other ones unchanged. In contrast,
 * the soft thresholding scales the remaining coefficients in order to form a
 * continuous distribution of the coefficients centered on zero.
 * <li> Reconstruct the signal (apply the inverse wavelet transform).
 * </ol>
 * The biggest challenge in the wavelet shrinkage approach is finding an
 * appropriate threshold value. In this class, we use the universal threshold
 * T = &sigma; sqrt(2*log(N)), where N is the length of time series
 * and &sigma; is the estimate of standard deviation of the noise by the
 * so-called scaled median absolute deviation (MAD) computed from the high-pass
 * wavelet coefficients of the first level of the transform.
 *
 * @author Haifeng Li
 */
public class WaveletShrinkage implements AudioProcessor{
    /**
     * Adaptive hard-thresholding denoising a time series with given wavelet.
     *
     * @param t the time series array. The size should be a power of 2. For time
     * series of size no power of 2, 0 padding can be applied.
     * @param wavelet the wavelet to transform the time series.
     */
    public static void denoise(float[] t, Wavelet wavelet) {
        denoise(t, wavelet, false);
    }

    /**
     * Adaptive denoising a time series with given wavelet.
     *
     * @param t the time series array. The size should be a power of 2. For time
     * series of size no power of 2, 0 padding can be applied.
     * @param wavelet the wavelet to transform the time series.
     * @param soft true if apply soft thresholding.
     */
    public static void denoise(float[] t, Wavelet wavelet, boolean soft) {
        wavelet.transform(t);

        int n = t.length;
        int nh = t.length >> 1;

        double[] wc = new double[nh];
        System.arraycopy(t, nh, wc, 0, nh);
        double error = mad(wc) / 0.6745;

        double lambda = error * Math.sqrt(2 * Math.log(n));

        if (soft) {
            for (int i = 4; i < n; i++) {
                t[i] = (float)(Math.signum(t[i]) * Math.max(Math.abs(t[i]) - lambda, 0.0));
            }
        } else {
            for (int i = 4; i < n; i++) {
                if (Math.abs(t[i]) < lambda) {
                    t[i] = 0.0f;
                }
            }
        }

        wavelet.inverse(t);
    }



    public static double mad(double[] x) {
        double m = median(x);
        for (int i = 0; i < x.length; i++) {
            x[i] = Math.abs(x[i] - m);
        }

        return median(x);
    }

    /**
     * Find the median of an array of type float.
     */
    public static double median(double[] a) {
        int k = a.length / 2;
        return select(a, k);
    }
    /**
     * Given k in [0, n-1], returns an array value from arr such that k array
     * values are less than or equal to the one returned. The input array will
     * be rearranged to have this value in location arr[k], with all smaller
     * elements moved to arr[0, k-1] (in arbitrary order) and all larger elements
     * in arr[k+1, n-1] (also in arbitrary order).
     */
    public static double select(double[] arr, int k) {
        int n = arr.length;
        int l = 0;
        int ir = n - 1;

        double a;
        int i, j, mid;
        for (;;) {
            if (ir <= l + 1) {
                if (ir == l + 1 && arr[ir] < arr[l]) {
                    swap(arr, l, ir);
                }
                return arr[k];
            } else {
                mid = (l + ir) >> 1;
                swap(arr, mid, l + 1);
                if (arr[l] > arr[ir]) {
                    swap(arr, l, ir);
                }
                if (arr[l + 1] > arr[ir]) {
                    swap(arr, l + 1, ir);
                }
                if (arr[l] > arr[l + 1]) {
                    swap(arr, l, l + 1);
                }
                i = l + 1;
                j = ir;
                a = arr[l + 1];
                for (;;) {
                    do {
                        i++;
                    } while (arr[i] < a);
                    do {
                        j--;
                    } while (arr[j] > a);
                    if (j < i) {
                        break;
                    }
                    swap(arr, i, j);
                }
                arr[l + 1] = arr[j];
                arr[j] = a;
                if (j >= k) {
                    ir = j - 1;
                }
                if (j <= k) {
                    l = i;
                }
            }
        }
    }

    /**
     * Swap two positions.
     */
    public static void swap(double arr[], int i, int j) {
        double a;
        a = arr[i];
        arr[i] = arr[j];
        arr[j] = a;
    }

    @Override
    public boolean process(AudioEvent var1) {
        Log.i("length",var1.getFloatBuffer().length+"");
        denoise(var1.getFloatBuffer(),new DaubechiesWavelet(8));
        return true;
    }

    @Override
    public void processingFinished() {

    }
}
