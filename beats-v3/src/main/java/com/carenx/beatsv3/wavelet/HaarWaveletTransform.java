package com.carenx.beatsv3.wavelet;

/**
 * Created by avinash on 29/9/16.
 */

public class HaarWaveletTransform {
    public HaarWaveletTransform() {
    }

    public void transform(float[] var1) {
        int var2 = var1.length;

        assert isPowerOfTwo(var2);

        int var3 = this.log2(var2);
        int var4 = 2;
        int var5 = 1;

        for(int var6 = 0; var6 < var3; ++var6) {
            var2 /= 2;

            for(int var7 = 0; var7 < var2; ++var7) {
                float var8 = (var1[var4 * var7] + var1[var4 * var7 + var5]) / 2.0F;
                float var9 = (var1[var4 * var7] - var1[var4 * var7 + var5]) / 2.0F;
                var1[var4 * var7] = var8;
                var1[var4 * var7 + var5] = var9;
            }

            var5 = var4;
            var4 *= 2;
        }

    }

    public void inverseTransform(float[] var1) {
        int var2 = var1.length;

        assert isPowerOfTwo(var2);

        int var3 = this.log2(var2);
        int var4 = this.pow2(var3 - 1);
        int var5 = 2 * var4;
        var2 = 1;

        for(int var6 = var3; var6 >= 1; --var6) {
            for(int var7 = 0; var7 < var2; ++var7) {
                float var8 = var1[var5 * var7] + var1[var5 * var7 + var4];
                float var9 = var1[var5 * var7] - var1[var5 * var7 + var4];
                var1[var5 * var7] = var8;
                var1[var5 * var7 + var4] = var9;
            }

            var5 = var4;
            var4 /= 2;
            var2 = 2 * var2;
        }

    }

    private static boolean isPowerOfTwo(int var0) {
        if(var0 <= 0) {
            throw new IllegalArgumentException("number: " + var0);
        } else {
            return (var0 & -var0) == var0;
        }
    }

    private int log2(int var1) {
        return var1 == 0?0:31 - Integer.numberOfLeadingZeros(var1);
    }

    private int pow2(int var1) {
        return 1 << var1;
    }
}

