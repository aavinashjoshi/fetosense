package com.carenx.beatsv3;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothHeadset;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.provider.MediaStore.Images;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.Toast;

import com.carenx.beatsv3.dsp.AudioEvent;
import com.carenx.beatsv3.utils.BeatsVisualizer2;
import com.carenx.beatsv3.utils.BluetoothDeviceConnectionReciver;
import com.carenx.beatsv3.utils.WavFileWriter;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by avinash on 11/6/16.
 * recorder
 */

public class BeatsRecorder implements BeatsProcessor.OnAudioEventListener,
        NstView.NSTViewTouchListener, BluetoothDeviceConnectionReciver.OnBluetoothDeviceStateChangeLisener {
    public static final int RECORDING_MODE_DIAPHRAM = 0;
    public static final int RECORDING_MODE_BELL = 1;
    public static final int RECORDING_MODE_DOPLER = 2;
    public static final int SAMPLE_RATE = 8000;
    private static boolean isExternalMicPluggedIn;
    private static boolean isBluetoothDopplerConnected = false;
    private static boolean bluetoothMarkerConected = false;
    BeatsRecorder.RecordingEventListener mListener;
    private BeatsProcessor mBeatsProcessor;
    private static Context mContext;
    //private AudioTrack mAudioTrack;
    private BeatsVisualizer2 mBeatsVisualizer2;
    private NstView nstView;
    public static String AUDIO_RECORDER_FOLDER = "CareNx";
    private WavFileWriter fWriter;
    private String mFileName;
    private BeatsRecorder mBeatsRecorder;
    BluetoothDeviceConnectionReciver.OnBluetoothDeviceStateChangeLisener blueListener = this;
    private BluetoothDeviceConnectionReciver bleReciver;
    private AudioManager audioManager;
    private static GraphExtraData data;

    //private MediaSessionCompat mediaSession;


    public BeatsRecorder(Context context, BeatsRecorder.RecordingEventListener rListener, boolean bleReceiver, GraphExtraData data) {
        this.mContext = context;
        this.mListener = rListener;
        isBluetoothDopplerConnected = isBluetoothHeadsetConnected();
        bluetoothMarkerConected = isBluetoothMarkerConnected();

        this.data = data;

        this.init(context);
        //if(bleReceiver)
        initBleReceiver(context);
    }
    public static boolean getIsBluetoothDopplerConnected() {
        return isBluetoothDopplerConnected;
    }

    public static boolean getIsBluetoothMarkerConnected() {
        return bluetoothMarkerConected;
    }

    public void setBleReceiver() {
        bleReciver.setOnBluetoothStateChangeListener(blueListener);
    }


    public String getFileName() {
        return this.mFileName;
    }


    AudioManager.OnAudioFocusChangeListener amf = new AudioManager.OnAudioFocusChangeListener() {
        @Override
        public void onAudioFocusChange(int i) {
            if (i <= 0) {
                Log.i("music", "true");
            } else {
                Log.i("music", "false");
            }

        }
    };

    private void init(Context context) {

        mBeatsProcessor = new BeatsProcessor(context, SAMPLE_RATE);
        mBeatsProcessor.setOnAudioEventListener(this);

        audioManager = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
        int result = audioManager.requestAudioFocus(amf, AudioManager.STREAM_MUSIC,
                AudioManager.AUDIOFOCUS_GAIN);


        /*mAudioTrack = new AudioTrack(AudioManager.ROUTE_SPEAKER,//AudioManager.ROUTE_SPEAKER,
                SAMPLE_RATE,
                DSPAudioFormat.CHANNEL_OUT_MONO,
                DSPAudioFormat.ENCODING_PCM_16BIT,
                mBeatsProcessor.getBufferSize(),
                //mBeatsProcessor.getBlockSize(),
                AudioTrack.MODE_STREAM
        );

        mAudioTrack.setPlaybackRate(SAMPLE_RATE);*/


      /*  ComponentName nameReceiver = new ComponentName(mContext.getPackageName(), RemoteReceiver.class.getName());
        mediaSession = new MediaSessionCompat(mContext, "PlayerService", nameReceiver, null);
        mediaSession.setFlags(MediaSessionCompat.FLAG_HANDLES_MEDIA_BUTTONS |
                MediaSessionCompat.FLAG_HANDLES_TRANSPORT_CONTROLS);
        mediaSession.setPlaybackState(new PlaybackStateCompat.Builder()
                .setState(PlaybackStateCompat.STATE_PAUSED, 0, 0)
                .setActions(PlaybackStateCompat.ACTION_PLAY_PAUSE)
                .build());*/
        /*mediaSession.setMetadata(new MediaMetadataCompat.Builder()
                .putString(MediaMetadataCompat.METADATA_KEY_ARTIST, "Test Artist")
                .putString(MediaMetadataCompat.METADATA_KEY_ALBUM, "Test Album")
                .putString(MediaMetadataCompat.METADATA_KEY_TITLE, "Test Track Name")
                .putLong(MediaMetadataCompat.METADATA_KEY_DURATION, 10000)
                .putBitmap(MediaMetadataCompat.METADATA_KEY_ALBUM_ART,
                        BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                .build());*/

        /*AudioManager audioManager = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
        audioManager.requestAudioFocus(amf, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);*/
        //mediaSession.setActive(true);

    }

    private void initBleReceiver(Context context) {

        bleReciver = new BluetoothDeviceConnectionReciver();
        bleReciver.setOnBluetoothStateChangeListener(blueListener);
        IntentFilter markerFilter = new IntentFilter();
        IntentFilter doplerFilter = new IntentFilter();
        doplerFilter.addAction(BluetoothDeviceConnectionReciver.ACTION_ON_DOPPLER_CONNECTED);
        doplerFilter.addAction(BluetoothDeviceConnectionReciver.ACTION_ON_DOPPLER_DISCONNECTED);
        doplerFilter.addAction(BluetoothDeviceConnectionReciver.CUSTOM_ACTION_ON_CONNECTED);
        doplerFilter.addAction(BluetoothDeviceConnectionReciver.CUSTOM_ACTION_ON_DISCONNECTED);
        doplerFilter.addAction(BluetoothDeviceConnectionReciver.CUSTOM_ACTION_ON_SINGLE_CLICK);
        context.registerReceiver(bleReciver, doplerFilter);
        //context.registerReceiver(bleReciver, markerFilter);

    }

    public void setVisualizer(BeatsVisualizer2 visualizer) {
        this.mBeatsVisualizer2 = visualizer;
        //this.mBeatsVisualizer2.setListener(listener);

    }

    public void setVisualizer(NstView visualizer) {
        this.nstView = visualizer;
        nstView.setTouchListener(this);
        int frameRate = SAMPLE_RATE * 2 / mBeatsProcessor.getBufferSize();
        nstView.setFrameRate(frameRate);
        nstView.toggleDeviceState(isBluetoothDopplerConnected, bluetoothMarkerConected);
        if(isBluetoothDopplerConnected)
            startListening();
        //this.mBeatsVisualizer2.setListener(listener);
        Log.i("FrameRate", frameRate + "");
        Log.i("FrameBuffer", mBeatsProcessor.getBufferSize() + "");
        mBeatsProcessor.setVisualizer(nstView.getVisualizer());

    }

    private void startListening() {
        if(!mBeatsProcessor.isListening())
            mBeatsProcessor.startListening(BeatsProcessor.BLUETOOTH_HEADSET);
    }

    public void setTestDetails(String name, int age, String mobileNo) {
        //nstView.setNstTestDetails(name, age, mobileNo);
    }

    public void setRecordingMode(int mode) {
        this.mBeatsProcessor.setRecordingMode(mode);
    }

    public boolean isRecording() {
        return this.mBeatsProcessor.isRecording();
    }

    public boolean isPause() {
        return this.mBeatsProcessor.isPaused();
    }
    final Handler startHandler = new Handler();
    private void startBack(){
        mBeatsProcessor.startListening(/*BeatsProcessor.isUseBluetooth() &&*/ isBluetoothDopplerConnected ? BeatsProcessor.BLUETOOTH_HEADSET : BeatsProcessor.WIRED_HEADSET);
        startHandler.postDelayed(StartListen, 1000);
    }
    Runnable StartListen = new Runnable() {
        @Override
        public void run() {
            startRecording();
        }
    };
    public boolean startRecording() {
        //if(isExternalMicPluggedIn||true) {

        if (BeatsProcessor.isUseBluetooth() && !isBluetoothDopplerConnected) {
            Toast.makeText(this.mContext, "Fetal Doppler is not connected try demo mode", Toast.LENGTH_SHORT).show();
            return false;
            //Toast.makeText(this.mContext, "Fetal Doppler is not connected thus default mic will be used", Toast.LENGTH_SHORT).show();
        }


        //if (true || isBluetoothDopplerConnected) {
            if (!mBeatsProcessor.isListening()) {
                startBack();
                return true;
            }

            mBeatsProcessor.startRecording();

            if (mBeatsVisualizer2 != null) {
                mBeatsVisualizer2.resetVisualizer();
                mBeatsVisualizer2.setupVisualizerFxAndUI(mBeatsProcessor.getAudioSessionId());
                mBeatsVisualizer2.setEnabled(true);
                mBeatsVisualizer2.setPlaying(true);
            }

            if (nstView != null) {
                nstView.prepareVisualizer(mBeatsProcessor.getAudioSessionId());

            }

            //this.mAudioTrack.play();
            Log.i("isMusicActive", audioManager.isMusicActive() + "");
            //mediaSession.setActive(true);
            return true;
        //} else {
            //Toast.makeText(this.mContext, "Please connect the device properly and try again", Toast.LENGTH_SHORT).show();
            //return false;
        //}
    }

    public boolean isListening(){
        return mBeatsProcessor.isListening();
    }
    public void stopListening() {
        if (mBeatsProcessor.isListening())
            mBeatsProcessor.stopListening();
    }

    public void stopRecording() {
        if (this.mBeatsProcessor.isRecording()) {
            this.mBeatsProcessor.stopRecording();
            //this.mAudioTrack.stop();
            this.mBeatsProcessor.resume();
            if (this.mBeatsVisualizer2 != null) {
                this.mBeatsVisualizer2.setEnabled(false);
                this.mBeatsVisualizer2.setPlaying(Boolean.valueOf(false));
            }
            if (nstView != null) {
                nstView.stopNstView();
            }
        }

    }

    public boolean toggelRecordingState() {
        resumeNSTView();
        if (isRecording()) {
            if (isPause()) {
                mBeatsProcessor.resume();
                nstView.timerStart();
                return false;
            } else {
                mBeatsProcessor.pause();
                nstView.timerPause();
                return true;
            }
        } else
            return false;

    }

    public void changeRecordingNstGraphState(boolean state) {
        boolean newState = nstView.toggelNstPlayState(state);
        Log.i("PauseState", "" + newState);
    }

    public void resumeNSTView() {
        if (nstView != null) {
            nstView.toggleDeviceState(isBluetoothDopplerConnected, bluetoothMarkerConected);
        }
    }


    public void processStart() {

        mContext.sendBroadcast(new Intent("com.carenx.fetoscope.nst.start"));

        int sampleRate = (int) this.mBeatsProcessor.getAudioForrmat().getSampleRate();
        int sampleSizeInBits = this.mBeatsProcessor.getAudioForrmat().getSampleSizeInBits();
        this.mFileName = this.getFilename();
        this.fWriter = new WavFileWriter(this.mFileName, sampleRate, sampleSizeInBits);
        mListener.onRecordingStartListener();
    }

    public void processAudioProcEvent(AudioEvent ae) {
        byte[] filteredBuffer = ae.getByteBuffer();
        ae.getFloatBuffer();
        //this.mAudioTrack.write(filteredBuffer, 0, filteredBuffer.length);
        Log.i("isMusicActive", audioManager.isMusicActive() + "");
        this.fWriter.writeToFile(filteredBuffer);
        if (mBeatsVisualizer2 != null)
            this.mBeatsVisualizer2.updateVisualizer(ae.getFloatBuffer());
        if (nstView != null) {
            nstView.updateView(ae.getFloatBuffer());
        }
    }

    public static String getImageFilename() {
        String filepath = Environment.getExternalStorageDirectory().getPath();
        File file = new File(filepath, BeatsRecorder.AUDIO_RECORDER_FOLDER);
        if (!file.exists()) {
            file.mkdirs();
        }

        SimpleDateFormat format = new SimpleDateFormat("ddMMyyyy_HHmmss_SSS", Locale.ENGLISH);
        Date now = Calendar.getInstance().getTime();
        return file.getAbsolutePath() + "/test_" + format.format(now) + ".jpg";
    }


    public void setStorageFolderName(String folder) {
        this.AUDIO_RECORDER_FOLDER = folder;
    }

    public String getFilename() {
        String filepath = Environment.getExternalStorageDirectory().getPath();
        File file = new File(filepath, this.AUDIO_RECORDER_FOLDER);
        if (!file.exists()) {
            file.mkdirs();
        }

        SimpleDateFormat format = new SimpleDateFormat("ddMMyyyy_HHmmss");
        Date now = Calendar.getInstance().getTime();
        return file.getAbsolutePath() + "/beats_" + format.format(now) + ".wav";
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        mContext.unregisterReceiver(bleReciver);
        mBeatsProcessor.finalize();
        mBeatsProcessor = null;

    }

    @Override
    public boolean onButtonPressedListener(boolean state) {
        if (state) {
            return startRecording();
        } else {
            stopRecording();
            return false;
        }
    }

    @Override
    public boolean onPauseButtonPressedListener(boolean state) {
        if (isRecording())
            return toggelRecordingState();
        else

            return false;
    }

    @Override
    public boolean onFhrButtonPressed() {

        if (!mBeatsProcessor.isListening() && isBluetoothDopplerConnected && BeatsProcessor.isUseBluetooth()) {
            mBeatsProcessor.startListening(/*BeatsProcessor.isUseBluetooth() &&*/ isBluetoothDopplerConnected ? BeatsProcessor.BLUETOOTH_HEADSET : BeatsProcessor.WIRED_HEADSET);

        } else if ((!mBeatsProcessor.isListening() && !isBluetoothDopplerConnected && BeatsProcessor.isUseBluetooth())) {
            mListener.onFhrButtonPressed();
        } else if (!mBeatsProcessor.isListening() && !BeatsProcessor.isUseBluetooth()) {
            Toast.makeText(this.mContext, "Try turning off demo mode", Toast.LENGTH_SHORT).show();
        } else if(mBeatsProcessor.isListening() && !mBeatsProcessor.isRecording()){
            mListener.onFhrButtonPressed();
        }
        return mBeatsProcessor.isListening();
    }

    @Override
    public boolean onMarkerButtonPressed() {
        if(!mBeatsProcessor.isRecording()) {
            if(mBeatsProcessor.isListening())
                mBeatsProcessor.stopListening();
            mListener.onMarkerButtonPressed();
        }
        return false;
    }


    public boolean isBluetoothHeadsetConnected() {
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        boolean connected = mBluetoothAdapter != null && mBluetoothAdapter.isEnabled()
                && mBluetoothAdapter.getProfileConnectionState(BluetoothHeadset.HEADSET) == BluetoothHeadset.STATE_CONNECTED;
        Log.i("bluetoothConnected", connected + "");
        return connected;
    }

    /*public boolean isBluetoothMarkerConnected() {
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        boolean connected = mBluetoothAdapter != null && mBluetoothAdapter.isEnabled()
                && mBluetoothAdapter.getProfileConnectionState(BluetoothGattServer.GATT_SERVER) == BluetoothGattServer.STATE_CONNECTED;
        Log.i("bluetoothGattConnected",connected+" "+mBluetoothAdapter.getProfileConnectionState(BluetoothGatt.GATT));
        return  connected;
    }*/

    private boolean isBluetoothMarkerConnected() {
        BluetoothManager bluetoothManager = (BluetoothManager) mContext.getSystemService(Context.BLUETOOTH_SERVICE);
        List<BluetoothDevice> devices = bluetoothManager.getConnectedDevices(BluetoothProfile.GATT);
        for (BluetoothDevice device : devices) {
            Log.i("bluetoothGattConnected", " " + device.getType());
            if (device.getType() == BluetoothDevice.DEVICE_TYPE_LE) {
                Log.i("bluetoothGattConnected", "connected");
            }
        }
        return devices.size() > 0;
    }

    private boolean removeAllConnectedBluetoothMarkers() {
        BluetoothManager bluetoothManager = (BluetoothManager) mContext.getSystemService(Context.BLUETOOTH_SERVICE);
        List<BluetoothDevice> devices = bluetoothManager.getConnectedDevices(BluetoothProfile.GATT);
        for (BluetoothDevice device : devices) {
            if (device.getType() == BluetoothDevice.DEVICE_TYPE_LE) {
                try {
                    if (device.getName().contains("abc")) {
                        Method m = device.getClass()
                                .getMethod("removeBond", (Class[]) null);
                        m.invoke(device, (Object[]) null);
                    }
                } catch (Exception e) {
                    Log.e("fail", e.getMessage());
                }
            }
        }
        return devices.size() > 0;
    }

    private boolean connectBluetoothMarker() {
        BluetoothManager bluetoothManager = (BluetoothManager) mContext.getSystemService(Context.BLUETOOTH_SERVICE);
        List<BluetoothDevice> devices = bluetoothManager.getConnectedDevices(BluetoothProfile.GATT);
        for (BluetoothDevice device : devices) {
            if (device.getType() == BluetoothDevice.DEVICE_TYPE_LE) {
                try {
                    if (device.getName().contains("abc")) {
                        Method m = device.getClass()
                                .getMethod("removeBond", (Class[]) null);
                        m.invoke(device, (Object[]) null);
                    }
                } catch (Exception e) {
                    Log.e("fail", e.getMessage());
                }
            }
        }
        return devices.size() > 0;
    }

    @Override
    public void OnBluetoothHeadsetConnected() {
        isBluetoothDopplerConnected = true;

        if (nstView != null)
            nstView.toggleDeviceState(isBluetoothDopplerConnected, bluetoothMarkerConected);
        if(!mBeatsProcessor.isListening())
            mBeatsProcessor.startListening(BeatsProcessor.BLUETOOTH_HEADSET );

        Toast.makeText(this.mContext.getApplicationContext(), "Device Detected", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void OnBluetoothHeadsetDisconnected() {
        isBluetoothDopplerConnected = false;
        this.stopRecording();

        Toast.makeText(this.mContext.getApplicationContext(), "Device Disconnected", Toast.LENGTH_SHORT).show();

        if (nstView != null)
            nstView.toggleDeviceState(isBluetoothDopplerConnected, bluetoothMarkerConected);
        stopRecording();
        stopListening();
    }

    @Override
    public void OnBluetoothMarkerStateChanged(boolean state) {
        bluetoothMarkerConected = state;
        if (nstView != null)
            nstView.toggleDeviceState(isBluetoothDopplerConnected, state);
        if(state && isBluetoothDopplerConnected && mBeatsProcessor!= null && !mBeatsProcessor.isListening())
            startListening();
    }

    @Override
    public void OnBluetoothSingleKeyPress() {
        Log.i("singleclick", isRecording() + " ");
        if (nstView != null && isRecording() && !isPause())
            sendVolumeBroadcast();
    }

    @Override
    public void OnBluetoothDoubleKeyPress() {
        //if(isRecording())
        //    toggelRecordingState();
    }

    private void sendVolumeBroadcast() {
        Intent intent = new Intent(NstView.NST_MOVEMENTS);
        intent.putExtra("key", KeyEvent.KEYCODE_MEDIA_NEXT);
        LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
    }

    public GraphExtraData getData() {
        return data;
    }

    public void setData(GraphExtraData data) {
        this.data = data;
    }

    public static String getPDFFilename() {
        String filepath = Environment.getExternalStorageDirectory().getPath();
        File file = new File(filepath, BeatsRecorder.AUDIO_RECORDER_FOLDER);
        if (!file.exists()) {
            file.mkdirs();
        }

        SimpleDateFormat format = new SimpleDateFormat("ddMMyyyy_HHmmss_SSS");
        Date now = Calendar.getInstance().getTime();
        return file.getAbsolutePath() + "/test_" + format.format(now) + ".pdf";
    }

    public static void appendLog(ArrayList<Integer> text) {
        File logFile = new File(Environment.getExternalStorageDirectory().getPath() + "/log.txt");
        if (!logFile.exists()) {
            try {
                logFile.createNewFile();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        try {
            //BufferedWriter for performance, true to set append to file flag
            BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
            for (Integer i : text) {
                String s = i + "";
                buf.append(s);
                buf.newLine();
            }

            buf.close();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static String[] getBitmapFromView() {

        //Define a bitmap with the same size as the view
        //Bitmap b = nstView.getParentView().getDrawingCache();
        FHRGraphView graphView = new FHRGraphView();

        Bitmap b[] = graphView.getNSTGraph(NstView.graphBpmEntries, NstView.graphMovmentEntries, data);
        String imgPath[] = new String[b.length];
        for (int i = 0; i < b.length; i++) {


            imgPath[i] = getImageFilename();
            File file = new File(imgPath[i]);

            try {
                OutputStream os = new FileOutputStream(file);
                b[i].compress(Bitmap.CompressFormat.JPEG, 60, os);
                os.flush();
                os.close();
                ContentValues image = new ContentValues();
                image.put(Images.Media.TITLE, "NST");
                image.put(Images.Media.DISPLAY_NAME, imgPath[i].substring(imgPath[i].lastIndexOf('/') + 1));
                image.put(Images.Media.DESCRIPTION, "App Image");
                image.put(Images.Media.DATE_ADDED, System.currentTimeMillis());
                image.put(Images.Media.MIME_TYPE, "image/jpg");
                image.put(Images.Media.ORIENTATION, 0);
                File parent = file.getParentFile();
                image.put(Images.ImageColumns.BUCKET_ID, parent.toString()
                        .toLowerCase().hashCode());
                image.put(Images.ImageColumns.BUCKET_DISPLAY_NAME, parent.getName()
                        .toLowerCase());
                image.put(Images.Media.SIZE, file.length());
                image.put(Images.Media.DATA, file.getAbsolutePath());
                Uri result = mContext.getContentResolver().insert(
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI, image);
            } catch (FileNotFoundException fnfe) {
                fnfe.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        return imgPath;
    }

    public static String createNstPdf() {
        if (NstView.graphBpmEntries != null && NstView.graphBpmEntries.size() == 0)
            return null;
        String dirpath = getPDFFilename();
        try {
            Document document = new Document(PageSize.A4.rotate());
            PdfWriter.getInstance(document, new FileOutputStream(dirpath)); //  Change pdf's name.
            document.open();


            String paths[] = getBitmapFromView();
            Image img[] = new Image[paths.length];  // Change image's name and extension.
            for (int i = 0; i < paths.length; i++) {
                img[i] = Image.getInstance(paths[i]);


                float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
                        - document.rightMargin() - 0) / img[i].getWidth()) * 100; // 0 means you have no indentation. If you have any, change it.
                img[i].scalePercent(scaler);


                img[i].setAlignment(Image.ALIGN_CENTER | Image.ALIGN_TOP);
                //img.setAlignment(Image.LEFT| Image.TEXTWRAP);

            /* float width = document.getPageSize().width() - document.leftMargin() - document.rightMargin();
            float height = document.getPageSize().height() - document.topMargin() - document.bottomMargin();
            img.scaleToFit(width, height)*/

                document.add(img[i]);
            }
            document.close();
        } catch (BadElementException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return dirpath;
    }

    public void processFinished() {
        try {
            mContext.sendBroadcast(new Intent("com.carenx.fetoscope.nst.stop"));
            String filePath = this.fWriter.finishWritingFile();
            if (this.mListener != null) {
                if (nstView != null) {
                    //this.mListener.onRecordingFinishListener(filePath, createNstPdf());
                    this.mListener.onRecordingFinishListener(filePath, createNstPdf(), NstView.graphBpmEntries,NstView.graphMovmentEntries);
                }else
                    this.mListener.onRecordingFinishListener(filePath);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public interface RecordingEventListener {
        void onRecordingStartListener();

        void onRecordingFinishListener(String audioPath);

        void onRecordingFinishListener(String audioPath, String imagePath);

        void onRecordingFinishListener(String audioPath, String imagePath, ArrayList<Integer> bpm, ArrayList<Integer> movements);

        boolean onFhrButtonPressed();

        boolean onMarkerButtonPressed();
    }

}
