package com.carenx.beatsv3.dsp.filters;

/**
 * Created by avinash on 11/6/16.
 */
public class HighPass extends IIRFilter {
    public HighPass(float freq, float sampleRate) {
        super(freq, sampleRate);
    }

    protected void calcCoeff() {
        float fracFreq = this.getFrequency() / this.getSampleRate();
        float x = (float)Math.exp(-6.283185307179586D * (double)fracFreq);
        this.a = new float[]{(1.0F + x) / 2.0F, -(1.0F + x) / 2.0F};
        this.b = new float[]{x};
    }
}