package com.carenx.beatsv3.dsp.filters;

/**
 * Created by avinash on 11/6/16.
 */
public class BandPass extends IIRFilter {
    private float bw;

    public BandPass(float freq, float bandWidth, float sampleRate) {
        super(freq, sampleRate);
        this.setBandWidth(bandWidth);
    }

    public void setBandWidth(float bandWidth) {
        this.bw = bandWidth / this.getSampleRate();
        this.calcCoeff();
    }

    public float getBandWidth() {
        return this.bw * this.getSampleRate();
    }

    protected void calcCoeff() {
        float R = 1.0F - 3.0F * this.bw;
        float fracFreq = this.getFrequency() / this.getSampleRate();
        float T = 2.0F * (float)Math.cos(6.283185307179586D * (double)fracFreq);
        float K = (1.0F - R * T + R * R) / (2.0F - T);
        this.a = new float[]{1.0F - K, (K - R) * T, R * R - K};
        this.b = new float[]{R * T, -R * R};
    }
}
