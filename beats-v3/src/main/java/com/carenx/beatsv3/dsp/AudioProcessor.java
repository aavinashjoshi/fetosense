package com.carenx.beatsv3.dsp;

/**
 * Created by avinash on 11/6/16.
 */
public interface AudioProcessor {
    boolean process(AudioEvent var1);

    void processingFinished();
}
