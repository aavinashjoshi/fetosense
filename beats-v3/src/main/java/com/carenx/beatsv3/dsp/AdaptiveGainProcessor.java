package com.carenx.beatsv3.dsp;

import android.os.Environment;
import android.util.Log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import static android.view.View.Y;

/**
 * Created by avinash on 7/6/17.
 */

public class AdaptiveGainProcessor implements AudioProcessor {
    private File logFile;
    private double gain;
    private double gainValue;
    private float power = 0;
    private double powerPre = 0;
    private double alpha = 0;
    private double powerRec=0;

    /*final float VERY_LOW_THRES = -9; // Threshold for low power
    final float MOD_LOW_THRES = -5; // Threshold for low power
    final float LOW_THRES = -2; // Threshold for low power
    final float HIGH_THRES = 3; // Threshold for high power
    final float VERY_HIGH_THRES = 10; // Threshold for high power
    final float MOD_HIGH_THRES = 6; // Threshold for high power*/


    final float LOW_THRES = 3; // Threshold for low power
    final float HIGH_THRES = 12; // Threshold for high power
    final float MOD_THRES = 8;

    private double kVal;
    private double factor;
    private double inverseFactor;

    public AdaptiveGainProcessor(double newGain) {
        this.setGain(newGain);
        power=0;
        powerPre=0;
        createLog();
    }

    public void setGain(double newGain) {
        this.gainValue = newGain;
        this.gain =  Math.pow(10,newGain / 10);
    }
    public double getGain() {
        return gainValue;
    }

    /*public boolean process(AudioEvent audioEvent) {
        float[] audioFloatBuffer = audioEvent.getFloatBuffer();
        power=0;

        for (int i = audioEvent.getOverlap(); i < audioFloatBuffer.length; ++i) {

            power += (double) audioFloatBuffer[i] * (double) audioFloatBuffer[i];
        }

        factor = power==0?power:1/power;
        factor = Math.pow(10,(factor/10));

        //appendLog(power+"   "+factor);
        Log.i("Power",""+(power));

        for (int i = audioEvent.getOverlap(); i < audioFloatBuffer.length; ++i) {

            float newValue = (float) (((double) (audioFloatBuffer[i]) * (gain * factor)));
            if (newValue > 1.0F) {
                newValue = 1.0F;
            } else if (newValue < -1.0F) {
                newValue = -1.0F;
            }

            audioFloatBuffer[i] = newValue;
        }
        //factor = power/

        return true;
    }
*/
    /*public boolean process(AudioEvent audioEvent) {
        float[] audioFloatBuffer = audioEvent.getFloatBuffer();
        power=0;
        for(int i = audioEvent.getOverlap(); i < audioFloatBuffer.length; ++i) {
            power+=(double)audioFloatBuffer[i] * (double)audioFloatBuffer[i];
        }

        if (power > powerPre) {
            alpha = 0.8187;
        } else {
            alpha = 0.99;
        }

        if(powerPre==0){
            powerRec = power;
        }else
            powerRec = (alpha *power) + (1-alpha)*powerPre;
        powerPre = powerRec;
        Log.i("ADC power",""+power);

        inverseFactor = power==0?power:1/power;
        inverseFactor = Math.pow(10,(factor/10))*10;

        if (true) {
            double value = Math.pow(10,powerRec/10);
            Log.i("ADC_value",""+value);*/
            /*if (value < VERY_LOW_THRES) {
                kVal = (value + inverseFactor*10);
                Log.i("ADC_kVal",""+kVal);
                factor = Math.pow(10, (kVal * 0.1));
                Log.i("Factor low",""+factor);
            }else if (value < MOD_LOW_THRES) {
                kVal = (value + 15);
                Log.i("ADC_kVal",""+kVal);
                factor = Math.pow(10, (kVal * 0.1));
                Log.i("Factor low",""+factor);
            }else if (value < LOW_THRES) {
                kVal = (value + 10);
                Log.i("ADC_kVal",""+kVal);
                factor = Math.pow(10, (kVal * 0.1));
                Log.i("Factor low",""+factor);
            } else if (value > MOD_HIGH_THRES && value < HIGH_THRES) {
                kVal = (-1.333333 * value) + (5 * -1.333333);
                factor = Math.pow(10, (kVal * 0.1));
                Log.i("Factor mod",""+factor);
            } else if (value > HIGH_THRES && value < VERY_HIGH_THRES) {
                kVal = (-1.333333 * value) + (10 * -1.333333);
                factor = Math.pow(10, (kVal * 0.1));
                Log.i("Factor high",""+factor);
            }else if (value > VERY_HIGH_THRES) {
                kVal = (-1.333333 * value) + (20 * -1.333333);
                factor = Math.pow(10, (kVal * 0.1));
                Log.i("Factor high",""+factor);
            } else {
                factor = 1;
                Log.i("Factor",""+factor);
            }*/

            /*if (value > HIGH_THRES && value < HIGH_THRES) {
                kVal = (value * inverseFactor);
                Log.i("ADC_kVal",""+kVal);
                factor = Math.pow(10, (kVal * 0.1));
                Log.i("Factor high",""+factor);
            }else if (value > HIGH_THRES) {
                kVal = (-1.333333 * value) + (inverseFactor * -1.333333);
                factor = Math.pow(10, (kVal * 0.1));
                Log.i("Factor high",""+factor);
            } else {
                factor = 1;
                Log.i("Factor",""+factor);
            }

            //Log.i("Factor",""+factor);
            for (int n = 0; n < audioFloatBuffer.length; n++) {
                float newValue = (float) ((double) audioFloatBuffer[n] * this.gain);
                newValue += newValue * factor;
                if (newValue > 1.0F) {
                    newValue = 1.0F;
                } else if (newValue < -1.0F) {
                    newValue = -1.0F;
                }

                audioFloatBuffer[n] = newValue;
            }
        }else {
            for (int i = audioEvent.getOverlap(); i < audioFloatBuffer.length; ++i) {
                float newValue = (float) ((double) audioFloatBuffer[i] * this.gain);
                if (newValue > 1.0F) {
                    newValue = 1.0F;
                } else if (newValue < -1.0F) {
                    newValue = -1.0F;
                }

                audioFloatBuffer[i] = newValue;
            }
        }
        return true;
    }*/

    public boolean process(AudioEvent audioEvent) {
        float[] audioFloatBuffer = audioEvent.getFloatBuffer();
        power=0;
        for(int i = audioEvent.getOverlap(); i < audioFloatBuffer.length; ++i) {
            power+=(double)audioFloatBuffer[i] * (double)audioFloatBuffer[i];
        }

        if (power > powerPre) {
            alpha = 0.8187;
        } else {
            alpha = 0.99;
        }

        if(powerPre==0){
            powerRec = power;
        }else
            powerRec = (alpha *power) + (1-alpha)*powerPre;
        powerPre = powerRec;
        Log.i("ADC power",""+power);

        if (true) {
            double value = 10 * Math.log10(powerRec);
            Log.i("ADC_",""+value);
            if (value < LOW_THRES) {
                kVal = (value + 25);
                Log.i("ADC_kVal",""+kVal);
                factor = Math.pow(10, (kVal * 0.1));
                Log.i("Factor low",""+factor);
            } else if (value > MOD_THRES) {
                kVal = (value + 15 );
                factor = Math.pow(10, (kVal * 0.1));
                Log.i("Factor mod",""+factor);
            } if (value > HIGH_THRES) {
                kVal = (-1.333333 * value) + (15 * -1.333333);
                factor = Math.pow(10, (kVal * 0.1));
                Log.i("Factor high",""+factor);
            } else {
                factor = 1;
                Log.i("Factor",""+factor);
            }
            //Log.i("Factor",""+factor);
            for (int n = 0; n < audioFloatBuffer.length; n++) {
                float newValue = (float) ((double) audioFloatBuffer[n] * this.gain);
                newValue += newValue * factor;
                if (newValue > 1.0F) {
                    newValue = 1.0F;
                } else if (newValue < -1.0F) {
                    newValue = -1.0F;
                }

                audioFloatBuffer[n] = newValue;
            }
        }else {
            for (int i = audioEvent.getOverlap(); i < audioFloatBuffer.length; ++i) {
                float newValue = (float) ((double) audioFloatBuffer[i] * this.gain);
                if (newValue > 1.0F) {
                    newValue = 1.0F;
                } else if (newValue < -1.0F) {
                    newValue = -1.0F;
                }

                audioFloatBuffer[i] = newValue;
            }
        }
        return true;
    }


    public void createLog() {
        logFile = new File(Environment.getExternalStorageDirectory().getPath() + "/Powerlog.txt");
        if (!logFile.exists()) {
            try {
                logFile.createNewFile();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }
    public void appendLog(String text)
    {
        try
        {
            //BufferedWriter for performance, true to set append to file flag
            BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));

                String s = text+"";
                buf.append(s);
                buf.newLine();


            buf.close();
        }
        catch (Exception e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


    public void processingFinished() {
    }
}
