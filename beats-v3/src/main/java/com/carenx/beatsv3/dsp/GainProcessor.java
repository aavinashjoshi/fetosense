package com.carenx.beatsv3.dsp;

import android.util.Log;

/**
 * Created by avinash on 11/6/16.
 */
public class GainProcessor implements AudioProcessor {
    private double gain;
    private double gainValue;

    public GainProcessor(double newGain) {
        this.setGain(newGain);
    }

    public void setGain(double newGain) {
        this.gainValue = newGain;
        this.gain =  Math.pow(10,newGain / 10);
    }
    public double getGain() {
        return gainValue;
    }

    public boolean process(AudioEvent audioEvent,boolean preamp) {
        byte[] buffer = audioEvent.getByteBuffer();
        for(int i = audioEvent.getOverlap(); i < buffer.length; ++i) {
            int newValue = (int)((int)buffer[i] * this.gain * 5);
            /*if(newValue > 128) {
                newValue = 128;
            } else if(newValue < -128) {
                newValue = -128;
            }*/

            buffer[i] = (byte)newValue;
        }


    /*float[] audioFloatBuffer = audioEvent.getFloatBuffer();

        for(int i = audioEvent.getOverlap(); i < audioFloatBuffer.length; ++i) {
            float newValue = (float)((double)audioFloatBuffer[i] * this.gain);
            if(newValue > 1.0F) {
                newValue = 1.0F;
            } else if(newValue < -1.0F) {
                newValue = -1.0F;
            }

            audioFloatBuffer[i] = newValue;
        }*/

        return true;
    }

    public boolean process(AudioEvent audioEvent) {
        float[] audioFloatBuffer = audioEvent.getFloatBuffer();

        for(int i = audioEvent.getOverlap(); i < audioFloatBuffer.length; ++i) {
            float newValue = (float)((double)audioFloatBuffer[i] * this.gain);
            if(newValue > 1.0F) {
                newValue = 1.0F;
            } else if(newValue < -1.0F) {
                newValue = -1.0F;
            }

            audioFloatBuffer[i] = newValue;
        }

        return true;
    }

    public void processingFinished() {
    }
}
