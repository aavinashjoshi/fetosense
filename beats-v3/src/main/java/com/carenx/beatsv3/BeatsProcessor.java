package com.carenx.beatsv3;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.net.rtp.AudioStream;
import android.os.Process;
import android.util.Log;

import com.carenx.beatsv3.dsp.AdaptiveGainProcessor;
import com.carenx.beatsv3.dsp.AudioEvent;
import com.carenx.beatsv3.dsp.AudioPlayer;
import com.carenx.beatsv3.dsp.DSPAudioFormat;
import com.carenx.beatsv3.dsp.AutomaticGainControl;
import com.carenx.beatsv3.dsp.GainProcessor;
import com.carenx.beatsv3.dsp.filters.LowPass;
import com.carenx.beatsv3.utils.BeatsVisualizer4;
import com.carenx.beatsv3.wavelet.Daubechies4WaveletCoder;
import com.carenx.beatsv3.wavelet.HaarWaveletCoder;
import com.carenx.beatsv3.wavelet.WaveletShrinkage;

import java.io.BufferedInputStream;
import java.io.IOException;

import static com.carenx.beatsv3.BeatsRecorder.SAMPLE_RATE;
import static com.carenx.beatsv3.BeatsRecorder.getIsBluetoothDopplerConnected;


/**
 * Created by avinash on 11/6/16.
 *
 */

public class BeatsProcessor {
    public static final int WIRED_HEADSET = 0;
    public static final int BLUETOOTH_HEADSET = 1;
    private final Context context;
    private final AudioManager am;
    public byte[] mBuffer;
    public short[] mBuffer2;
    private int mBufferSize;
    private AudioRecord mRecorder;
    private int mSampleRate;
    private boolean mIsRecording = false;
    private DSPAudioFormat mTarsosFormat;
    private BeatsProcessor.OnAudioEventListener mOnAudioEventListener;
    private BufferedInputStream mBufStream;
    private int recordingMode = 3;
    private GainProcessor preAmplifier;
    private GainProcessor postAmplifier;
    private AutomaticGainControl postAmplifier2;
    private AdaptiveGainProcessor postAmplifier3;
    private LowPass mLowPassFilter;
    private static float preAmpDi = 2.5F;
    private static float postAmpDi = 2.5F;
    private static float preAmpbe = 5.0F;
    private static float postAmpbe = 5.0F;
    private static float preAmpDefault = 10.0F;
    private static float postAmpDefault = 10.0F;

    private static boolean isFilterOn = true;

    private Daubechies4WaveletCoder db4WaveletCoder;
    private HaarWaveletCoder haarWaveletCoder;
    private WaveletShrinkage dbWavelet;

    private static boolean useMic = true;
    private static boolean useBluetooth = true;
    private static boolean useWavelet = false;
    private static int gain = 1;

    private GainProcessor postWaveletAmplifier;
    private boolean isPaused;
    private AudioPlayer audioPlayer;
    private boolean mIsListening = false;
    private BeatsVisualizer4 visualizer;

    public BeatsProcessor(Context context, int sampleRate) {
        this.context = context;
        this.mSampleRate = sampleRate;
        //this.mBufferSize = AudioRecord.getMinBufferSize(mSampleRate, android.media.DSPAudioFormat.CHANNEL_IN_MONO,
        //        android.media.DSPAudioFormat.ENCODING_PCM_16BIT);
        this.mBufferSize = 320;
        this.mBuffer = new byte[this.mBufferSize];

        this.mRecorder = new AudioRecord(MediaRecorder.AudioSource.MIC, this.mSampleRate, 16, 2, this.mBufferSize);
        am = (AudioManager)context.getSystemService(Context.AUDIO_SERVICE);
        //am.setSpeakerphoneOn(true);
        this.mTarsosFormat = new DSPAudioFormat((float)this.mSampleRate, 16, 1, true, false);

        mBufStream = new BufferedInputStream(context.getResources().openRawResource(R.raw.test_5));
        /*if (AutomaticGainControl.isAvailable()) {
            AutomaticGainControl agc = AutomaticGainControl.create(mRecorder.getAudioSessionId());
            agc.setEnabled(true);
        }*/
        /*if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
            LoudnessEnhancer loudnessEnhancer= new LoudnessEnhancer(mRecorder.getAudioSessionId());
            loudnessEnhancer.setEnabled(true);
            loudnessEnhancer.setTargetGain(1000);

        }*/

    }

    public static boolean isUseBluetooth() {
        return useBluetooth;
    }

    public static void setUseBluetooth(boolean _useBluetooth) {
        BeatsProcessor.useBluetooth = _useBluetooth;
    }

    public void setRecordingMode(int mode) {
        this.recordingMode = mode;
    }

    public DSPAudioFormat getAudioForrmat() {
        return this.mTarsosFormat;
    }

    public int getBufferSize() {
        return this.mBufferSize;
    }

    public boolean isRecording() {
        return this.mIsRecording;
    }
    public boolean isListening() {
        return this.mIsListening;
    }
    @Override
    protected void finalize() throws Throwable {

        if (isRecording())
            stopRecording();
        if(mIsListening)
            stopListening();
        super.finalize();
    }

    public void startRecording() {
        if(this.mOnAudioEventListener != null) {
            this.mOnAudioEventListener.processStart();
        }

        if(!useMic)
            mBufStream = new BufferedInputStream(context.getResources().openRawResource(R.raw.test_5));

        this.mIsRecording = true;
        resume();

    }

    private void startListening() {
        this.initFilters();


        if(!useMic) {
            mBufStream = new BufferedInputStream(context.getResources().openRawResource(R.raw.test_5));
            this.mRecorder.startRecording();
            this.mIsListening = true;
            resume();
            this.processAudio();
        }

        if(useMic && getIsBluetoothDopplerConnected() && am.isBluetoothScoOn()) {
            this.mRecorder.startRecording();
            this.mIsListening = true;
            resume();
            this.processAudio();
        }
    }



    public void stopListening() {
        mIsListening = false;
        mRecorder.stop();
        resume();
        am.stopBluetoothSco();


    }
    public void stopRecording() {
        mIsRecording = false;

        resume();

        if(mOnAudioEventListener != null) {
            mOnAudioEventListener.processFinished();
        }

    }

    public void pause(){
        if(isRecording())
            this.isPaused = true;

    }

    public void resume(){
        if(isRecording())
            this.isPaused = false;
    }

    public void setOnAudioEventListener(BeatsProcessor.OnAudioEventListener listener) {
        this.mOnAudioEventListener = listener;
    }

    private void processAudio() {
        (new Thread(new Runnable() {
            public void run() {
                Process.setThreadPriority(Process.THREAD_PRIORITY_URGENT_AUDIO);

                while(mIsListening) {

                    AudioEvent audioEvent;
                    if (useMic) {
                        int bufferReadResult = BeatsProcessor.this.mRecorder.read(BeatsProcessor.this.mBuffer, 0, BeatsProcessor.this.mBufferSize);
                        audioEvent = new AudioEvent(BeatsProcessor.this.mTarsosFormat, (long) bufferReadResult);
                        audioEvent.setFloatBufferWithByteBuffer(BeatsProcessor.this.mBuffer);
                    }else{
                        audioEvent = new AudioEvent(mTarsosFormat, mBufferSize);
                        try {
                            int bufferReadResult = BeatsProcessor.this.mRecorder.read(BeatsProcessor.this.mBuffer, 0, BeatsProcessor.this.mBufferSize);
                            audioEvent = new AudioEvent(BeatsProcessor.this.mTarsosFormat, (long) bufferReadResult);
                            audioEvent.setFloatBufferWithByteBuffer(BeatsProcessor.this.mBuffer);
                            mBufStream.read(mBuffer, 0, mBufferSize);
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        audioEvent.setFloatBufferWithByteBuffer(mBuffer);
                    }
                        BeatsProcessor.this.filterAudio(audioEvent);

                    if(mIsRecording && !isPaused && BeatsProcessor.this.mOnAudioEventListener != null) {
                        BeatsProcessor.this.mOnAudioEventListener.processAudioProcEvent(audioEvent);
                    }




                }

            }
        })).start();
    }

    private void initFilters() {
        audioPlayer = new AudioPlayer(SAMPLE_RATE,mBufferSize, AudioManager.STREAM_MUSIC);
        this.mLowPassFilter = new LowPass(200.0F, (float)this.mSampleRate);
        this.postAmplifier2 = new AutomaticGainControl((double)preAmpDefault);
        this.postAmplifier = new GainProcessor((double)postAmpDefault);

        /*if(this.recordingMode == 0) {
            this.postAmplifier2 = new AutomaticGainControl((double)preAmpDi);
            this.postAmplifier = new GainProcessor((double)postAmpDi);

        } else if(this.recordingMode == 1) {
            this.postAmplifier2 = new AutomaticGainControl((double)preAmpbe);
            this.postAmplifier = new GainProcessor((double)postAmpbe);
        } else {
            this.postAmplifier2 = new AutomaticGainControl((double)preAmpDefault);
            this.postAmplifier = new GainProcessor((double)postAmpDefault);
        }*/
        //this.postAmplifier2 = new AutomaticGainControl(1);
        //this.postAmplifier3 = new AdaptiveGainProcessor(1.5);

        if(useWavelet) {
            haarWaveletCoder = new HaarWaveletCoder();
            dbWavelet = new WaveletShrinkage();
            db4WaveletCoder = new Daubechies4WaveletCoder();
            postWaveletAmplifier = new GainProcessor(8.0);
        }
    }

    private void filterAudio(AudioEvent ae) {
        //this.postAmplifier2.process(ae);
        //if(postAmplifier2.getGain()>1)
        audioPlayer.process(ae);
            this.postAmplifier2.process(ae);
        if (useWavelet){
            haarWaveletCoder.process(ae);
            //dbWavelet.process(ae);
            //db4WaveletCoder.process(ae);
            this.postWaveletAmplifier.process(ae);
        }
        if (isFilterOn){
                this.mLowPassFilter.process(ae);
        }
        if(postAmplifier.getGain()>1)
            this.postAmplifier.process(ae);
        if(visualizer!=null)
            visualizer.process(ae);
        //this.postAmplifier2.process(ae);
        //this.postAmplifier3.process(ae);
    }

/*
    public static void setPreAmpBell(float preAmpbe) {
        preAmpbe = preAmpbe;
    }

    public static void setPostAmpBell(float postAmpbe) {
        postAmpbe = postAmpbe;
    }

    public static void setPostAmpDiaphragm(float postAmpDi) {
        postAmpDi = postAmpDi;
    }

    public static void setPreAmpDiaphragm(float _preAmpDi) {
        preAmpDi = _preAmpDi;
    }
*/

     public static void setPreAmpDefault(float _preAmpDefault) {
        preAmpDefault = _preAmpDefault;
    }

    public static void setPostAmpDefault(float _postAmpDefault) {
        postAmpDefault = _postAmpDefault;
    }

    public static void setFilterOn(boolean filterOn) {
        isFilterOn = filterOn;
    }


    public static boolean isUseMic() {
        return useMic;
    }

    public static void setUseMic(boolean _useMic) {
        useMic = _useMic;
    }
    public static void setUseWavelet(boolean _useWavelet) {
        useWavelet = _useWavelet;
    }

    public boolean isPaused() {
        return isPaused;
    }

    public int getAudioSessionId() {
        return audioPlayer.getAudioSessionId();
    }

    public void setVisualizer(BeatsVisualizer4 visualizer) {
        this.visualizer = visualizer;
    }

    public interface OnAudioEventListener {
        void processStart();

        void processAudioProcEvent(AudioEvent var1);

        void processFinished();
    }

    public void setGainLevel(boolean val){
        if(val){
            gain++;
            
        }else {

        }
    }


    public void startListening(int mode){
        if(mode == WIRED_HEADSET){
            startListening();
        }else if(mode == BLUETOOTH_HEADSET){
            bleListen();
        }
    }

    public void bleListen()
    {
        context.registerReceiver(new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                int state = intent.getIntExtra(AudioManager.EXTRA_SCO_AUDIO_STATE, -1);
                Log.i("D", "Audio SCO state: " + state);

                if (AudioManager.SCO_AUDIO_STATE_CONNECTED == state) {
                    Log.d("D", "Audio SCO state: "+state);
	                  /*
	                   * Now the connection has been established to the bluetooth device.
	                   * Record audio or whatever (on another thread).With AudioRecord you can record with an object created like this:
	                   * new AudioRecord(MediaRecorder.AudioSource.MIC, 8000, DSPAudioFormat.CHANNEL_CONFIGURATION_MONO,
	                   * DSPAudioFormat.ENCODING_PCM_16BIT, audioBufferSize);
	                   *
	                   * After finishing, don't forget to unregister this receiver and
	                   * to stop the bluetooth connection with am.stopBluetoothSco();
	                   */
                    startListening();

                    context.unregisterReceiver(this);
                }

            }
        }, new IntentFilter(AudioManager.ACTION_SCO_AUDIO_STATE_CHANGED));

        Log.d("D", "starting bluetooth");

        am.setMode(AudioManager.MODE_NORMAL);
        am.startBluetoothSco();
        am.setSpeakerphoneOn(true);
    }

}
